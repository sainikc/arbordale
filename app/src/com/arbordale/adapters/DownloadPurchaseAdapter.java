package com.arbordale.adapters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arbordale.beans.GridImageList;
import com.arbordale.beans.GridImgpickerData;
import com.arbordale.beans.HeaderData;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.R;
import com.arbordale.inapp.InAppPurchaseActivity;
import com.arbordale.navfragments.PurchaseDownload;
import com.arbordale.navfragments.UnZipper;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.PathUtil;
import com.arbordale.util.UtilMethods;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

public class DownloadPurchaseAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter, Observer {

    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private List<HeaderData> mItems;
    private List<GridImageList> mItems1;
    private List<GridImgpickerData> mItemsS;
    Context mContext;
    ProgressBar mBar;
    Dialog d;
    private static String fileDownloading = "File", fileSetName = "set", file_downloading_db_name = "";
    private static int positionSelected = 0;
    private float scale;
    private LinearLayout.LayoutParams lparams;


    public DownloadPurchaseAdapter(Context context, List<HeaderData> items, List<GridImageList> items1, List<GridImgpickerData> itemss, int headerResId, int itemResId) {
        init(context, items, items1, itemss, headerResId, itemResId);
        scale = mContext.getResources().getDisplayMetrics().density;
    }

    private void init(Context context, List<HeaderData> items, List<GridImageList> items1, List<GridImgpickerData> itemss, int headerResId, int itemResId) {
        this.mItems = items;
        this.mItems1 = items1;
        this.mItemsS = itemss;
        this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        mInflater = LayoutInflater.from(context);
        this.mContext = context;
        lparams = new LinearLayout.LayoutParams(112, 148);

    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return mItems1.size();
    }


    @Override
    public ArrayList<String> getItem(int position) {
        return mItems1.get(position).getSectionNameList();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mItemResId, parent, false);

            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.img_icon);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.button = (Button) convertView.findViewById(R.id.purchase);

            if (scale == 2.0) {
            } else
                holder.imageView.setLayoutParams(lparams);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageView.setImageResource(android.R.color.transparent);

        try {

            if (mItemsS.get(position).getSection().contains("Set")) {
             /*if(scale==2.0)
              {*/
                if (mItemsS.get(position).getShortTitle().equalsIgnoreCase("AllTitleSeteBooks"))
                    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImageDownloads(mContext, "thumbs_high_density/ic_launcher_132" + AppConstants.IMAGE_FORMAT_PNG));

                else
                    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImageDownloads(mContext, "thumbs_high_density/ic_launcher_orange_132" + AppConstants.IMAGE_FORMAT_PNG));
                //}
             
            /* else
             {
         	    if(mItemsS.get(position).getShortTitle().equalsIgnoreCase("AllTitleSeteBooks"))	
      			     holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs/ic_launcher_orange_132"+AppConstants.IMAGE_FORMAT_PNG));
        		
           	    else
        		     holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs/ic_launcher_132"+AppConstants.IMAGE_FORMAT_PNG));

             }	 
      */
            } else {
                //if(scale==2.0)

                if (mItemsS.get(position).getShortTitle().equalsIgnoreCase("WaterBeds") ||
                        mItemsS.get(position).getShortTitle().equalsIgnoreCase("RiverBeds") ||
                        mItemsS.get(position).getShortTitle().equalsIgnoreCase("Pieces") ||
                        mItemsS.get(position).getShortTitle().equalsIgnoreCase("Dolphin") ||
                        mItemsS.get(position).getShortTitle().equalsIgnoreCase("Parrot") ||
                        mItemsS.get(position).getShortTitle().equalsIgnoreCase("Moon")) {
                    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImageDownloadsSmallThumbs(mContext, "thumbs_high_density/" + mItemsS.get(position).getShortTitle() + "_132" + AppConstants.IMAGE_FORMAT_PNG));
                } else
                    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImageDownloads(mContext, "thumbs_high_density/" + mItemsS.get(position).getShortTitle() + "_132" + AppConstants.IMAGE_FORMAT_PNG));

        		/*else
                    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs/"+mItemsS.get(position).getShortTitle()+"_132"+AppConstants.IMAGE_FORMAT_PNG));
*/
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.imageView.setScaleType(ScaleType.FIT_END);

        if (mItemsS.get(position).getBookFullName().contains(", The") || mItemsS.get(position).getBookFullName().contains(", A")) {
            String arr[] = mItemsS.get(position).getBookFullName().split(",");
            holder.title.setText(arr[1] + " " + arr[0]);
        } else
            holder.title.setText(mItemsS.get(position).getBookFullName());

        holder.description.setText(mItemsS.get(position).getShortDescription());

        if (mItemsS.get(position).getDownloadStatus().equals("YES") && mItemsS.get(position).getStatus().equals("YES")) {
            holder.button.setText("Uninstall");
            holder.button.setBackgroundResource(R.drawable.login_btn_selector2);
        } else if (mItemsS.get(position).getStatus().equals("YES") && mItemsS.get(position).getDownloadStatus().equals("NO")) {
            holder.button.setText("Download");
            holder.button.setBackgroundResource(R.drawable.login_btn_selector1);
        } else {
            holder.button.setText("Buy Now");
            holder.button.setBackgroundResource(R.drawable.continue_btn_selector);
        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemsS.get(position).getBookFullName().contains(", The") || mItemsS.get(position).getBookFullName().contains(", A")) {
                    String arr[] = mItemsS.get(position).getBookFullName().split(",");
                    fileDownloading = arr[1] + " " + arr[0];
                    file_downloading_db_name = mItemsS.get(position).getBookFullName();
                } else {
                    fileDownloading = mItemsS.get(position).getBookFullName();
                    file_downloading_db_name = mItemsS.get(position).getBookFullName();
                }

                if (mItemsS.get(position).getSection().contains("Set"))
                    fileSetName = mItemsS.get(position).getShortTitle();

                else
                    fileSetName = "set";

                positionSelected = position;

                if (mItemsS.get(position).getDownloadStatus().equals("YES") && mItemsS.get(position).getStatus().equals("YES")) {
                    String path = Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + mItemsS.get(position).getShortTitle();
                    File f = new File(path);

                    if (f.isDirectory() && f.exists())
                        new UninstallTask().execute(mItemsS.get(position).getShortTitle());

                    else {
                        uninstall();
                        mItemsS.get(positionSelected).setDownloadStatus("NO");
                        notifyDataSetChanged();
                    }
                } else if (mItemsS.get(position).getStatus().equals("YES")) {
                    if (UtilMethods.isConnectionAvailable(mContext)) {
                        Log.i("Downloader", PathUtil.currentPath(1, "") + mItemsS.get(position).getShortTitle() + ".zip");
                        new DownLoader().execute(new String[]{PathUtil.currentPath(1, "") + mItemsS.get(position).getShortTitle() + ".zip", mItemsS.get(position).getShortTitle()});
                            /*Intent it=new Intent(mContext,DownloadService.class);
                            it.putExtra("source", PathUtil.currentPath(1,"")+mItemsS.get(position).getShortTitle()+".zip");
						    it.putExtra("destination", Environment.getExternalStorageDirectory()+"/Arbordale/"+mItemsS.get(position).getShortTitle()+".zip");
							it.putExtra("description", mItemsS.get(position).getShortDescription());
						    it.putExtra("title", mItemsS.get(position).getBookFullName());
						    mContext.startService(it);*/
                    } else {
                        UtilMethods.createToast(mContext, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                } else {
                    if (UtilMethods.isConnectionAvailable(mContext)) {
                        if (mItemsS.get(position).getSection().contains("Set"))
                            fileSetName = mItemsS.get(position).getShortTitle();

                        else
                            fileSetName = "set";

                        System.out.println("color code value is " + mItemsS.get(position).getColorCode());

                        Intent it = new Intent(mContext, InAppPurchaseActivity.class);
                        it.putExtra("book_name", mItemsS.get(position).getShortTitle());
                        it.putExtra("book_full_name", mItemsS.get(position).getBookFullName());
                        it.putExtra("file_set_name", fileSetName);
                        it.putExtra("no_of_pages", mItemsS.get(position).getNoOfPages());
                        it.putExtra("colorcode", mItemsS.get(position).getColorCode());
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(it);
                    } else {
                        UtilMethods.createToast(mContext, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                }

            }
        });
        return convertView;
    }


    protected class HeaderViewHolder {
        public TextView textView;
    }

    protected class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView description;
        public Button button;
    }

    public int getNumHeaders() {
        return mItems.size();
    }

    @Override
    public int getCountForHeader(int header) {
        return mItems1.get(header).getSectionNameList().size();
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mHeaderResId, parent, false);
            holder = new HeaderViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.top_grid_title);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in string
        holder.textView.setText("" + mItems.get(position).getSectionName());

        return convertView;
    }

    public class UninstallTask extends AsyncTask<String, Integer, String> {
        ProgressDialog ps = new ProgressDialog(mContext);

        @Override
        protected String doInBackground(String... params) {

            if (!fileSetName.equalsIgnoreCase("set")) {
                uninstall();
            } else {
                File fileTodelete = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + params[0]);

                try {
                    FileUtils.deleteDirectory(fileTodelete);
                    uninstall();
                    System.out.println("directory deleted");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ps.setTitle("Uninstall");
            ps.setMessage("in progress...");
            ps.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ps.dismiss();
            mItemsS.get(positionSelected).setDownloadStatus("NO");
            notifyDataSetChanged();
        }
    }

    public class DownLoader extends AsyncTask<String, Integer, Long> {

        int downloadingsuccessful = 1;
        int downloadingerror = 0;
        String filename = "";
        String urlValue = "";

        @Override
        public Long doInBackground(String... params) {

            Log.i("Downloader", "name of file is : " + params[0] + "/" + params[1]);
            System.out.println("Downloading path : " + params[0] + "/" + params[1]);

            urlValue = params[0].toString().replace(" ", "%20");

            try {
                InputStream InStream = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet getRequest = new HttpGet();
                    getRequest.setURI(new URI(urlValue));
                    HttpResponse response = httpclient.execute(getRequest);
                    long length = response.getEntity().getContentLength();
                    InStream = response.getEntity().getContent();

                    if (urlValue.contains("AllTitleSeteBooks")) {
                        mBar.setMax((int) (length / 100));
                    } else {
                        mBar.setMax((int) length);
                    }

                    mBar.setProgress(0);
                } catch (URISyntaxException e) {
                    Log.e("Downloading error ", e.getMessage().toString());
                    mBar.setProgress(0);
                    return Long.valueOf(downloadingerror);
                }

                File SDCardRoot = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER);
                //create a new file, specifying the path, and the filename
                //which we want to save the file as.

                File file = new File(SDCardRoot, params[1] + ".zip");
                FileOutputStream output = new FileOutputStream(file);
                byte data[] = new byte[256];
                long total = 0;
                int count;
                while ((count = InStream.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);

                    if (urlValue.contains("AllTitleSeteBooks")) {
                        publishProgress((int) (total / 100));
                    } else {
                        publishProgress((int) (total));
                    }

                }

                output.flush();
                output.close();
                InStream.close();

                System.out.println("file downloaded " + params[1]);
                filename = params[1];

            } catch (Exception e) {/*
				Log.e("Downloading error " , e.getMessage().toString());
				return Long.valueOf(downloadingerror);
			*/
            }
            return Long.valueOf(downloadingsuccessful);
        }

        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
            d.dismiss();

            Log.i("Downloader", "name of file is : " + filename);

            if (result == 1)
                unzipWebFile(filename);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            d = new Dialog(mContext, R.style.FullHeightDialog);
            d.setContentView(R.layout.download_dialog);
            d.setCancelable(false);
            d.setCanceledOnTouchOutside(false);
            mBar = (ProgressBar) d.findViewById(R.id.downloadprogresshorizontal);
            TextView txt = (TextView) d.findViewById(R.id.title);
            txt.setText("" + fileDownloading);
            d.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            mBar.setProgress(values[0]);
        }
    }

    private void unzipWebFile(String filename) {

        d = new Dialog(mContext, R.style.FullHeightDialog);
        d.setContentView(R.layout.download_dialog);
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);
        mBar = (ProgressBar) d.findViewById(R.id.downloadprogresshorizontal);
        TextView txt1 = (TextView) d.findViewById(R.id.dwnldtxt);
        txt1.setText("Installing...");
        TextView txt = (TextView) d.findViewById(R.id.title);
        txt.setText("" + fileDownloading);
        mBar.setVisibility(View.INVISIBLE);
        ProgressBar pb = (ProgressBar) d.findViewById(R.id.install_progress);
        pb.setVisibility(View.VISIBLE);
        d.show();

        File SDCardRoot = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER);

        String unzipLocation = SDCardRoot + "";
        String filePath = SDCardRoot.toString();

        UnZipper unzipper = new UnZipper(filename, filePath, unzipLocation);
        unzipper.addObserver(this);
        unzipper.unzip();

    }

    @Override
    public void update(Observable arg0, Object arg1) {

        SqliteDb db_conn = new SqliteDb(mContext);
        System.out.println("Updating book icon table, bookName : " + "book_full_name=" + DatabaseUtils.sqlEscapeString(file_downloading_db_name.trim()));
        System.out.println("File setName : " + fileSetName.trim());
        ContentValues cv = new ContentValues();
        cv.put("Download_status", "YES");

        System.out.println("Is Record Updated" + db_conn.updateData("book_icon", cv, "book_full_name=" + DatabaseUtils.sqlEscapeString(file_downloading_db_name.trim())));
        // To show db data before updation

        Cursor dbCurstor = db_conn.fetchData("select * from book_icon", null);
        dbCurstor.moveToFirst();
        while (!dbCurstor.isAfterLast()) {
            System.out.println("Before ! Book title:" + dbCurstor.getString(4) + " Download_status:" + dbCurstor.getString(7));
            dbCurstor.moveToNext();
        }


        if (!fileSetName.trim().equalsIgnoreCase("set")) {

            if (fileSetName.trim().equalsIgnoreCase("AllTitleSeteBooks")) {
                db_conn.updateData("book_icon", cv, null);
            } else {
                //System.out.println("Section="+DatabaseUtils.sqlEscapeString(fileSetName.trim()));
                db_conn.updateData("book_icon", cv, "Section=" + DatabaseUtils.sqlEscapeString(fileSetName.trim()));
            }
            Cursor numberOfRows;

            if (fileSetName.trim().equalsIgnoreCase("AllTitleSeteBooks"))
                numberOfRows = db_conn.fetchData("select Book_Name from book_icon", null);

            else
                //numberOfRows=db_conn.fetchData("select Book_Name from book_icon where Section=?", new String[]{"Spring2016"});
                numberOfRows = db_conn.fetchData("select Book_Name from book_icon where Section=?", new String[]{fileSetName.trim()});


            if (numberOfRows != null && numberOfRows.getCount() > 0) {
                numberOfRows.moveToFirst();
                while (!numberOfRows.isAfterLast()) {
                    int i = 0, size = mItemsS.size();
                    while (i < size) {
                        //if(mItemsS.get(i).getShortTitle().equalsIgnoreCase(numberOfRows.getString(0)))
                        if (mItemsS.get(i).getShortTitle().equalsIgnoreCase(numberOfRows.getString(0))) {
                            mItemsS.get(i).setDownloadStatus("YES");
                        }
                        i++;
                    }
                    numberOfRows.moveToNext();
                }
                numberOfRows.close();
            }
        }
        //	System.out.println("Book Name : "+mItemsS.get(positionSelected).getBookFullName());
        mItemsS.get(positionSelected).setDownloadStatus("YES");


        if (d != null && d.isShowing())
            d.dismiss();

        notifyDataSetChanged();
        // code to show book_icon data
        Cursor dbCurstor2 = db_conn.fetchData("select * from book_icon", null);
        dbCurstor2.moveToFirst();
        while (!dbCurstor2.isAfterLast()) {
            System.out.println("After ! Book title:" + dbCurstor2.getString(4) + " Download_status:" + dbCurstor2.getString(7));
            dbCurstor2.moveToNext();
        }
        dbCurstor.close();
        dbCurstor2.close();
        db_conn.closeDb();
    }

    public void uninstall() {
        SqliteDb db_conn = new SqliteDb(mContext);

        ContentValues cv = new ContentValues();
        cv.put("Download_status", "NO");

        db_conn.updateData("book_icon", cv, "book_full_name=" + DatabaseUtils.sqlEscapeString(file_downloading_db_name.trim()));

        if (!fileSetName.trim().equalsIgnoreCase("set")) {
            if (fileSetName.trim().equalsIgnoreCase("AllTitleSeteBooks"))
                db_conn.updateData("book_icon", cv, null);

            else
                db_conn.updateData("book_icon", cv, "Section=" + DatabaseUtils.sqlEscapeString(fileSetName));

            Cursor numberOfRows;

            if (fileSetName.trim().equalsIgnoreCase("AllTitleSeteBooks"))
                numberOfRows = db_conn.fetchData("select Book_Name from book_icon", null);

            else
                numberOfRows = db_conn.fetchData("select Book_Name from book_icon where Section=?", new String[]{fileSetName.trim()});

            if (numberOfRows.getCount() > 0) {
                numberOfRows.moveToFirst();
                while (!numberOfRows.isAfterLast()) {
                    File fileTodelete = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + numberOfRows.getString(0));

                    try {
                        FileUtils.deleteDirectory(fileTodelete);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    int i = 0, size = mItemsS.size();
                    while (i < size) {
                        if (mItemsS.get(i).getShortTitle().equalsIgnoreCase(numberOfRows.getString(0))) {
                            mItemsS.get(i).setDownloadStatus("NO");
                        }
                        i++;
                    }
                    numberOfRows.moveToNext();
                }
                numberOfRows.close();
            }
        }

        mItemsS.get(positionSelected).setDownloadStatus("NO");

        ((PurchaseDownload) mContext).runOnUiThread(new Runnable() {

            public void run() {

                notifyDataSetChanged();

            }
        });


        db_conn.closeDb();
    }

}

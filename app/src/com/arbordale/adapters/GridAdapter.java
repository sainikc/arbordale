package com.arbordale.adapters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.arbordale.beans.GridImageList;
import com.arbordale.beans.GridImgpickerData;
import com.arbordale.beans.HeaderData;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.reader.CurlActivity;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.UtilMethods;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

/**
 * @param <T>
 * @author Tonic Artos
 */
public class GridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter, Filterable {

    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;

    private List<HeaderData> mItems;
    private List<GridImageList> mItems1;
    private List<GridImgpickerData> mItemsS;
    private float scale;
    private RelativeLayout.LayoutParams lparams;

    GridFilter gridFilter;

    AQuery aq;
    Context mContext;

    private double densityX, densityY;

    public GridAdapter(Context context, List<HeaderData> items, List<GridImageList> items1, List<GridImgpickerData> itemss, int headerResId, int itemResId, int width, int height, double densityX, double densityY) {
        init(context, items, items1, itemss, headerResId, itemResId, width, height, densityX, densityY);
        scale = mContext.getResources().getDisplayMetrics().density;
    }

    private void init(Context context, List<HeaderData> items, List<GridImageList> items1, List<GridImgpickerData> itemss, int headerResId, int itemResId, int width, int height, double densityX, double densityY) {
        this.mItems = items;
        this.mItems1 = items1;
        this.mItemsS = itemss;
        this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.mContext = context;
        this.densityX = densityX;
        this.densityY = densityY;

        aq = new AQuery(context);

        mInflater = LayoutInflater.from(context);
        lparams = new RelativeLayout.LayoutParams(width, height);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return mItems1 != null ? mItems1.size() : 0;
    }


    @Override
    public ArrayList<String> getItem(int position) {
        return mItems1.get(position).getSectionNameList();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mItemResId, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.grid_img);
            
           /* if(scale==2.0)
            {}
            
            else*/
            holder.imageView.setLayoutParams(lparams);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (mItemsS.get(position).getStatus().equalsIgnoreCase("YES") && mItemsS.get(position).getDownloadStatus().equalsIgnoreCase("YES"))
            holder.imageView.setImageResource(R.drawable.green_check);

        else if (mItemsS.get(position).getStatus().equalsIgnoreCase("YES") && mItemsS.get(position).getDownloadStatus().equalsIgnoreCase("NO"))
            holder.imageView.setImageResource(android.R.color.transparent);

        else
            holder.imageView.setImageResource(R.drawable.contextual_image);


        //   System.out.println("image required is "+mItemsS.get(position).getShortTitle()+"_132"+AppConstants.IMAGE_FORMAT_PNG);

        try {
            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mContext, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_ENGLISH)) {
                //if(scale==2.0)
                holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs_high_density/" + mItemsS.get(position).getShortTitle() + "_132" + AppConstants.IMAGE_FORMAT_PNG, densityX, densityY));

        		/*else
        			{
        		     holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs_high_density/"+mItemsS.get(position).getShortTitle()+"_132"+AppConstants.IMAGE_FORMAT_PNG));
			         }*/
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mContext, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
        		/*if(scale==2.0)*/
                holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs_high_density/" + mItemsS.get(position).getShortTitle() + "ES_132" + AppConstants.IMAGE_FORMAT_PNG, densityX, densityY));
  			    
        		/*else
        		    holder.imageView.setBackgroundDrawable(UtilMethods.getAssetImage(mContext, "thumbs_high_density/"+mItemsS.get(position).getShortTitle()+"_132"+AppConstants.IMAGE_FORMAT_PNG));
  			  */
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.imageView.setScaleType(ScaleType.FIT_END);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mItemsS.get(position).getDownloadStatus().equalsIgnoreCase("YES")) {
                    if (UtilMethods.isConnectionAvailable(mContext)) {
                        CurlActivity.currentIndex = 0;
                        Intent it = new Intent(mContext, CurlActivity.class);
                        String chaptername = mItemsS.get(position).getShortTitle().replace(AppConstants.IMAGE_FORMAT_PNG, "");
                        String chapternameFinal = chaptername.replace("_132", "");
                        it.putExtra("chaptername", chapternameFinal);
                        it.putExtra("colorcode", mItemsS.get(position).getColorCode());
                        it.putExtra("book_full_name", mItemsS.get(position).getBookFullName());
                        it.putExtra("no_of_pages", mItemsS.get(position).getNoOfPages());
                        it.putExtra("mode", mItemsS.get(position).getStatus());
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(it);
                        ((HomeScreen) mContext).finish();
                    } else {
                        UtilMethods.createToast(mContext, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                } else {
                    Intent it = new Intent(mContext, CurlActivity.class);
                    String chaptername = mItemsS.get(position).getShortTitle().replace(AppConstants.IMAGE_FORMAT_PNG, "");
                    String chapternameFinal = chaptername.replace("_132", "");
                    it.putExtra("chaptername", chapternameFinal);
                    it.putExtra("colorcode", mItemsS.get(position).getColorCode());
                    it.putExtra("no_of_pages", mItemsS.get(position).getNoOfPages());
                    it.putExtra("book_full_name", mItemsS.get(position).getBookFullName());
                    it.putExtra("mode", mItemsS.get(position).getStatus());
                    it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(it);
                    ((HomeScreen) mContext).finish();
                }
            }
        });

        return convertView;
    }


    protected class HeaderViewHolder {
        public TextView textView;
    }

    protected class ViewHolder {
        public ImageView imageView;
    }

    public int getNumHeaders() {
        return mItems.size();
    }

    @Override
    public int getCountForHeader(int header) {
        return mItems1.get(header).getSectionNameList().size();
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        HeaderViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mHeaderResId, parent, false);
            holder = new HeaderViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.top_grid_title);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in string
        holder.textView.setText("" + mItems.get(position).getSectionName());

        return convertView;
    }

    @Override
    public Filter getFilter() {

        if (gridFilter == null)
            gridFilter = new GridFilter();

        return gridFilter;
    }

    private class GridFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                filterResults.values = mItemsS;
                filterResults.count = mItemsS.size();
            } else {
                // We perform filtering operation
                List<GridImgpickerData> nPlanetList = new ArrayList<GridImgpickerData>();

                for (int i = 0; i < mItemsS.size(); i++) {
                    if (mItemsS.get(i).getSdKeywords().toUpperCase().contains(constraint.toString().toUpperCase())
                            || mItemsS.get(i).getSdAnimals().toUpperCase().contains(constraint.toString().toUpperCase())
                            || mItemsS.get(i).getShortTitle().toUpperCase().contains(constraint.toString().toUpperCase())
                            || mItemsS.get(i).getAuthor().toUpperCase().contains(constraint.toString().toUpperCase())
                            || mItemsS.get(i).getIllustrator().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        nPlanetList.add(mItemsS.get(i));
                    }
                }

                filterResults.values = nPlanetList;
                filterResults.count = nPlanetList.size();

            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
            if (results.count == 0) {
                notifyDataSetInvalidated();
            } else {

                mItemsS = (List<GridImgpickerData>) results.values;

                List<ArrayList<String>> l1 = new ArrayList<ArrayList<String>>();

                l1.clear();

                for (int i = 0; i < mItems1.size(); i++) {
                    ArrayList<String> lnew = new ArrayList<String>();
                    for (GridImgpickerData gp : mItemsS) {
                        for (String gl : mItems1.get(i).getSectionNameList()) {
                            if (gl.equalsIgnoreCase(gp.getShortTitle())) {
                                lnew.add(gp.getShortTitle());
                            }
                        }
                    }
                    l1.add(lnew);
                }

                for (int j = 0; j < mItems1.size(); j++) {
                    mItems1.get(j).setSectionNameList(l1.get(j));
                }

                notifyDataSetChanged();
            }
        }

    }

}

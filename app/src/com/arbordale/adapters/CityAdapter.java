package com.arbordale.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arbordale.home.R;
import com.arbordale.jsonparse.SateData;
import com.arbordale.jsonparse.SchoolData;

public class CityAdapter extends ArrayAdapter<String> {
	  private final Activity context;
	  private final ArrayList<SchoolData> countryList;

	  static class ViewHolder {
	    public TextView text;
	  }

	  public CityAdapter(Activity context, ArrayList<SchoolData> countryList) {
		super(context, R.layout.spinner_item,R.id.txt);
	    this.context = context;
	    this.countryList = countryList;
	  }

      @SuppressLint("InflateParams")
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    View rowView = convertView;
	    // reuse views
	    if (rowView == null) {
	      LayoutInflater inflater = context.getLayoutInflater();
	      rowView = inflater.inflate(R.layout.spinner_item, null);
	      // configure view holder
	      ViewHolder viewHolder = new ViewHolder();
	      viewHolder.text = (TextView) rowView.findViewById(R.id.txt);
	      rowView.setTag(viewHolder);
	    }

	    // fill data
	    ViewHolder holder = (ViewHolder) rowView.getTag();
	    holder.text.setText(countryList.get(position).getSchoolName());

	    return rowView;
	  }
      
      @Override
  	public int getCount() {
  		return countryList.size();
  	}
      
      @Override
    	public String getItem(int position) {
    		// TODO Auto-generated method stub
    		return countryList.get(position).getSchoolName();
    	}
	} 

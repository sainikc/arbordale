package com.arbordale.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class DisabledScrollView extends ScrollView {

    public DisabledScrollView(Context context) {
		super(context);
	}

	private boolean mIsDisable = false;

    // if status is true, disable the ScrollView
    public void setDisableStatus(boolean status) {
        mIsDisable = status;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // no more tocuh events for this ScrollView
        if (mIsDisable) {
            return false;
        }       
        return super.onTouchEvent(ev);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        // although the ScrollView doesn't get touch events , its children will get them so intercept them.
        if (mIsDisable) {
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }

}
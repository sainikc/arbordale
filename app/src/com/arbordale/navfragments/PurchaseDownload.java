package com.arbordale.navfragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.WindowManager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.arbordale.adapters.DownloadPurchaseAdapter;
import com.arbordale.beans.GridImageList;
import com.arbordale.beans.GridImgpickerData;
import com.arbordale.beans.HeaderData;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import java.util.ArrayList;

public class PurchaseDownload extends SherlockFragmentActivity {

	private StickyGridHeadersGridView listView;
	private ArrayList<HeaderData> headerData=new ArrayList<HeaderData>();
	private ArrayList<GridImageList> imglistData1=new ArrayList<GridImageList>();
	private ArrayList<GridImgpickerData> imglistData=new ArrayList<GridImgpickerData>();
	private DownloadPurchaseAdapter adapter;
	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.purchase_download_ebook);
		
		actionBar = getSupportActionBar();
	    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
	    actionBar.setTitle("Manage eBooks");
	    actionBar.setIcon(R.drawable.splash_drawable);
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		listView = (StickyGridHeadersGridView) findViewById(R.id.purchase_list);
		
		//listView.setKeepScreenOn(true);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		UtilMethods.executeAsyncTask(new ListDataTask(PurchaseDownload.this), "");
	}
	
    
	public class ListDataTask extends AsyncTask<Object , Integer , String>{

		Context context;
		
		public ListDataTask(Context context) {
	        this.context = context;
	    }
		
		@Override
		protected String doInBackground(Object... params) {
			
			headerData.clear();
			imglistData1.clear();
			imglistData.clear();
			
			SqliteDb db_conn=new SqliteDb(PurchaseDownload.this);
			
            Cursor cr_set_books=db_conn.fetchData(QueryManager.QUERY_FETCH_BOOK_SETS,new String [] {QueryManager.PARAM_SET});
			
			HeaderData setData=new HeaderData();
			GridImageList setList=new GridImageList();
			ArrayList<String> setSections=new ArrayList<String>();
			
			if(cr_set_books !=null && cr_set_books.getCount()>0)
			{
				cr_set_books.moveToFirst();
				setData.setSectionCount(cr_set_books.getCount());
				setData.setSectionName("Multi-eBook Downloads");
				while(!cr_set_books.isAfterLast())
				{
					setSections.add(cr_set_books.getString(0));
					
					GridImgpickerData gm=new GridImgpickerData();
					gm.setShortTitle(cr_set_books.getString(1));
					gm.setBookFullName(cr_set_books.getString(0));
					gm.setShortDescription(cr_set_books.getString(2));
					gm.setStatus(cr_set_books.getString(3));
					gm.setDownloadStatus(cr_set_books.getString(4));
					gm.setSection("Set");
					imglistData.add(gm);
					
					cr_set_books.moveToNext();
				}
			
				
				setList.setSectionNameList(setSections);
				imglistData1.add(setList);
				headerData.add(setData);
			}
			cr_set_books.close();
			
			Cursor cr_fall_books=db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASE,new String [] {QueryManager.PARAM_SPRING_2017,QueryManager.PARAM_FALL,QueryManager.PARAM_SPRING_2015});
			
			HeaderData fallData=new HeaderData();
			GridImageList fallList=new GridImageList();
			ArrayList<String> sections=new ArrayList<String>();
			
			if(cr_fall_books !=null && cr_fall_books.getCount()>0)
			{
				cr_fall_books.moveToFirst();
				fallData.setSectionCount(cr_fall_books.getCount());
				fallData.setSectionName("New Releases");
				while(!cr_fall_books.isAfterLast())
				{
					sections.add(cr_fall_books.getString(0));
					GridImgpickerData gm=new GridImgpickerData();
					gm.setShortTitle(cr_fall_books.getString(0));
					gm.setStatus(cr_fall_books.getString(1));
					gm.setBookFullName(cr_fall_books.getString(2));
					gm.setDownloadStatus(cr_fall_books.getString(3));
					gm.setShortDescription(cr_fall_books.getString(4));
					gm.setNoOfPages(cr_fall_books.getString(5));
					gm.setColorCode(cr_fall_books.getString(6));
					imglistData.add(gm);
					
					cr_fall_books.moveToNext();
				}
				
				
				fallList.setSectionNameList(sections);
				imglistData1.add(fallList);
				headerData.add(fallData);
			}
			cr_fall_books.close();


			System.out.println("DATA --> "+headerData.get(0).getSectionCount()+"/"+imglistData.size());
			
            Cursor cr_other_books=db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_OTHERS_PURCHASE,new String [] {QueryManager.PARAM_FALL,QueryManager.PARAM_SPRING_2014,QueryManager.PARAM_SET,QueryManager.PARAM_SPRING_2015});
			
			HeaderData otherData=new HeaderData();
			GridImageList otherList=new GridImageList();
			ArrayList<String> otherSections=new ArrayList<String>();
			
			if(cr_other_books != null && cr_other_books.getCount()>0)
			{
				cr_other_books.moveToFirst();
				otherData.setSectionCount(cr_other_books.getCount());
				otherData.setSectionName("Additional Titles");
				while(!cr_other_books.isAfterLast())
				{
					otherSections.add(cr_other_books.getString(0));
					
					GridImgpickerData gm=new GridImgpickerData();
					gm.setShortTitle(cr_other_books.getString(0));
					gm.setStatus(cr_other_books.getString(1));
					gm.setBookFullName(cr_other_books.getString(2));
					gm.setDownloadStatus(cr_other_books.getString(3));
					gm.setShortDescription(cr_other_books.getString(4));
					gm.setNoOfPages(cr_other_books.getString(5));
					gm.setColorCode(cr_other_books.getString(6));
					imglistData.add(gm);
					
					cr_other_books.moveToNext();
				}
				
				
				otherList.setSectionNameList(otherSections);
				headerData.add(otherData);
				imglistData1.add(otherList);
			}
			cr_other_books.close();
			if(db_conn.isOpenOrclose())
			{
			  db_conn.closeDb();
			}	
			
			System.out.println("DATA --> "+headerData.get(1).getSectionCount()+"/"+imglistData.size());
			
			return "";
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result){
			adapter=new DownloadPurchaseAdapter(PurchaseDownload.this,headerData,imglistData1,imglistData,R.layout.header_layout,R.layout.list_item_layout);
			listView.setAdapter(adapter);
			
		}

		@Override
		protected void onPreExecute(){
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		 switch (item.getItemId()) {
		    
		    case android.R.id.home:
		    	onBackPressed();
	           break;
		 }
		 return true;
	}
	
	@Override
	public void onBackPressed() {
		
		GlobalVars.BACK_FROM_PURCHASE_DOWNLOAD="true";
		GlobalVars.HOME_READER_PRESSED="false";
		
		Intent it=new Intent(PurchaseDownload.this,HomeScreen.class);
		it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(it);
		
		finish();
	}
	
}

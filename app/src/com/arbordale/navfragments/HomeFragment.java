package com.arbordale.navfragments;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.util.AppConstants;
import com.arbordale.util.UtilMethods;


public class HomeFragment extends Fragment{

	private ImageView splashimg;
	private List<String> imagesForSplash;
	private String randomImgPath;
	private HomeScreen homeActivity;
	private Handler handler;
	private TextView img_overlay;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			homeActivity=(HomeScreen)activity;
		} catch (Exception e) {
		}
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View v= inflater.inflate(R.layout.splash_screen, container, false);
		splashimg=(ImageView) v.findViewById(R.id.splashscreen);
		img_overlay=(TextView)v.findViewById(R.id.img_overlay);
		
		img_overlay.setVisibility(View.GONE);
		
		
		handler=new Handler();
		
		homeActivity.setFullscreenMode();
		
		try {
			imagesForSplash=UtilMethods.getImage(getActivity(), AppConstants.ASSET_HOME);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		randomImgPath=UtilMethods.randomImage(imagesForSplash);
		
		splashimg.setImageBitmap(UtilMethods.getBitmapFromAssets(AppConstants.ASSET_HOME+"/"+randomImgPath, getActivity()));
		
        callHandler();
        
        splashimg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handler.removeCallbacks(r);
				
				if(UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EMAIL).equalsIgnoreCase("")
				   &&UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE).equalsIgnoreCase("")
				   &&UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_DIALOG_SEEN).equalsIgnoreCase(""))
				{
					homeActivity.moveToRegistration();
				}
				
				else
				{
				homeActivity.attachGridFragment();
				}	
			}
		});
        
		return v;
	}
	
	public static HomeFragment newInstance(int position) {
		
		HomeFragment pageFragment = new HomeFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("frag_pos", position);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}
	
	 
	private void callHandler()
	{
		handler.postDelayed(r,AppConstants.HOME_DISPLAY_LENGHT);
	}
	
	Runnable r=new Runnable(){
        @Override
        public void run() {
        	
        	randomImgPath=UtilMethods.randomImage(imagesForSplash);
    		
        	splashimg.refreshDrawableState();
    		splashimg.setScaleType(ScaleType.FIT_XY);
    		splashimg.setImageBitmap(UtilMethods.getBitmapFromAssets(AppConstants.ASSET_HOME+"/"+randomImgPath, getActivity()));
    		callHandler();
        }
    };
    
    @Override
    public void onStop() {
    	super.onStop();
    	
    	if(r!=null&&handler!=null)
    	  handler.removeCallbacks(r);
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	if(r!=null&&handler!=null)
        	handler.removeCallbacks(r);
    }
}

package com.arbordale.navfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.ui.PageLoadingView;
import com.arbordale.util.GlobalVars;

public class WebViewFragment extends Fragment{

	PageLoadingView mLoadingView;
	boolean running = false;
	public static int progress;
	private HomeScreen homeActivity;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			homeActivity=(HomeScreen)activity;
		} catch (Exception e) {
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View v= inflater.inflate(R.layout.webview_fragment, container, false);
		WebView webview=(WebView) v.findViewById(R.id.webview_panel);
		mLoadingView = (PageLoadingView) v.findViewById(R.id.pageprogressStudy);
		
		homeActivity.setFullscreenMode();
		System.out.println("WebView Fragment Running");
		webview.getSettings().setJavaScriptEnabled(true);
		webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

		final Thread s = new Thread(r);
		webview.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				Integer temp = newProgress;
				if (temp != null) {
					progress = (int) (3.6 * temp);
					if (!running) {
						try {
							s.join();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						running = true;
					}
					if (progress >= 360)
						{
						mLoadingView.setVisibility(View.INVISIBLE);
						}
				}
			}
		});
		
	    webview.setWebViewClient(new WebViewClient() {
	    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
	     }
	    });
	    
	    webview.loadUrl(GlobalVars.URL);

		return v;
	}
	
	public static WebViewFragment newInstance(int position) {
		
		WebViewFragment pageFragment = new WebViewFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("frag_pos", position);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if (!mLoadingView.isSpinning) {
			mLoadingView.spin();
			mLoadingView.setVisibility(View.VISIBLE);
		}
	}
	
	 final Runnable r = new Runnable() {
			public void run() {
				while (progress <= 360) {
					running = true;
					mLoadingView.incrementProgress(progress);
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				mLoadingView.setVisibility(View.INVISIBLE);
				running = false;
			}
		}; 

}
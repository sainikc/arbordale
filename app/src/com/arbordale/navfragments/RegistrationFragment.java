package com.arbordale.navfragments;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.arbordale.database.SqliteDb;
import com.arbordale.home.AsyncTaskCompleteListener;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.home.WebServiceCallTask;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;

public class RegistrationFragment extends Fragment implements AsyncTaskCompleteListener<String> {

	EditText firstName,lastName,email,password,confirmPassword;
	Button signUp,continueWithoutSignup,link_to_login;
	ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
	WebServiceCallTask webCall;
	private HomeScreen mActivity;
	private TextView label_top,last_label;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View v= inflater.inflate(R.layout.registration_fragment, container, false);
		
		firstName=(EditText) v.findViewById(R.id.first_name);
		lastName=(EditText) v.findViewById(R.id.last_name);
		email=(EditText) v.findViewById(R.id.email);
		password=(EditText) v.findViewById(R.id.password);
		confirmPassword=(EditText) v.findViewById(R.id.confirm_password);
		
		label_top=(TextView) v.findViewById(R.id.label_top);
		last_label=(TextView) v.findViewById(R.id.last_label);
		
		
		label_top.setText(Html.fromHtml("<font color=\"#000000\">I am</font> <font color=\"#F49020\"><b>new to Arbordale</b></font>"), BufferType.SPANNABLE);
		
		if(UtilMethods.isKindleFire())
		  {
			last_label.setClickable(false);
			last_label.setEnabled(false);
			last_label.setText(Html.fromHtml("<font color=\"#000000\">Have questions? Please call 877-243-3457 (toll free) or</font> <a href=\"#\">info@arbordalepublishing.com</a>"));
		  }

		else
		{
		  last_label.setClickable(true);	
		  last_label.setAutoLinkMask(2);
		  last_label.setText(Html.fromHtml("<font color=\"#000000\">Have questions? Please call 877-243-3457 (toll free) or</font> <a href=\"mailto:info@arbordalepublishing.com\">info@arbordalepublishing.com</a>"));
		}
		
		signUp=(Button)v.findViewById(R.id.sign_up);
		continueWithoutSignup=(Button)v.findViewById(R.id.btn_continue_without_signup);
		link_to_login=(Button)v.findViewById(R.id.link_to_login);
		
		link_to_login.setVisibility(View.GONE);
		mActivity.clearFullscreenMode();
		
		link_to_login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mActivity.replaceWithLoginFragment();
			}
		});
		
		signUp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(UtilMethods.isConnectionAvailable(getActivity()))
				{
				data.clear();
				
				data.add(new BasicNameValuePair("new","1"));
				data.add(new BasicNameValuePair("e",email.getText().toString()));
				data.add(new BasicNameValuePair("p",password.getText().toString()));
				data.add(new BasicNameValuePair("f",firstName.getText().toString()));
				data.add(new BasicNameValuePair("l",lastName.getText().toString()));
				
				
				if(firstName.getText().toString().length()!=0
						&&lastName.getText().toString().length()!=0
						&&email.getText().toString().length()!=0
						&&password.getText().toString().length()!=0
						&&confirmPassword.getText().toString().length()!=0)
				{
				   if(UtilMethods.isValidEmail(email.getText().toString())&&UtilMethods.passwordsMatch(password.getText().toString(), confirmPassword.getText().toString()))	
				    {
					 GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_REGISTER;
					 GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;  
					 webCall=new WebServiceCallTask(getActivity(),RegistrationFragment.this);
				     webCall.execute(AppConstants.SERVICE_VALIDATE, data);
				     IBinder windowToken=getActivity().getCurrentFocus().getWindowToken();
				     UtilMethods.hidekeyboard(getActivity(),windowToken); 
				    }
				   
				   else
				   {
					   if(!UtilMethods.isValidEmail(email.getText().toString()))
					    {
						   UtilMethods.createToast(getActivity(), AppMessaging.EMAIL_INVALID);
					    }
					   
					   if(!UtilMethods.passwordsMatch(password.getText().toString(), confirmPassword.getText().toString()))
					   {
						   UtilMethods.createToast(getActivity(), AppMessaging.PASSWORDS_DOESNT_MATCH);
					   }  
				   } 
				}
				
				else
				{
				   UtilMethods.createToast(getActivity(), AppMessaging.CANT_BE_BLANK);
				}
			  }
				
			  else
			  {
				  UtilMethods.createToast(getActivity(), AppMessaging.NO_INTERNET_CONNECTION);
			  }	
			}
		});
		
		continueWithoutSignup.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				mActivity.attachGridFragment();
			}
		});
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			mActivity=(HomeScreen)activity;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public static RegistrationFragment newInstance(int position) {
		RegistrationFragment pageFragment = new RegistrationFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("frag_pos", position);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}

	@Override
	public void onTaskComplete(String result) {
		
		if(result.equalsIgnoreCase("-1"))
		{
			UtilMethods.createToast(getActivity(), AppMessaging.CONNECTION_TIMEOUT);
		}
		
		else if(result.equalsIgnoreCase(AppConstants.STATUS_SUCCESS))
		   {
			
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_PASSWORD, password.getText().toString());
			
			data.clear();
			
			data.add(new BasicNameValuePair("e",email.getText().toString()));
			data.add(new BasicNameValuePair("p",password.getText().toString()));
			data.add(new BasicNameValuePair("info",""));
			GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_REGISTER;
			GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;  
			webCall=new WebServiceCallTask(getActivity(),RegistrationFragment.this);
			webCall.execute(AppConstants.SERVICE_VALIDATE, data);
			
		   }
		
		else if(result.equalsIgnoreCase(AppConstants.STATUS_EMAIL_EXISTS))
		   UtilMethods.createToast(getActivity(),AppMessaging.REG_ALREADY_REGISTERED);
		
		else if(result.split("\\s+").length==6)
		{
			String arr[]=result.split("\\s+");
			
            SqliteDb db_conn=new SqliteDb(getActivity());
			
			Cursor users=db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);
			
			if(users.getCount()==1)
			{
				users.moveToFirst();
				
				ContentValues cv=new ContentValues();
				cv.put("username", arr[2]);
				cv.put("password", password.getText().toString());
				cv.put("first_name", arr[0]);
				cv.put("last_name", arr[1]);
				cv.put("Email", arr[2]);
				cv.put("Require_login", arr[3]);
				cv.put("Display_data_usage", arr[4]);
				cv.put("Language", arr[5]);
				
				db_conn.updateData("users", cv, "username='"+users.getString(0)+"'");
			}
			
			users.close();
			
			db_conn.closeDb();
			
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_FIRST_NAME, arr[0]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LAST_NAME, arr[1]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EMAIL, arr[2]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_REQUIRE_LOGIN,arr[3]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_DISPLAY_DATA_USAGE, arr[4]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LANGUAGE, arr[5]);
			
			UtilMethods.createToast(getActivity(),AppMessaging.REG_SUCCESS);
			mActivity.attachGridFragment();
		}	
	}
	
}

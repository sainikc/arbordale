package com.arbordale.navfragments;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;

import com.arbordale.util.AppConstants;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;


public class UnZipper extends Observable {

    private static final String TAG = "UnZip";
    private String mFileName, mFilePath, mDestinationPath;
    private String fileToDelete = "";

    public UnZipper(String fileName, String filePath, String destinationPath) {
        mFileName = fileName;
        mFilePath = filePath;
        mDestinationPath = destinationPath;
    }

    public String getFileName() {
        return mFileName;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public String getDestinationPath() {
        return mDestinationPath;
    }

    public void unzip() {
        String fullPath = mFilePath + "/" + mFileName + ".zip";
        //Log.d(TAG, "unzipping " + mFileName + " to " + mDestinationPath);
        new UnZipTask().execute(fullPath, mDestinationPath);
    }

    private class UnZipTask extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("rawtypes")
        @Override
        protected Boolean doInBackground(String... params) {

            System.out.println("Folder to unzip " + params[0]);

            String filePath = params[0];
            String destinationPath = params[1];

            File archive = new File(filePath);
            try {
                ZipFile zipfile = new ZipFile(archive);
                for (Enumeration e = zipfile.entries(); e.hasMoreElements(); ) {
                    ZipEntry entry = (ZipEntry) e.nextElement();
                    unzipEntry(zipfile, entry, destinationPath);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error while extracting file " + archive, e);
                return false;
            }

            return true;
        }

        private String obfuscateFileExtension(String fileEntry) {
            if (fileEntry.contains(".mp3")) {
                return fileEntry.replace(".mp3", ".a");
            } else if (fileEntry.contains(".jpg")) {
                return fileEntry.replace(".jpg", ".b");
            }
            return fileEntry;
        }


        @Override
        protected void onPostExecute(Boolean result) {

            File SDCardRoot = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER);
            File file = new File(SDCardRoot, getFileName() + ".zip");
            if (file.exists()) {
                file.delete();
                System.out.println("file deleted");
            }
            setChanged();
            notifyObservers();
        }

        private void unzipEntry(ZipFile zipfile, ZipEntry entry,
                                String outputDir) throws IOException {

            if (entry.isDirectory()) {
                createDir(new File(outputDir, entry.getName()));
                return;
            }

            File outputFile = new File(outputDir, obfuscateFileExtension(entry.getName()));
            if (!outputFile.getParentFile().exists()) {
                createDir(outputFile.getParentFile());
            }

            //Log.v(TAG, "Extracting: " + entry);
            BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

            try {
                IOUtils.copy(inputStream, outputStream);
            } finally {
                outputStream.close();
                inputStream.close();
            }
        }

        private void createDir(File dir) {
            if (dir.exists()) {
                return;
            }
            Log.v(TAG, "Creating dir " + dir.getName());
            if (!dir.mkdirs()) {
                throw new RuntimeException("Can not create dir " + dir);
            } else {
                try {
                    File file = new File(dir.getAbsolutePath() + File.separator + ".nomedia");
                    file.createNewFile();
                } catch (IOException e) {
                    throw new RuntimeException("Can not create .nomedia " + dir);
                }
            }
        }
    }
} 
package com.arbordale.navfragments;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.arbordale.adapters.CityAdapter;
import com.arbordale.adapters.CountryAdapter;
import com.arbordale.adapters.LibraryAdapter;
import com.arbordale.adapters.StateAdapter;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.AsyncTaskCompleteListener;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.home.WebServiceCallTask;
import com.arbordale.jsonparse.CountryData;
import com.arbordale.jsonparse.Library;
import com.arbordale.jsonparse.SateData;
import com.arbordale.jsonparse.SchoolData;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;


public class LoginFragment extends Fragment implements AsyncTaskCompleteListener<String>,OnItemSelectedListener{

	private String schoolCode="",schoolPass="",libraryDigitsToMatch="",librarySiteLicenseCode="";
	int libraryDigitsExpected=0;
	private ArrayList<String> digitsToMatch=new ArrayList<String>(20);
	
	EditText email,password,siteCode,txtLibraryCode;
	Button login,siteCodeLogin,link_to_registration,loginPublicLibrary;
	ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
	WebServiceCallTask webCall;
	private static int step=0,sitecode=0,publicLibrary=0;
	private HomeScreen mActivity;
	private TextView label1,label2,last_label,label_3;
	private static String code;
	
	private ArrayList<CountryData> countryList=new ArrayList<CountryData>();
	private ArrayList<SateData> stateList;
	private ArrayList<SchoolData> schoolList;
	private ArrayList<Library> libraryList;
	
	private CountryAdapter cAdapter;
	private StateAdapter sAdapter;
	private LibraryAdapter lAdapter;
	private CityAdapter ctAdapter;
	
	private Spinner txtCountry,txtState,txtPublicLibraryName;
	private Spinner txtCountrys,txtStates,txtPublicLibraryNames;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View v= inflater.inflate(R.layout.login_fragment, container, false);
		
		mActivity.clearFullscreenMode();
		
		email=(EditText) v.findViewById(R.id.email);
		password=(EditText) v.findViewById(R.id.password);
		password.setTypeface(Typeface.DEFAULT);
		password.setTransformationMethod(new PasswordTransformationMethod());
		
		siteCode=(EditText) v.findViewById(R.id.site_code);
		siteCode.setTypeface(Typeface.DEFAULT);
		siteCode.setTransformationMethod(new PasswordTransformationMethod());
		txtLibraryCode=(EditText) v.findViewById(R.id.txtLibraryCode);
		txtLibraryCode.setTypeface(Typeface.DEFAULT);
		txtLibraryCode.setTransformationMethod(new PasswordTransformationMethod());
		login=(Button) v.findViewById(R.id.login);
		siteCodeLogin=(Button) v.findViewById(R.id.login_site_code);
		link_to_registration=(Button) v.findViewById(R.id.link_to_registration);
		loginPublicLibrary=(Button) v.findViewById(R.id.loginPublicLibrary);
		link_to_registration.setVisibility(View.GONE);
		
		label1=(TextView) v.findViewById(R.id.top_label);
		label2=(TextView) v.findViewById(R.id.label_2);
		label_3=(TextView) v.findViewById(R.id.label_3);
		
		txtCountry=(Spinner) v.findViewById(R.id.txtCountry);
		txtState=(Spinner) v.findViewById(R.id.txtState);
		txtPublicLibraryName=(Spinner) v.findViewById(R.id.txtPublicLibraryName);

		txtCountrys=(Spinner) v.findViewById(R.id.txtCountrys);
		txtStates=(Spinner) v.findViewById(R.id.txtStates);
		txtPublicLibraryNames=(Spinner) v.findViewById(R.id.txtPublicLibraryNames);

		txtCountry.setOnItemSelectedListener(this);
		txtState.setOnItemSelectedListener(this);
		txtPublicLibraryName.setOnItemSelectedListener(this);

		txtCountrys.setOnItemSelectedListener(this);
		txtStates.setOnItemSelectedListener(this);
		txtPublicLibraryNames.setOnItemSelectedListener(this);
		
		txtCountry.setPrompt("Select a Country");
		txtState.setPrompt("Select a State");
		txtPublicLibraryName.setPrompt("Select a Library");

		txtCountrys.setPrompt("Select a Country");
		txtStates.setPrompt("Select a State");
		txtPublicLibraryNames.setPrompt("Select a Library");
		
		last_label=(TextView) v.findViewById(R.id.last_label);
		
		Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("mailto:" + "info@arbordalepublishing.com"));
		PackageManager manager = getActivity().getPackageManager();
		List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
		if (infos.size() > 0) {
			System.out.println("email client present");
		    Linkify.addLinks(last_label, Linkify.EMAIL_ADDRESSES);
		    last_label.setAutoLinkMask(Linkify.EMAIL_ADDRESSES);
		}else{
		   //UtilMethods.createToast(getActivity(), "No Email Client Configured!");
		}
		
		
		label1.setText(Html.fromHtml("<font color=\"#000000\">I am</font> <font color=\"#1BB04B\"><b>already a Arbordale customer</b></font> <font color=\"#000000\">and would like to access my existing</font> <font color=\"#1BB04B\"><b>Personal eLibrary</b></font> <font color=\"#000000\">and previously purchased eBooks</font>"), BufferType.SPANNABLE);
		label2.setText(Html.fromHtml("<font color=\"#000000\">I would like to access eBooks through my</font> <font color=\"#ED1C24\"><b>school\'s</b></font> <font color=\"#000000\">eBook Site License</b></font>"));
		label_3.setText(Html.fromHtml("<font color=\"#000000\">I would like to access eBooks through my</font> <font color=\"#ED1C24\"><b>public library\'s</b></font><font color=\"#000000\"> eBook Site License</b></font>"));
		if(UtilMethods.isKindleFire())
		  {
			last_label.setClickable(false);
			last_label.setEnabled(false);
			last_label.setText(Html.fromHtml("<font color=\"#000000\">Have questions? Please call 877-243-3457 (toll free) or</font> <a href=\"#\">info@arbordalepublishing.com</a>"));
		  }

		else
		{
		  last_label.setClickable(true);	
		  last_label.setAutoLinkMask(2);
		  last_label.setText(Html.fromHtml("<font color=\"#000000\">Have questions? Please call 877-243-3457 (toll free) or</font> <a href=\"mailto:info@arbordalepublishing.com\">info@arbordalepublishing.com</a>"));
		}
		
		
		login.setTextColor(Color.WHITE);
		siteCodeLogin.setTextColor(Color.WHITE);
		
		mActivity.clearFullscreenMode();
		
		link_to_registration.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				System.out.println("clicked");
				mActivity.replaceWithRegistrationFragment();
			}
		});
		
		login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(UtilMethods.isConnectionAvailable(getActivity()))
				{	
				step=0;
				sitecode=0;
				data.clear();
				
				data.add(new BasicNameValuePair("e",email.getText().toString()));
				data.add(new BasicNameValuePair("p",password.getText().toString()));
				data.add(new BasicNameValuePair("info",""));
				
				if(email.getText().toString().length()!=0
						&&password.getText().toString().length()!=0)
				{
				   if(UtilMethods.isValidEmail(email.getText().toString()))	
				    {
					 GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_1;
					 GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
					 webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				     webCall.execute(AppConstants.SERVICE_VALIDATE, data);
				     
				     IBinder windowToken=getActivity().getCurrentFocus().getWindowToken();
				     UtilMethods.hidekeyboard(getActivity(),windowToken);
				    }
				   
				   else
				   {
					   if(!UtilMethods.isValidEmail(email.getText().toString()))
					    {
						   UtilMethods.createToast(getActivity(), AppMessaging.EMAIL_INVALID);
					    }
				   } 
				}
				
				else
				{
					UtilMethods.createToast(getActivity(), AppMessaging.CANT_BE_BLANK);
				}	
			  }
				
			  else
			  {
					UtilMethods.createToast(getActivity(), AppMessaging.NO_INTERNET_CONNECTION);
			  }	
			}
		 });
		
		siteCodeLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(UtilMethods.isConnectionAvailable(getActivity()))
				{
				step=0;
				sitecode=0;
				publicLibrary=0;
				data.clear();

				
				
				if(siteCode.getText().toString().length()!=0){
					{
						if(siteCode.getText().toString().equalsIgnoreCase(schoolCode)||siteCode.getText().toString().equalsIgnoreCase(schoolPass))
						{	
						
						 data.add(new BasicNameValuePair("c",schoolCode));
						 data.add(new BasicNameValuePair("s","ANY"));
							
						 GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_1;
						 GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;	
						 webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
						 webCall.execute(AppConstants.SERVICE_VALIDATE, data);
						}
						
						else
						{
							UtilMethods.createToast(getActivity(), AppMessaging.WRONG_SITE_CODE);
						}	
						 
					}	
				 	
				 sitecode=1;
				 
				 IBinder windowToken=getActivity().getCurrentFocus().getWindowToken();
			     UtilMethods.hidekeyboard(getActivity(),windowToken);
				}
				
				else
				{
					UtilMethods.createToast(getActivity(), AppMessaging.SITE_CODE_BLANK);
				}
			   }
				
			   else
			   {
						UtilMethods.createToast(getActivity(), AppMessaging.NO_INTERNET_CONNECTION);
			   }	
			}
		 });
		
		loginPublicLibrary.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(UtilMethods.isConnectionAvailable(getActivity())){
				step=0;
				sitecode=0;
				publicLibrary=0;
				data.clear();
				
				
				if(txtLibraryCode.getText().toString().length()==libraryDigitsExpected){
				
					if(Integer.parseInt(libraryDigitsToMatch.trim())==0||matchDigits(txtLibraryCode.getText().toString()))
					{
					  data.add(new BasicNameValuePair("c",librarySiteLicenseCode));
					  data.add(new BasicNameValuePair("s","ANY"));
						
				      GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_1;
				      GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;	
				      webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				      webCall.execute(AppConstants.SERVICE_VALIDATE, data);
					}
					
					else 
					{
						UtilMethods.createToast(getActivity(), AppMessaging.WRONG_Library_code);
					}
					
				 publicLibrary=1;
				 
				 IBinder windowToken=getActivity().getCurrentFocus().getWindowToken();
			     UtilMethods.hidekeyboard(getActivity(),windowToken);
				}
				
				else if(txtLibraryCode.getText().toString().length()==0)
				{
					UtilMethods.createToast(getActivity(), AppMessaging.LIBRARY_CODE_BLANK);
				}
				
				else 
				{
					UtilMethods.createToast(getActivity(), AppMessaging.WRONG_Library_code);
				}

			   }
				
			   else
			   {
						UtilMethods.createToast(getActivity(), AppMessaging.NO_INTERNET_CONNECTION);
			   }	
			}
		 });
		
		siteCode.setOnKeyListener(new View.OnKeyListener() {
		    public boolean onKey(View v, int keyCode, KeyEvent event) {
		        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
		        	siteCodeLogin.performClick();
		          return true;
		        }
		        return false;
		    }
		});
		
		email.setOnKeyListener(new View.OnKeyListener() {
		    public boolean onKey(View v, int keyCode, KeyEvent event) {
		        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
		        	login.performClick();
		          return true;
		        }
		        return false;
		    }
		});
		
		password.setOnKeyListener(new View.OnKeyListener() {
		    public boolean onKey(View v, int keyCode, KeyEvent event) {
		        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
		        	login.performClick();
		          return true;
		        }
		        return false;
		    }
		});
		
		new GetAllStates().execute();
		
		txtLibraryCode.setEnabled(false);
		loginPublicLibrary.setEnabled(false);
		
		siteCodeLogin.setEnabled(false);
		siteCode.setEnabled(false);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			mActivity=(HomeScreen)activity;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static LoginFragment newInstance(int position) {
		
		LoginFragment pageFragment = new LoginFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("frag_pos", position);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}

	@Override
	public void onTaskComplete(String result) {
		
		//System.out.println("value of result is "+result);
		
		if(result.equalsIgnoreCase(AppConstants.STATUS_WRONG_EMAIL))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_WRONG_EMAIL);
		}
		
		else if(result.equalsIgnoreCase(AppConstants.STATUS_WRONG_PASSWORD))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_WRONG_PASSWORD);
		}	
		
		else if(result.equalsIgnoreCase(AppConstants.STATUS_WRONG_SITE_CODE))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_WRONG_SITE_CODE);
		}	
		else if(result.equalsIgnoreCase(AppConstants.STATUS_ERROR_SITE_CODE))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_WRONG_SITE_CODE);
		}	
		else if(result.equalsIgnoreCase(AppConstants.STATUS_INVALID))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_INVALID_MESSAGE);
		}
		else if(result.equalsIgnoreCase(AppConstants.STATUS_NO))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_NO_MESSAGE);
		}
		else if(result.equalsIgnoreCase(AppConstants.STATUS_BADSETUP ))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_BADSETUP_MESSAGE);
		}
		else if(result.equalsIgnoreCase(AppConstants.STATUS_BADCARDNO  ))
		{
			UtilMethods.createToast(getActivity(), AppConstants.STATUS_BADCARDNO_MESSAGE);
		}
		
		else if(result.equalsIgnoreCase("-1"))
		{
			UtilMethods.createToast(getActivity(), AppMessaging.CONNECTION_TIMEOUT);
		}
		
		else
		{	
		boolean checkjson=false;
		Object json = null;
		try {
			json = new JSONTokener(result).nextValue();
			if (json instanceof JSONObject)
			{
				checkjson=true;
			}
			else
			{
				checkjson=false;
			}	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("checkjson : "+checkjson);
		
		if((step==0||step==1)&&sitecode==0 && publicLibrary==0)	{
		
		if(step==0)
		 {	
		  String arr[]=result.split("\\t");
		  String[] splitname = null;
		  UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_PASSWORD, password.getText().toString());

          SqliteDb db_conn=new SqliteDb(getActivity());
			
		  Cursor users=db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);
			
			if(users.getCount()==1)
			{
				users.moveToFirst();
				
				splitname=arr[0].split("\\s");
				
				ContentValues cv=new ContentValues();
				cv.put("username", arr[1]);
				cv.put("password", password.getText().toString());
				cv.put("first_name", splitname[0]);
				cv.put("last_name", splitname[1]);
				cv.put("Email", arr[1]);
				cv.put("Organization_name", arr[2]);
				cv.put("City", arr[3]);
				cv.put("State", arr[4]);
				cv.put("Require_login", arr[5]);
				cv.put("Display_data_usage", arr[6]);
				cv.put("Language", arr[7]);
				cv.put("Expiry_Date", arr[8]);
				
				db_conn.updateData("users", cv, "username='"+users.getString(0)+"'");
			}
			
			users.close();
			
			db_conn.closeDb();
			
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_FIRST_NAME, splitname[0]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LAST_NAME, splitname[1]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EMAIL, arr[1]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_REQUIRE_LOGIN,arr[5]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_DISPLAY_DATA_USAGE, arr[6]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LANGUAGE, arr[7]);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS1, AppConstants.VALUE_EBOOK_1_1);
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS2, AppConstants.VALUE_EBOOK_2_1);

		  
		  data.clear();
			
		  data.add(new BasicNameValuePair("e",email.getText().toString()));
		  data.add(new BasicNameValuePair("p",password.getText().toString()));
		  data.add(new BasicNameValuePair("list",""));
		  GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_2;
		  GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
		  webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
		  webCall.execute(AppConstants.SERVICE_VALIDATE, data);

		  System.out.println("info result "+result);

		  step++;
		 }
		
		else if(step==1)
		{
	        if(json instanceof JSONArray)
			{
			    System.out.println("data is json array");	
			}
			
			else if(json instanceof JSONObject)
			{
				SqliteDb db_conn=new SqliteDb(getActivity());
				
				JSONObject jsonObj;
				try {
					jsonObj = new JSONObject(json.toString());
					Iterator<?> keys=jsonObj.keys();
					while(keys.hasNext())
					{
						String key =(String)keys.next();
						
						if(key.equals("ALL TITLES"))
						{
							ContentValues cv=new ContentValues();
							cv.put("Status", "YES");
							db_conn.updateData("book_icon", cv, "");
						}
						
						else
						{	
						ContentValues cv=new ContentValues();
						cv.put("Status", "YES");
						db_conn.updateData("book_icon", cv, "Section="+DatabaseUtils.sqlEscapeString(key)+"");
							
						ContentValues cv1=new ContentValues();
						cv1.put("Status", "YES");
						db_conn.updateData("book_icon", cv1, "Book_name="+DatabaseUtils.sqlEscapeString(key)+"");
						
						ContentValues cv2=new ContentValues();
						cv2.put("Expiry_date", "Various");
						db_conn.updateData("users", cv2, "Email='"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EMAIL)+"'");
						}
					}	
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				db_conn.closeDb();
			}	
			
			else{
				System.out.println("data is not json");	
			}	
		    mActivity.attachHomeFragment();
		 
		  step++;
		 }
	   }
		
	   else if((step==0||step==1||step==2||step==3)&&sitecode==1 && publicLibrary==0){
		   System.out.println("SiteCode : "+sitecode);
			if(result.equalsIgnoreCase(AppConstants.STATUS_SUCCESS)&&step==0){   
				System.out.println("Step is zero");
				  data.clear();
					
				  data.add(new BasicNameValuePair("c",schoolCode));
				  data.add(new BasicNameValuePair("info",""));
				
				  GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_2;
				  GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
				  
				  webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				  webCall.execute(AppConstants.SERVICE_VALIDATE, data);
				  step++;
		    }
			
			else if(step==1){	
				System.out.println("Step is 1");
			  String arr[]=result.split("\\t");
			  
			  int l=arr.length,i=0;
			  
			  while(l>0)
			  {
				 System.out.println("array elements "+arr[i]);
				 l--;
				 i++;
			  }

			  
			  UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE, schoolCode);

	          SqliteDb db_conn=new SqliteDb(getActivity());
				
			  Cursor users=db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);
				
				if(users.getCount()==1)
				{
					users.moveToFirst();
					
					ContentValues cv=new ContentValues();
					cv.put("username", "");
					cv.put("password", "");
					cv.put("Email", "");
					cv.put("last_name", "");
					cv.put("first_name", arr[0]);
					cv.put("City", arr[1]);
					cv.put("State", arr[2]);
					cv.put("Require_login", arr[3]);
					cv.put("Display_data_usage", arr[4]);
					cv.put("Language", arr[5]);
					cv.put("Expiry_date", arr[6]);
					cv.put("site_code", siteCode.getText().toString());
					
					db_conn.updateData("users", cv, "username='"+users.getString(0)+"'");
				}
				users.close();
				
				db_conn.closeDb();
				
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_FIRST_NAME, arr[0]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_CITY, arr[1]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_STATE, arr[2]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_REQUIRE_LOGIN,arr[3]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_DISPLAY_DATA_USAGE, arr[4]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LANGUAGE, arr[5]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EXPIRY_DATE, arr[6]);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS1, AppConstants.VALUE_EBOOK_1_1);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS2, AppConstants.VALUE_EBOOK_2_1);
		
				step++;  
			}
			
			if(step==2)
			{
				System.out.println("Step is 2");
				  data.clear();
					
				  data.add(new BasicNameValuePair("c",schoolCode));
				  data.add(new BasicNameValuePair("list",""));
				  GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_3;
				  GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
				  webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				  webCall.execute(AppConstants.SERVICE_VALIDATE, data);
				  step++;
			}
			
			if(step==3&&checkjson==true)
			{
				System.out.println("Step is 3 and checkjson is true");
		        if(json instanceof JSONArray)
				{
				    System.out.println("data is json array");	
				}
				
				else if(json instanceof JSONObject)
				{
					SqliteDb db_conn=new SqliteDb(getActivity());
					
					JSONObject jsonObj;
					try {
						jsonObj = new JSONObject(json.toString());
						Iterator<?> keys=jsonObj.keys();
						while(keys.hasNext())
						{
							String key =(String)keys.next();
							
							if(key.equals("ALL TITLES"))
							{
								ContentValues cv=new ContentValues();
								cv.put("Status", "YES");
								db_conn.updateData("book_icon", cv, "");
								
								ContentValues cv1=new ContentValues();
								cv1.put("Expiry_date", jsonObj.getString(key));
								db_conn.updateData("users", cv1, "site_code='"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE)+"'");
							}
							
							else
							{	
							ContentValues cv=new ContentValues();
							cv.put("Status", "YES");
							db_conn.updateData("book_icon", cv, "Section="+DatabaseUtils.sqlEscapeString(key)+"");
								
							ContentValues cv1=new ContentValues();
							cv1.put("Status", "YES");
							db_conn.updateData("book_icon", cv1, "Book_name="+DatabaseUtils.sqlEscapeString(key)+"");
							
							ContentValues cv2=new ContentValues();
							cv2.put("Expiry_date", "Various");
							db_conn.updateData("users", cv2, "site_code='"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE)+"'");
							}
						}	
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					db_conn.closeDb();
				}	
				
				else
				{
					System.out.println("data is not json");	
				}	
			 mActivity.attachHomeFragment();
		   }	
		}	else if((step==0||step==1||step==2||step==3)&&publicLibrary==1 && sitecode==0){
			//String []resultValue=result.split(":");
			if(result !=null && result.length()>0 && result.trim().equalsIgnoreCase("success") && step==0){
				 data.clear();
					
				  data.add(new BasicNameValuePair("c",librarySiteLicenseCode));
				  data.add(new BasicNameValuePair("info",""));
				  code=librarySiteLicenseCode;
				  GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_2;
				  GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
				  
				  webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				  try {
					  
					  String value=webCall.execute(AppConstants.SERVICE_VALIDATE, data).get();
						System.out.println("Values : "+value);
						String []userInfo=value.split("\\t");
						if(userInfo != null && userInfo.length>0){
							UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE, librarySiteLicenseCode);

					          SqliteDb db_conn=new SqliteDb(getActivity());
								
							  Cursor users=db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);
								
								if(users.getCount()==1)
								{
									users.moveToFirst();
									
									ContentValues cv=new ContentValues();
									cv.put("username", "");
									cv.put("password", "");
									cv.put("Email", "");
									cv.put("last_name", "");
									cv.put("first_name", userInfo[0]);
									cv.put("City", userInfo[1]);
									cv.put("State", userInfo[2]);
									cv.put("Require_login", userInfo[3]);
									cv.put("Display_data_usage", userInfo[4]);
									cv.put("Language", userInfo[5]);
									cv.put("Expiry_date", userInfo[6]);
									cv.put("site_code", librarySiteLicenseCode);
									
									db_conn.updateData("users", cv, "username='"+users.getString(0)+"'");
								}
								users.close();
								
								db_conn.closeDb();
								
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_FIRST_NAME, userInfo[0]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_CITY, userInfo[1]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_STATE, userInfo[2]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_REQUIRE_LOGIN,userInfo[3]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_DISPLAY_DATA_USAGE, userInfo[4]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_LANGUAGE, userInfo[5]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EXPIRY_DATE, userInfo[6]);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS1, AppConstants.VALUE_EBOOK_1_1);
								UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_EBOOKS2, AppConstants.VALUE_EBOOK_2_1);
						
								step++;  
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			else if(step==1){
				 data.clear();
					
				  data.add(new BasicNameValuePair("c",code));
				  data.add(new BasicNameValuePair("list",""));
				  GlobalVars.WEB_DIALOG_TITLE=GlobalVars.WEB_CALL_DIALOG_TITLE_LOGIN_3;
				  GlobalVars.WEB_DIALOG_MSG=GlobalVars.TXT_LOADING;
				  webCall=new WebServiceCallTask(getActivity(),LoginFragment.this);
				  webCall.execute(AppConstants.SERVICE_VALIDATE, data);
				  step++;
			}
			else if(step==2 && checkjson==true){
				System.out.println("Step is 3 and checkjson is true");
		        if(json instanceof JSONArray)
				{
				    System.out.println("data is json array");	
				}
				
				else if(json instanceof JSONObject)
				{
					SqliteDb db_conn=new SqliteDb(getActivity());
					
					JSONObject jsonObj;
					try {
						jsonObj = new JSONObject(json.toString());
						Iterator<?> keys=jsonObj.keys();
						while(keys.hasNext())
						{
							String key =(String)keys.next();
							
							if(key.equals("ALL TITLES"))
							{
								ContentValues cv=new ContentValues();
								cv.put("Status", "YES");
								db_conn.updateData("book_icon", cv, "");
								
								ContentValues cv1=new ContentValues();
								cv1.put("Expiry_date", jsonObj.getString(key));
								db_conn.updateData("users", cv1, "site_code='"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE)+"'");
							}
							
							else
							{	
							ContentValues cv=new ContentValues();
							cv.put("Status", "YES");
							db_conn.updateData("book_icon", cv, "Section="+DatabaseUtils.sqlEscapeString(key)+"");
								
							ContentValues cv1=new ContentValues();
							cv1.put("Status", "YES");
							db_conn.updateData("book_icon", cv1, "Book_name="+DatabaseUtils.sqlEscapeString(key)+"");
							
							ContentValues cv2=new ContentValues();
							cv2.put("Expiry_date", "Various");
							db_conn.updateData("users", cv2, "site_code='"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, getActivity(), AppConstants.KEY_SITECODE)+"'");
							}
						}	
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					db_conn.closeDb();
				}	
				
				else
				{
					System.out.println("data is not json");	
				}	
			 mActivity.attachHomeFragment();
		   }
		 }
	   }
	}	
	
	private class GetAllStates extends AsyncTask<Void, Void, String>{
		private String result=null;
		private ProgressDialog progressDialog;

		@Override
		protected String doInBackground(Void... params) {
			
			//
			
			try {	

				File yourFile = new File(Environment.getExternalStorageDirectory(), "Arbordale/logdata.json");
	            FileInputStream stream = new FileInputStream(yourFile);
	            String jsonStr = null;
	            try {
	                FileChannel fc = stream.getChannel();
	                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

	                jsonStr = Charset.defaultCharset().decode(bb).toString();
	              }
	              finally {
	                stream.close();
	              }
	            
                JSONObject jsonObj = new JSONObject(jsonStr);

                JSONObject countryData= jsonObj.getJSONObject("CountryData");
                
                JSONArray countries  = countryData.getJSONArray("Country");


                CountryData cdata=new CountryData();
    			cdata.setCountryName("Select Country");
    			countryList.add(cdata);
    			
                // looping through All nodes
                for (int i = 0; i < countries.length(); i++) {
                	
                	CountryData cData = new CountryData();
                	
                    JSONObject c = countries.getJSONObject(i);

                    String countryCode = c.getString("CountryCode");
                    String countryName = c.getString("CountryName");
                    
                    cData.setCountryCode(countryCode);
                    cData.setCountryName(countryName);
                    
                    JSONArray states=null;
                    
                    if(c.has("States"))
                    {	
                    states = c.getJSONArray("States");
                    
                    stateList=new ArrayList<SateData>();

                    for (int j = 0; j < states.length(); j++) {
                    	
                    	SateData sData = new SateData();

                    	String stateCode = states.getJSONObject(j).getString("StateCode");
                        String stateName = states.getJSONObject(j).getString("StateName");
                        
                        sData.setStateCode(stateCode);
                        sData.setStateName(stateName);
                        
                        stateList.add(sData);
                        
                        
                        JSONArray schools=null;
                        
                        if(states.getJSONObject(j).has("Schools"))
                        {	
                        schools = states.getJSONObject(j).getJSONArray("Schools");
                        
                        schoolList=new ArrayList<SchoolData>();

                        for (int k = 0; k < schools.length(); k++) {

                        	SchoolData scData=new SchoolData();
                        	
                        	String schoolName = schools.getJSONObject(k).getString("SchoolName");
                        	String schoolPassword = schools.getJSONObject(k).getString("SchoolPassword");
                        	String siteLicense = schools.getJSONObject(k).getString("SiteLicenseCode");

                        	scData.setSchoolName(schoolName);
                        	scData.setSchoolPassword(schoolPassword);
                        	scData.setSiteLicenseCode(siteLicense);
                        	
                        	schoolList.add(scData);
                        }
                        sData.setSchools(schoolList);
                       }
                        
                        else
                        {
                        	schoolList = new ArrayList<SchoolData>();
                      	  sData.setSchools(schoolList); 
                        }	
                        
                        JSONArray libraries=null;
                        
                        if(states.getJSONObject(j).has("Libraries"))
                        {	
                        libraries = states.getJSONObject(j).getJSONArray("Libraries");
                        
                        libraryList=new ArrayList<Library>();

                        for (int k = 0; k < libraries.length(); k++) {

                        	Library lData=new Library();
                        	lData.setLibraryName(libraries.getJSONObject(k).getString("LibraryName"));
                        	lData.setLibrarySiteLicenseCode(libraries.getJSONObject(k).getString("LibrarySiteLicenseCode"));
                        	lData.setLibraryDigits2Expect(libraries.getJSONObject(k).getString("LibraryDigits2Expect"));
                        	lData.setLibraryDigits2Match(libraries.getJSONObject(k).getString("LibraryDigits2Match"));
                        	lData.setLibraryMatch1(libraries.getJSONObject(k).getString("LibraryMatch1"));
                        	lData.setLibraryMatch2(libraries.getJSONObject(k).getString("LibraryMatch2"));
                        	lData.setLibraryMatch3(libraries.getJSONObject(k).getString("LibraryMatch3"));
                        	lData.setLibraryMatch4(libraries.getJSONObject(k).getString("LibraryMatch4"));
                        	lData.setLibraryMatch5(libraries.getJSONObject(k).getString("LibraryMatch5"));
                        	lData.setLibraryMatch6(libraries.getJSONObject(k).getString("LibraryMatch6"));
                        	lData.setLibraryMatch7(libraries.getJSONObject(k).getString("LibraryMatch7"));
                        	lData.setLibraryMatch8(libraries.getJSONObject(k).getString("LibraryMatch8"));
                        	lData.setLibraryMatch9(libraries.getJSONObject(k).getString("LibraryMatch9"));
                        	lData.setLibraryMatch10(libraries.getJSONObject(k).getString("LibraryMatch10"));
                        	lData.setLibraryMatch11(libraries.getJSONObject(k).getString("LibraryMatch11"));
                        	lData.setLibraryMatch12(libraries.getJSONObject(k).getString("LibraryMatch12"));
                        	lData.setLibraryMatch13(libraries.getJSONObject(k).getString("LibraryMatch13"));
                        	lData.setLibraryMatch14(libraries.getJSONObject(k).getString("LibraryMatch14"));
                        	lData.setLibraryMatch15(libraries.getJSONObject(k).getString("LibraryMatch15"));
                        	lData.setLibraryMatch16(libraries.getJSONObject(k).getString("LibraryMatch16"));
                        	lData.setLibraryMatch17(libraries.getJSONObject(k).getString("LibraryMatch17"));
                        	lData.setLibraryMatch18(libraries.getJSONObject(k).getString("LibraryMatch18"));
                        	lData.setLibraryMatch19(libraries.getJSONObject(k).getString("LibraryMatch19"));
                        	lData.setLibraryMatch20(libraries.getJSONObject(k).getString("LibraryMatch20"));
                        	libraryList.add(lData);
                        
                        }
                        sData.setLibraries(libraryList);
                      }
                        
                      else
                      {
                    	  libraryList = new ArrayList<Library>();
                    	  sData.setLibraries(libraryList);
                      }	
                     }
                    cData.setStates(stateList);
                    }
                    
                    else
                    {
                     stateList=new ArrayList<SateData>();	
                     cdata.setStates(stateList);
                    }	
                    
                    countryList.add(cData);
                  }
                
			} catch (Exception e) {
	           e.printStackTrace();
	          }
			
			//
			
			
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(progressDialog!=null && progressDialog.isShowing())
				progressDialog.dismiss();
			
			cAdapter = new CountryAdapter(getActivity(), countryList);
			txtCountry.setAdapter(cAdapter);
			txtCountrys.setAdapter(cAdapter);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog= new ProgressDialog(getActivity());
			progressDialog.setTitle("");
			progressDialog.setMessage("Loading ...");
			progressDialog.setCancelable(false);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
		
		
	}
	
	int countryIndex,stateIndex;
	int countryIndexs,stateIndexs;
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch(parent.getId()){
		case R.id.txtCountry:
			if(position!=0&&countryList.get(position).getStates()!=null)
			{
			countryIndex=position;	
			sAdapter = new StateAdapter(getActivity(), countryList.get(position).getStates());
			txtState.setAdapter(sAdapter);
			}
			break;
		
		case R.id.txtState:
			if(position!=0&&countryList.get(countryIndex).getStates().get(position).getLibraries()!=null)
			{
			stateIndex=position;	
			lAdapter = new LibraryAdapter(getActivity(),countryList.get(countryIndex).getStates().get(position).getLibraries());
			txtPublicLibraryName.setAdapter(lAdapter);
			}
			break;

		case R.id.txtPublicLibraryName:
			txtLibraryCode.setEnabled(true);
			loginPublicLibrary.setEnabled(true);
			librarySiteLicenseCode=countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibrarySiteLicenseCode();
			libraryDigitsToMatch=countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryDigits2Match();
			libraryDigitsExpected=Integer.parseInt(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryDigits2Expect());

			digitsToMatch.clear();
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch1());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch2());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch3());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch4());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch5());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch6());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch7());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch8());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch9());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch10());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch11());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch12());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch13());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch14());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch15());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch16());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch17());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch18());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch19());
			digitsToMatch.add(countryList.get(countryIndex).getStates().get(stateIndex).getLibraries().get(position).getLibraryMatch20());

			break;
			
		case R.id.txtCountrys:
			if(position!=0&&countryList.get(position).getStates()!=null)
			{
			countryIndexs=position;	
			sAdapter = new StateAdapter(getActivity(), countryList.get(position).getStates());
			txtStates.setAdapter(sAdapter);
			}
			break;
		
		case R.id.txtStates:
			if(position!=0&&countryList.get(countryIndexs).getStates().get(position).getLibraries()!=null)
			{
			stateIndexs=position;	
			ctAdapter = new CityAdapter(getActivity(),countryList.get(countryIndexs).getStates().get(position).getSchools());
			txtPublicLibraryNames.setAdapter(ctAdapter);
			}
			break;

		case R.id.txtPublicLibraryNames:
			schoolCode=countryList.get(countryIndexs).getStates().get(stateIndexs).getSchools().get(position).getSiteLicenseCode();
			schoolPass=countryList.get(countryIndexs).getStates().get(stateIndexs).getSchools().get(position).getSchoolPassword();

			siteCode.setEnabled(true);
			siteCodeLogin.setEnabled(true);
			break;
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean matchDigits(String enteredData)
	{
		for(String m:digitsToMatch)
		{	
		if(enteredData.substring(0,Integer.parseInt(libraryDigitsToMatch.trim())).equalsIgnoreCase(m))
			return true;
		}
		
		return false;
	}
}

package com.arbordale.navfragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

import com.arbordale.adapters.GridAdapter;
import com.arbordale.beans.GridImageList;
import com.arbordale.beans.GridImgpickerData;
import com.arbordale.beans.HeaderData;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.util.AppConstants;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.NaturalSortOrderComparator1;
import com.arbordale.util.NaturalSortOrderComparator2;
import com.arbordale.util.NaturalSortOrderComparator3;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import java.util.ArrayList;
import java.util.Collections;

public class GridViewFragment extends Fragment {

    private StickyGridHeadersGridView gridView;
    private ArrayList<HeaderData> headerData = new ArrayList<HeaderData>();
    private ArrayList<GridImageList> imglistData1 = new ArrayList<GridImageList>();
    private ArrayList<GridImgpickerData> imglistData = new ArrayList<GridImgpickerData>();
    private GridAdapter gridAdapter;
    float scale;
    int widthOfScreen, heightOfScreen;
    private HomeScreen homeActivity;
    private TextView noBooks;

    private double densityX, densityY = 160;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            homeActivity = (HomeScreen) activity;
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.grid_layout, container, false);
        gridView = (StickyGridHeadersGridView) v.findViewById(R.id.grid_view);
        noBooks = (TextView) v.findViewById(R.id.message);

        homeActivity.setFullscreenMode();

        scale = homeActivity.getResources().getDisplayMetrics().density;
        widthOfScreen = homeActivity.getResources().getDisplayMetrics().widthPixels;
        heightOfScreen = homeActivity.getResources().getDisplayMetrics().heightPixels;

        densityX = homeActivity.getResources().getDisplayMetrics().xdpi;
        densityY = homeActivity.getResources().getDisplayMetrics().ydpi;

        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EMAIL).equalsIgnoreCase("")
                && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_SITECODE).equalsIgnoreCase("")) {
            SqliteDb db_conn = new SqliteDb(homeActivity);

            Cursor check = db_conn.fetchData("select * from users", null);

            if (check != null && check.getCount() > 0) {
                check.moveToFirst();
                if (check.getString(0).equalsIgnoreCase("Default") &&
                        check.getString(3).equalsIgnoreCase("Guest") &&
                        check.getString(4).equalsIgnoreCase("Welcome")) {
                    System.out.println("No changes required");
                } else {
                    System.out.println("changes required");

                    ContentValues cv = new ContentValues();
                    cv.put("Download_status", "NO");
                    cv.put("Status", "PRV");
                    db_conn.updateData("book_icon", cv, null);

                    ContentValues cv1 = new ContentValues();
                    cv1.put("username", "Default");
                    cv1.put("first_name", "Guest");
                    cv1.put("last_name", "Welcome");
                    cv1.put("password", "");
                    cv1.put("site_code", "");
                    cv1.put("Email", "");
                    cv1.put("Organization_name", "");
                    cv1.put("City", "");
                    cv1.put("State", "");
                    cv1.put("Require_login", "");
                    cv1.put("Display_data_usage", "");
                    cv1.put("Language", "");
                    cv1.put("Expiry_date", "");
                    db_conn.updateData("users", cv1, null);
                }

                check.close();
            }

            db_conn.closeDb();

        }

        System.out.println("Home reader pressed " + GlobalVars.HOME_READER_PRESSED);

        if ((GlobalVars.HOME_READER_PRESSED.equals("true")) &&
                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME,
                        homeActivity,
                        AppConstants.KEY_SORT_EBOOKS).equalsIgnoreCase("")) {
            new FetchGridSearchData(homeActivity).execute("");
        } else if ((GlobalVars.HOME_READER_PRESSED.equals("true")) &&
                !UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME,
                        homeActivity,
                        AppConstants.KEY_SORT_EBOOKS).equalsIgnoreCase("")) {
            new FetchGridSort1Data(homeActivity)
                    .execute(UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME,
                            homeActivity,
                            AppConstants.KEY_SORT_EBOOKS));
        }

        GlobalVars.HOME_READER_PRESSED = "false";

        return v;
    }

    public static GridViewFragment newInstance(int position) {

        GridViewFragment pageFragment = new GridViewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("frag_pos", position);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }

    public class FetchGridSearchData extends AsyncTask<Object, Integer, String> {

        Context context;
        private ProgressDialog progress;

        public FetchGridSearchData(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Object... params) {

            headerData.clear();
            imglistData1.clear();
            imglistData.clear();

            SqliteDb db_conn = new SqliteDb(homeActivity);

            //System.out.println("shared prefs "+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1)+"/"+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2));

// 2017 ===================================
            Cursor cr_spring_2017_books = null;

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_spring_2017_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_ALL, new String[]{QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_spring_2017_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_spring_2017_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_spring_2017_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            }


            //=================================


            Cursor cr_spring_2015_books = null;

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                System.out.println("New Release condition 1");
                cr_spring_2015_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_ALL, new String[]{QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                System.out.println("New Release condition 2");
                cr_spring_2015_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                System.out.println(" New Release condition 3");
                cr_spring_2015_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                System.out.println(" New Release condition 4");
                cr_spring_2015_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2017});
            }


            Cursor cr_fall_books = null;

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                System.out.println(" cursor fall book");
                cr_fall_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_ALL, new String[]{QueryManager.PARAM_FALL});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_fall_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_FALL});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_fall_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_FALL});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_fall_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, QueryManager.PARAM_FALL});
            }

            HeaderData fallData = new HeaderData();
            GridImageList fallList = new GridImageList();
            ArrayList<String> sections = new ArrayList<String>();
            System.out.println("Before New Release condition");
            if (cr_spring_2015_books != null)
                System.out.println("cr_spring_2017_books in New Release condition" +
                        cr_spring_2015_books.getCount() + " and Fall book 2017:" + cr_fall_books.getCount());
            if (cr_fall_books.getCount() > 0) {

                cr_fall_books.moveToFirst();

                cr_spring_2015_books.moveToFirst();
                fallData.setSectionCount(cr_fall_books.getCount() + cr_spring_2015_books.getCount());
                fallData.setSectionName("New Releases");

                while (!cr_fall_books.isAfterLast()) {
                    sections.add(cr_fall_books.getString(0));

                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(cr_fall_books.getString(0));
                    gm.setStatus(cr_fall_books.getString(1));
                    gm.setDownloadStatus(cr_fall_books.getString(2));
                    gm.setAccessFlag(cr_fall_books.getString(3));
                    gm.setNoOfPages(cr_fall_books.getString(4));
                    gm.setSdKeywords(cr_fall_books.getString(5));
                    gm.setSdAnimals(cr_fall_books.getString(6));
                    gm.setColorCode(cr_fall_books.getString(7));
                    gm.setAuthor(cr_fall_books.getString(8));
                    gm.setIllustrator(cr_fall_books.getString(9));
                    gm.setBookFullName(cr_fall_books.getString(10));
                    imglistData.add(gm);

                    cr_fall_books.moveToNext();
                }
                cr_fall_books.close();
                while (!cr_spring_2015_books.isAfterLast()) {
                    sections.add(cr_spring_2015_books.getString(0));

                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(cr_spring_2015_books.getString(0));
                    gm.setStatus(cr_spring_2015_books.getString(1));
                    gm.setDownloadStatus(cr_spring_2015_books.getString(2));
                    gm.setAccessFlag(cr_spring_2015_books.getString(3));
                    gm.setNoOfPages(cr_spring_2015_books.getString(4));
                    gm.setSdKeywords(cr_spring_2015_books.getString(5));
                    gm.setSdAnimals(cr_spring_2015_books.getString(6));
                    gm.setColorCode(cr_spring_2015_books.getString(7));
                    gm.setAuthor(cr_spring_2015_books.getString(8));
                    gm.setIllustrator(cr_spring_2015_books.getString(9));
                    gm.setBookFullName(cr_spring_2015_books.getString(10));
                    imglistData.add(gm);

                    cr_spring_2015_books.moveToNext();
                }
                cr_spring_2015_books.close();


                fallList.setSectionNameList(sections);
                imglistData1.add(fallList);
                headerData.add(fallData);
            }

            //System.out.println("DATA --> "+headerData.get(0).getSectionCount()+"/"+imglistData.size());
            Cursor cr_spring_books = null;

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_spring_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_ALL, new String[]{QueryManager.PARAM_SPRING_2014});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_spring_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2014});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_spring_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2014});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_spring_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, QueryManager.PARAM_SPRING_2014});
            }


            Cursor cr_other_books = null;

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_other_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_OTHERS_ALL, new String[]{QueryManager.PARAM_FALL, QueryManager.PARAM_SPRING_2017, QueryManager.PARAM_SET, QueryManager.PARAM_SPRING_2015});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_other_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_OTHERS_ALL_DOWNLOAD, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_FALL, QueryManager.PARAM_SPRING_2014, QueryManager.PARAM_SET, QueryManager.PARAM_SPRING_2015});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                cr_other_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_OTHERS_ALL_PURCHASED, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_FALL, QueryManager.PARAM_SPRING_2014, QueryManager.PARAM_SET, QueryManager.PARAM_SPRING_2015});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                cr_other_books = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOKS_OTHERS_ALL_PURCHASED_DOWNLOAD, new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, QueryManager.PARAM_FALL, QueryManager.PARAM_SPRING_2014, QueryManager.PARAM_SET, QueryManager.PARAM_SPRING_2015});
            }

            HeaderData otherData = new HeaderData();
            GridImageList otherList = new GridImageList();
            ArrayList<String> otherSections = new ArrayList<String>();

            if (cr_other_books.getCount() > 0) {
                cr_other_books.moveToFirst();
                cr_spring_books.moveToFirst();
                otherData.setSectionCount(cr_other_books.getCount() + cr_spring_books.getCount());
                otherData.setSectionName("Additional Titles");
                while (!cr_spring_books.isAfterLast()) {
                    otherSections.add(cr_spring_books.getString(0));
                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(cr_spring_books.getString(0));
                    gm.setStatus(cr_spring_books.getString(1));
                    gm.setDownloadStatus(cr_spring_books.getString(2));
                    gm.setAccessFlag(cr_spring_books.getString(3));
                    gm.setNoOfPages(cr_spring_books.getString(4));
                    gm.setSdKeywords(cr_spring_books.getString(5));
                    gm.setSdAnimals(cr_spring_books.getString(6));
                    gm.setColorCode(cr_spring_books.getString(7));
                    gm.setAuthor(cr_spring_books.getString(8));
                    gm.setIllustrator(cr_spring_books.getString(9));
                    gm.setBookFullName(cr_spring_books.getString(10));
                    imglistData.add(gm);

                    cr_spring_books.moveToNext();
                }
                cr_spring_books.close();
                while (!cr_other_books.isAfterLast()) {
                    otherSections.add(cr_other_books.getString(0));

                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(cr_other_books.getString(0));
                    gm.setStatus(cr_other_books.getString(1));
                    gm.setDownloadStatus(cr_other_books.getString(2));
                    gm.setAccessFlag(cr_other_books.getString(3));
                    gm.setNoOfPages(cr_other_books.getString(4));
                    gm.setSdKeywords(cr_other_books.getString(5));
                    gm.setSdAnimals(cr_other_books.getString(6));
                    gm.setColorCode(cr_other_books.getString(7));
                    gm.setAuthor(cr_other_books.getString(8));
                    gm.setIllustrator(cr_other_books.getString(9));
                    gm.setBookFullName(cr_other_books.getString(10));
                    imglistData.add(gm);

                    cr_other_books.moveToNext();
                }
                cr_other_books.close();

                otherList.setSectionNameList(otherSections);
                headerData.add(otherData);
                imglistData1.add(otherList);
            }

            if (db_conn.isOpenOrclose()) {
                db_conn.closeDb();
            }

            //System.out.println("DATA --> "+headerData.get(1).getSectionCount()+"/"+imglistData.size());

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                progress.dismiss();
                progress = null;
            } catch (Exception e) {
                // nothing
            }

            if (headerData.size() == 0 && imglistData1.size() == 0 && imglistData.size() == 0) {
                noBooks.setVisibility(View.VISIBLE);
            } else {
                noBooks.setVisibility(View.GONE);
            }

            int height = 0, width = 0;

            try {
            /*BitmapDrawable bd;

			if(scale>1.0)
			   bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs_high_density/ABC_132.png");
			
			else
				bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs/ABC_132.png");
			 
		    height=bd.getBitmap().getHeight();
			width=bd.getBitmap().getWidth();*/

                width = (int) (0.732 * densityX);
                height = (int) (0.967 * densityY);

                //System.out.println("image width/height | Screen width/height | NumberOfColumns | HorizontalSpacing | ColumnWidth :"+width+"/"+height+"|"+widthOfScreen+"/"+heightOfScreen+"|"+(widthOfScreen/width)+"|"+(widthOfScreen%width)+"|"+width);

                int widthOfGrid = gridView.getWidth();
                int numOfCols = widthOfGrid / width;
                int horizontalSpacing = (widthOfGrid % width) / numOfCols;
                int columnWidth = width + horizontalSpacing;

                if (horizontalSpacing < 10) {
                    numOfCols = (widthOfGrid / width) - 1;
                    horizontalSpacing = (width + (widthOfGrid % width)) / numOfCols;
                    columnWidth = width + horizontalSpacing;

                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                } else {
                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                }

                System.out.println("number of columns /Horizontal Spacing/Column Width : " + numOfCols + "/" + horizontalSpacing + "/" + columnWidth);
			
			/*
			if(((widthOfScreen%width)/(widthOfScreen/width))<10)
			{
				gridView.setNumColumns((widthOfScreen/width)-1);
				
				System.out.println("gridview width is "+gridView.getWidth());
				gridView.setHorizontalSpacing(((widthOfScreen%width)/((widthOfScreen/width)-1)));
				gridView.setColumnWidth(width);
			}
			
			else
			{	
			gridView.setNumColumns((widthOfScreen/width));
			gridView.setHorizontalSpacing(((widthOfScreen%width)/(widthOfScreen/width)));
			gridView.setColumnWidth(width);
			}*/


            } catch (Exception p) {
            }

            //System.out.println("new horizontalspacing width "+(widthOfScreen%width)+":"+gridView.getNumColumns());

            gridAdapter = new GridAdapter(homeActivity, headerData, imglistData1, imglistData, R.layout.header_layout_grid, R.layout.item_layout, width, height, densityX, densityY);
            gridView.setAdapter(gridAdapter);

        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setTitle(getString(R.string.ebooks));
            progress.setMessage(GlobalVars.TXT_LOADING);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }
    }

    public class FetchGridSearchDataNoCategory extends AsyncTask<Object, Integer, String> {

        Context context;
        public String filter = "";

        public FetchGridSearchDataNoCategory(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Object... params) {

            filter = (String) params[0];

            headerData.clear();
            imglistData1.clear();
            imglistData.clear();

            SqliteDb db_conn = new SqliteDb(homeActivity);

            Cursor no_category = null;

            no_category = db_conn.fetchData(QueryManager.QUERY_FETCH_BOOK_NO_CATEGORY, new String[]{QueryManager.PARAM_SET});

            HeaderData noCategoryData = new HeaderData();
            GridImageList fallList = new GridImageList();
            ArrayList<String> sections = new ArrayList<String>();

            if (no_category.getCount() > 0) {
                no_category.moveToFirst();
                noCategoryData.setSectionCount(no_category.getCount());
                noCategoryData.setSectionName("Search...");

                while (!no_category.isAfterLast()) {
                    sections.add(no_category.getString(0));

                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(no_category.getString(0));
                    gm.setStatus(no_category.getString(1));
                    gm.setDownloadStatus(no_category.getString(2));
                    gm.setAccessFlag(no_category.getString(3));
                    gm.setNoOfPages(no_category.getString(4));
                    gm.setSdKeywords(no_category.getString(5));
                    gm.setSdAnimals(no_category.getString(6));
                    gm.setColorCode(no_category.getString(7));
                    gm.setAuthor(no_category.getString(8));
                    gm.setIllustrator(no_category.getString(9));
                    gm.setBookFullName(no_category.getString(10));
                    imglistData.add(gm);

                    no_category.moveToNext();
                }
                no_category.close();

                fallList.setSectionNameList(sections);
                imglistData1.add(fallList);
                headerData.add(noCategoryData);
            }


            if (db_conn.isOpenOrclose()) {
                db_conn.closeDb();
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            if (headerData.size() == 0 && imglistData1.size() == 0 && imglistData.size() == 0) {
                noBooks.setVisibility(View.VISIBLE);
            } else {
                noBooks.setVisibility(View.GONE);
            }

            int height = 0, width = 0;

            try {
			/*BitmapDrawable bd;
			
			if(scale>1.0)
			   bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs_high_density/ABC_132.png");
			
			else
				bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs/ABC_132.png");
			 
		    height=bd.getBitmap().getHeight();
			width=bd.getBitmap().getWidth();
			*/

                width = (int) (0.732 * densityX);
                height = (int) (0.967 * densityY);

                int widthOfGrid = gridView.getWidth();
                int numOfCols = widthOfGrid / width;
                int horizontalSpacing = (widthOfGrid % width) / numOfCols;
                int columnWidth = width + horizontalSpacing;

                if (horizontalSpacing < 10) {
                    numOfCols = (widthOfGrid / width) - 1;
                    horizontalSpacing = (width + (widthOfGrid % width)) / numOfCols;
                    columnWidth = width + horizontalSpacing;

                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                } else {
                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                }

                System.out.println("number of columns /Horizontal Spacing/Column Width : " + numOfCols + "/" + horizontalSpacing + "/" + columnWidth);
			
			/*System.out.println("image width/height | Screen width/height | NumberOfColumns | HorizontalSpacing | ColumnWidth :"+width+"/"+height+"|"+widthOfScreen+"/"+heightOfScreen+"|"+(widthOfScreen/width)+"|"+(widthOfScreen%width)+"|"+width);
			gridView.setNumColumns((widthOfScreen/width));
			gridView.setHorizontalSpacing(((widthOfScreen%width)/(widthOfScreen/width)));
			gridView.setColumnWidth(width);*/
            } catch (Exception p) {
            }

            gridAdapter = new GridAdapter(homeActivity, headerData, imglistData1, imglistData, R.layout.header_layout_grid, R.layout.item_layout, width, height, densityX, densityY);
            gridView.setAdapter(gridAdapter);

            gridAdapter.getFilter().filter(filter);

        }

        @Override
        protected void onPreExecute() {
        }
    }

    public class FetchGridSort2Datas extends AsyncTask<String, Integer, String> {

        Context context;
        private ProgressDialog progress;

        public FetchGridSort2Datas(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            headerData.clear();
            imglistData1.clear();
            imglistData.clear();

            SqliteDb db_conn = new SqliteDb(homeActivity);

            Cursor cursor_further = null;
            //SPRING SET 2017
            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                System.out.println("Query running QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL");
                cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL, new String[]{params[0]});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                System.out.println("Query running QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_DOWNLOAD");
                cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_DOWNLOAD, new String[]{params[0], QueryManager.PARAM_YES});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                System.out.println("Query running QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED");
                cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED, new String[]{params[0], QueryManager.PARAM_YES});
            } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                System.out.println("Query running QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASE_DOWNLOAD");
                cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED_DOWNLOAD, new String[]{params[0], QueryManager.PARAM_YES, QueryManager.PARAM_YES});
            }


            HeaderData hData = new HeaderData();
            GridImageList imgList = new GridImageList();
            ArrayList<String> sections = new ArrayList<String>();

            if (cursor_further.getCount() > 0) {
                cursor_further.moveToFirst();
                hData.setSectionCount(cursor_further.getCount());
                hData.setSectionName(params[1]);
                System.out.println("Header : " + params[1]);
                while (!cursor_further.isAfterLast()) {
                    sections.add(cursor_further.getString(0));

                    GridImgpickerData gm = new GridImgpickerData();
                    gm.setShortTitle(cursor_further.getString(0));
                    System.out.println("Short Title :" + cursor_further.getString(0));
                    gm.setStatus(cursor_further.getString(4));
                    gm.setDownloadStatus(cursor_further.getString(5));
                    gm.setAccessFlag(cursor_further.getString(7));
                    gm.setNoOfPages(cursor_further.getString(6));
                    gm.setSdKeywords(cursor_further.getString(7));
                    gm.setSdAnimals(cursor_further.getString(8));
                    gm.setColorCode(cursor_further.getString(10));
                    gm.setAuthor(cursor_further.getString(11));
                    gm.setIllustrator(cursor_further.getString(12));
                    gm.setBookFullName(cursor_further.getString(13));
                    imglistData.add(gm);

                    cursor_further.moveToNext();
                }
                cursor_further.close();

                imgList.setSectionNameList(sections);
                imglistData1.add(imgList);
                headerData.add(hData);
            }

            //System.out.println("DATA --> "+headerData.get(0).getSectionCount()+"/"+imglistData.size());

            if (db_conn.isOpenOrclose()) {
                db_conn.closeDb();
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                progress.dismiss();
                progress = null;
            } catch (Exception e) {
                // nothing
            }

            if (headerData.size() == 0 && imglistData1.size() == 0 && imglistData.size() == 0) {
                noBooks.setVisibility(View.VISIBLE);
            } else {
                noBooks.setVisibility(View.GONE);
            }

            int height = 0, width = 0;

            try {
			/*BitmapDrawable bd;
			
			if(scale>1.0)
			   bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs_high_density/ABC_132.png");
			
			else
				bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs/ABC_132.png");
			 
		    height=bd.getBitmap().getHeight();
			width=bd.getBitmap().getWidth();*/

                width = (int) (0.732 * densityX);
                height = (int) (0.967 * densityY);

                int widthOfGrid = gridView.getWidth();
                int numOfCols = widthOfGrid / width;
                int horizontalSpacing = (widthOfGrid % width) / numOfCols;
                int columnWidth = width + horizontalSpacing;

                if (horizontalSpacing < 10) {
                    numOfCols = (widthOfGrid / width) - 1;
                    horizontalSpacing = (width + (widthOfGrid % width)) / numOfCols;
                    columnWidth = width + horizontalSpacing;

                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                } else {
                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                }

                System.out.println("number of columns /Horizontal Spacing/Column Width : " + numOfCols + "/" + horizontalSpacing + "/" + columnWidth);

			
/*			System.out.println("image width/height | Screen width/height | NumberOfColumns | HorizontalSpacing | ColumnWidth :"+width+"/"+height+"|"+widthOfScreen+"/"+heightOfScreen+"|"+(widthOfScreen/width)+"|"+(widthOfScreen%width)+"|"+width);
			gridView.setNumColumns((widthOfScreen/width));
			gridView.setHorizontalSpacing(((widthOfScreen%width)/(widthOfScreen/width)));
			gridView.setColumnWidth(width);*/
            } catch (Exception p) {
            }

            gridAdapter = new GridAdapter(homeActivity, headerData, imglistData1, imglistData, R.layout.header_layout_grid, R.layout.item_layout, width, height, densityX, densityY);
            gridView.setAdapter(gridAdapter);
        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setTitle(getString(R.string.ebooks));
            progress.setMessage(GlobalVars.TXT_LOADING);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }
    }

    public class FetchGridSort1Data extends AsyncTask<String, Integer, String> {

        Context context;
        private ProgressDialog progress;

        public FetchGridSort1Data(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            headerData.clear();
            imglistData1.clear();
            imglistData.clear();

            SqliteDb db_conn = new SqliteDb(homeActivity);

            Cursor cursor_outer = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER, new String[]{params[0]});

            if (cursor_outer.getCount() > 0) {
                cursor_outer.moveToFirst();
                while (!cursor_outer.isAfterLast()) {
                    Cursor cursor_further = null;

                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                            && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                        cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL, new String[]{cursor_outer.getString(0)});
                    } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_1)
                            && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                        cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_DOWNLOAD, new String[]{cursor_outer.getString(0), QueryManager.PARAM_YES});
                    } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                            && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_1)) {
                        cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED, new String[]{cursor_outer.getString(0), QueryManager.PARAM_YES});
                    } else if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS1).equalsIgnoreCase(AppConstants.VALUE_EBOOK_1_2)
                            && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, homeActivity, AppConstants.KEY_EBOOKS2).equalsIgnoreCase(AppConstants.VALUE_EBOOK_2_2)) {
                        cursor_further = db_conn.fetchData(QueryManager.QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED_DOWNLOAD, new String[]{cursor_outer.getString(0), QueryManager.PARAM_YES, QueryManager.PARAM_YES});
                    }

                    HeaderData hData = new HeaderData();
                    GridImageList imgList = new GridImageList();
                    ArrayList<String> sections = new ArrayList<String>();

                    hData.setSectionName(cursor_outer.getString(2));

                    if (cursor_further.getCount() > 0) {
                        cursor_further.moveToFirst();
                        hData.setSectionCount(cursor_further.getCount());

                        while (!cursor_further.isAfterLast()) {
                            sections.add(cursor_further.getString(0));

                            GridImgpickerData gm = new GridImgpickerData();
                            gm.setShortTitle(cursor_further.getString(0));
                            gm.setStatus(cursor_further.getString(4));
                            gm.setDownloadStatus(cursor_further.getString(5));
                            gm.setAccessFlag(cursor_further.getString(7));
                            gm.setNoOfPages(cursor_further.getString(6));
                            gm.setSdKeywords(cursor_further.getString(7));
                            gm.setSdAnimals(cursor_further.getString(8));
                            gm.setColorCode(cursor_further.getString(10));
                            gm.setAuthor(cursor_further.getString(11));
                            gm.setIllustrator(cursor_further.getString(12));
                            gm.setBookFullName(cursor_further.getString(13));
                            gm.setSectionName(hData.getSectionName());
                            imglistData.add(gm);

                            cursor_further.moveToNext();
                        }
                        cursor_further.close();

                        imgList.setSectionNameList(sections);
                        imgList.setSectionName(hData.getSectionName());
                        imglistData1.add(imgList);
                        headerData.add(hData);
                    }
                    cursor_outer.moveToNext();
                }
            }
            cursor_outer.close();

            //System.out.println("DATA --> "+headerData.get(0).getSectionCount()+"/"+imglistData.size());

            if (db_conn.isOpenOrclose()) {
                db_conn.closeDb();
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progress.dismiss();
                progress = null;
            } catch (Exception e) {
                // nothing
            }

            if (headerData.size() == 0 && imglistData1.size() == 0 && imglistData.size() == 0) {
                noBooks.setVisibility(View.VISIBLE);
            } else {
                noBooks.setVisibility(View.GONE);
            }


            int height = 0, width = 0;

            try {
			/*BitmapDrawable bd;
			
			if(scale>1.0)
			   bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs_high_density/ABC_132.png");
			
			else
				bd=(BitmapDrawable) UtilMethods.getAssetImage(homeActivity, "thumbs/ABC_132.png");
			 
		    height=bd.getBitmap().getHeight();
			width=bd.getBitmap().getWidth();*/

                width = (int) (0.732 * densityX);
                height = (int) (0.967 * densityY);

                int widthOfGrid = gridView.getWidth();
                int numOfCols = widthOfGrid / width;
                int horizontalSpacing = (widthOfGrid % width) / numOfCols;
                int columnWidth = width + horizontalSpacing;

                if (horizontalSpacing < 10) {
                    numOfCols = (widthOfGrid / width) - 1;
                    horizontalSpacing = (width + (widthOfGrid % width)) / numOfCols;
                    columnWidth = width + horizontalSpacing;

                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                } else {
                    gridView.setNumColumns(numOfCols);
                    gridView.setHorizontalSpacing(horizontalSpacing);
                    gridView.setColumnWidth(columnWidth);
                }

                System.out.println("number of columns /Horizontal Spacing/Column Width : " + numOfCols + "/" + horizontalSpacing + "/" + columnWidth);

                System.out.println("image width/height | Screen width/height | NumberOfColumns | HorizontalSpacing | ColumnWidth :" + width + "/" + height + "|" + widthOfScreen + "/" + heightOfScreen + "|" + (widthOfScreen / width) + "|" + (widthOfScreen % width) + "|" + width);
			/*gridView.setNumColumns((widthOfScreen/width));
			gridView.setHorizontalSpacing(((widthOfScreen%width)/(widthOfScreen/width)));
			gridView.setColumnWidth(width);*/
            } catch (Exception p) {
            }

            Collections.sort(headerData, new NaturalSortOrderComparator1());
            Collections.sort(imglistData1, new NaturalSortOrderComparator2());
            Collections.sort(imglistData, new NaturalSortOrderComparator3());

            for (GridImgpickerData a : imglistData)
                System.out.println("imagelistdata1 " + a.getSectionName());

            gridAdapter = new GridAdapter(homeActivity, headerData, imglistData1, imglistData, R.layout.header_layout_grid, R.layout.item_layout, width, height, densityX, densityY);
            gridView.setAdapter(gridAdapter);

        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setTitle(getString(R.string.ebooks));
            progress.setMessage(GlobalVars.TXT_LOADING);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }
    }

    public void handleGrid(String keyword, String filter, String userAction) {
        if (userAction.equals("search")) {
            System.out.println("action filter and keyword is " + keyword + "/" + filter + "/" + userAction);

            if ((keyword == null || keyword.length() == 0) && filter.length() == 0) {
                new FetchGridSearchData(homeActivity).execute("");
            } else if (keyword.length() != 0 && filter.length() == 0) {
                new FetchGridSearchDataNoCategory(homeActivity).execute(keyword);
            } else if (filter.length() != 0) {
                if (keyword.length() != 0)
                    new FetchGridSearchDataNoCategory(homeActivity).execute(keyword);

                else
                    new FetchGridSort1Data(homeActivity).execute(filter);
            } else {
                gridAdapter.getFilter().filter(keyword);
            }
        } else if (userAction.equals("sort1")) {
            System.out.println(" sort1  after selecting 2017 Spring set:");
            new FetchGridSort1Data(homeActivity).execute(filter);
        } else if (userAction.equals("sort2")) {
            System.out.println(" sort2  after selecting 2017 Spring set:");
            System.out.println("keyword : " + keyword + "   Filter : " + filter);
            new FetchGridSort2Datas(homeActivity).execute(keyword, filter);
        }
    }

    public int measureCellWidth(Context context, View cell) {

        // We need a fake parent
        FrameLayout buffer = new FrameLayout(context);
        android.widget.AbsListView.LayoutParams layoutParams = new android.widget.AbsListView.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        buffer.addView(cell, layoutParams);

        cell.forceLayout();
        cell.measure(1000, 1000);

        int width = cell.getMeasuredWidth();

        buffer.removeAllViews();

        return width;
    }
}

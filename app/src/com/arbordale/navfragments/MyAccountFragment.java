package com.arbordale.navfragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.arbordale.beans.UserData;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyAccountFragment extends Fragment implements OnClickListener {


    private ArrayList<UserData> userDataList = new ArrayList<UserData>();
    private TextView name;
    private TextView eMail;
    private TextView city;
    private TextView state;
    private Spinner ebooks_purchased1;
    private Spinner ebooks_purchased2;
    private TextView expiry_date;

    private Spinner language;
    private Spinner display_ebooks1;
    private Spinner display_ebooks2;
    private TextView individual_login;
    private TextView display_usage_data;

    private TextView overview;
    private TextView reading;
    private TextView discrete;
    private TextView quiz_scoring;

    private ImageView img_overview;
    private ImageView img_reading;
    private ImageView img_discrete;
    private ImageView img_quiz_scoring;

    private Button manage, rate;
    private HomeScreen mActivity;

    private List<String> purchaseList = new ArrayList<String>();
    private List<String> downloadList = new ArrayList<String>();
    private List<String> langList = new ArrayList<String>();
    private List<String> ebook1List = new ArrayList<String>();
    private List<String> ebook2List = new ArrayList<String>();

    ArrayAdapter<String> purchaseArrayAdapter;
    ArrayAdapter<String> downloadArrayAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.my_account_fragment, container, false);

        mActivity.setFullscreenMode();

        name = (TextView) v.findViewById(R.id.name);
        eMail = (TextView) v.findViewById(R.id.email);
        city = (TextView) v.findViewById(R.id.city);
        state = (TextView) v.findViewById(R.id.state);
        ebooks_purchased1 = (Spinner) v.findViewById(R.id.ebooks_purchased);
        ebooks_purchased2 = (Spinner) v.findViewById(R.id.ebooks_downloaded);
        expiry_date = (TextView) v.findViewById(R.id.expiry_date);
        language = (Spinner) v.findViewById(R.id.language);
        display_ebooks1 = (Spinner) v.findViewById(R.id.display_ebooks_that_are);
        display_ebooks2 = (Spinner) v.findViewById(R.id.display_ebooks_played_by);
        individual_login = (TextView) v.findViewById(R.id.individual_login_required);
        display_usage_data = (TextView) v.findViewById(R.id.display_usage_data);
        overview = (TextView) v.findViewById(R.id.overview_faq);
        reading = (TextView) v.findViewById(R.id.reading_learning);
        discrete = (TextView) v.findViewById(R.id.discrete_login);
        quiz_scoring = (TextView) v.findViewById(R.id.quiz_scoring);
        img_overview = (ImageView) v.findViewById(R.id.faq_arrow);
        img_reading = (ImageView) v.findViewById(R.id.reading_arrow);
        img_discrete = (ImageView) v.findViewById(R.id.discrete_arrow);
        img_quiz_scoring = (ImageView) v.findViewById(R.id.quiz_arrow);
        manage = (Button) v.findViewById(R.id.manage);
        rate = (Button) v.findViewById(R.id.rate);
        SharedPreferences myPrefs1 = mActivity.getSharedPreferences("FunReader", mActivity.context.MODE_PRIVATE);
        //SharedPreferences.Editor prefsEditor = myPrefs1.edit();


        boolean isClicked = myPrefs1.getBoolean("RATE_CLICKED", false);
        if (isClicked) {
            rate.setVisibility(View.GONE);
        }
        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EMAIL).equalsIgnoreCase("") && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_SITECODE).equalsIgnoreCase("")) {
            SqliteDb db_conn = new SqliteDb(mActivity);

            Cursor check = db_conn.fetchData("select * from users", null);

            if (check != null && check.getCount() > 0) {
                check.moveToFirst();
                if (check.getString(0).equalsIgnoreCase("Default") && check.getString(3).equalsIgnoreCase("Guest") && check.getString(4).equalsIgnoreCase("Welcome")) {
                    System.out.println("No changes required");
                } else {
                    System.out.println("changes required");

                    ContentValues cv = new ContentValues();
                    cv.put("Download_status", "NO");
                    cv.put("Status", "PRV");
                    db_conn.updateData("book_icon", cv, null);

                    ContentValues cv1 = new ContentValues();
                    cv1.put("username", "Default");
                    cv1.put("first_name", "Guest");
                    cv1.put("last_name", "Welcome");
                    cv1.put("password", "");
                    cv1.put("site_code", "");
                    cv1.put("Email", "");
                    cv1.put("Organization_name", "");
                    cv1.put("City", "");
                    cv1.put("State", "");
                    cv1.put("Require_login", "");
                    cv1.put("Display_data_usage", "");
                    cv1.put("Language", "");
                    cv1.put("Expiry_date", "");
                    db_conn.updateData("users", cv1, null);
                }

                check.close();
            }

            db_conn.closeDb();

        }

        new AccountData().execute("");

        overview.setOnClickListener(this);
        reading.setOnClickListener(this);
        discrete.setOnClickListener(this);
        quiz_scoring.setOnClickListener(this);
        img_overview.setOnClickListener(this);
        img_reading.setOnClickListener(this);
        img_discrete.setOnClickListener(this);
        img_quiz_scoring.setOnClickListener(this);
        manage.setOnClickListener(this);
        rate.setOnClickListener(this);

        //mActivity.clearFullscreenMode();

        display_ebooks1.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EBOOKS1, "" + arg0.getItemAtPosition(arg2));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        display_ebooks2.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EBOOKS2, "" + arg0.getItemAtPosition(arg2));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        language.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_LANGUAGE, "" + arg0.getItemAtPosition(arg2));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        ebooks_purchased1.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                try {
                    if (Integer.parseInt(arg0.getItemAtPosition(arg2).toString().trim()) < 200) {
                        if (purchaseList.size() > 1)
                            purchaseList.remove(0);
                    }
                } catch (NumberFormatException e) {
                    purchaseList.add(0, purchaseList.size() + "");
                    ebooks_purchased1.setSelection(0, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        ebooks_purchased2.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                try {
                    if (Integer.parseInt(arg0.getItemAtPosition(arg2).toString().trim()) < 200) {
                        if (downloadList.size() > 1)
                            downloadList.remove(0);
                    }
                } catch (NumberFormatException e) {
                    downloadList.add(0, downloadList.size() + "");
                    ebooks_purchased2.setSelection(0, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        return v;
    }

    public static MyAccountFragment newInstance(int position) {

        MyAccountFragment pageFragment = new MyAccountFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("frag_pos", position);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }


    public class AccountData extends AsyncTask<String, Integer, String> {

        private ProgressDialog progress;

        @Override
        protected String doInBackground(String... params) {

            userDataList.clear();

            UserData userData = new UserData();

            SqliteDb db_conn = new SqliteDb(mActivity);

            Cursor userdata_cursor = db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);

            if (!userdata_cursor.isAfterLast()) {
                userdata_cursor.moveToFirst();

                userData.setUsername(userdata_cursor.getString(0));
                userData.setPassword(userdata_cursor.getString(1));
                userData.setSiteCode(userdata_cursor.getString(2));
                userData.setFirstName(userdata_cursor.getString(3));
                userData.setLastName(userdata_cursor.getString(4));
                userData.setEmail(userdata_cursor.getString(5));
                userData.setOrganizationName(userdata_cursor.getString(6));
                userData.setCity(userdata_cursor.getString(7));
                userData.setState(userdata_cursor.getString(8));
                userData.setRequireLogin(userdata_cursor.getString(9));
                userData.setDisplayDataUsage(userdata_cursor.getString(10));
                userData.setLanguage(userdata_cursor.getString(11));
                userData.setExpiryDate(userdata_cursor.getString(12));

                System.out.println("Expiry date is " + userdata_cursor.getString(12));

                userdata_cursor.moveToNext();
            }

            userDataList.add(userData);


            //purchased books logic
            purchaseList.clear();

            Cursor purchased_books = db_conn.fetchData(QueryManager.QUERY_FETCH_PURCHASED_BOOKS, new String[]{"YES", "SET"});

            if (purchased_books.getCount() > 0) {
                purchased_books.moveToFirst();

                purchaseList.add(purchased_books.getCount() + " ");

                while (!purchased_books.isAfterLast()) {
//                    if (purchased_books.getString(0).contains(", The") || purchased_books.getString(0).contains(", A")) {
//                        String arr[] = purchased_books.getString(0).split(",");
//                        purchaseList.add("" + arr[1] + " " + arr[0]);
//                    } else
                    purchaseList.add(purchased_books.getString(0));

                    purchased_books.moveToNext();
                }
                Collections.sort(purchaseList);
                purchased_books.close();
            } else {
                purchaseList.add("0 ");
            }


            //downloaded books logic
            downloadList.clear();

            Cursor downloaded_books = db_conn.fetchData(QueryManager.QUERY_FETCH_DOWNLOADED_BOOKS, new String[]{"YES", "YES", "SET"});
            if (downloaded_books != null) {
                if (downloaded_books.getCount() > 0) {
                    downloaded_books.moveToFirst();

                    downloadList.add(downloaded_books.getCount() + " ");

                    while (!downloaded_books.isAfterLast()) {
//						if (downloaded_books.getString(0).contains(", The") || downloaded_books.getString(0).contains(", A")) {
//							String arr[] = downloaded_books.getString(0).split(",");
//							downloadList.add("" + arr[1] + " " + arr[0]);
//						} else


                        downloadList.add(downloaded_books.getString(0));

                        downloaded_books.moveToNext();
                    }
                    Collections.sort(downloadList);

                }
                downloaded_books.close();
            } else {
                downloadList.add("0 ");
            }

            //language logic
            langList.clear();

            if (userDataList.get(0).getLanguage().length() == 0) {
                langList.add("English");
                langList.add("Spanish");
            } else {
                langList.add(userDataList.get(0).getLanguage());

                if (userDataList.get(0).getLanguage().equalsIgnoreCase("english"))
                    langList.add("Spanish");

                else
                    langList.add("English");
            }

            //list 1 logic
            ebook1List.clear();
            ebook1List.add("Purchased and Previews");
            ebook1List.add("Purchased only");

            //list 2 logic
            ebook2List.clear();
            ebook2List.add("Live Stream and Downloads");
            ebook2List.add("Downloads Only");


            db_conn.closeDb();

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();

            populateData();
        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(mActivity);
            progress.setTitle("Account");
            progress.setMessage("Loading ...");
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }
    }

    private void populateData() {
        if (userDataList.get(0).getLastName().trim().equalsIgnoreCase("welcome"))
            name.setText(userDataList.get(0).getFirstName());

        else
            name.setText(userDataList.get(0).getFirstName() + " " + userDataList.get(0).getLastName());

        eMail.setText(userDataList.get(0).getEmail());
        city.setText(userDataList.get(0).getCity());
        state.setText(userDataList.get(0).getState());

        System.out.println("getactivity " + mActivity + "/" + mActivity);

        purchaseArrayAdapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_dropdown_layout, purchaseList);
        ebooks_purchased1.setAdapter(purchaseArrayAdapter);

        downloadArrayAdapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_dropdown_layout, downloadList);
        ebooks_purchased2.setAdapter(downloadArrayAdapter);

        expiry_date.setText(userDataList.get(0).getExpiryDate());

        ArrayAdapter<String> languageArrayAdapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_dropdown_layout, langList);
        language.setAdapter(languageArrayAdapter);

        ArrayAdapter<String> ebook1ArrayAdapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_dropdown_layout, ebook1List);
        display_ebooks1.setAdapter(ebook1ArrayAdapter);

        ArrayAdapter<String> ebook2ArrayAdapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_dropdown_layout, ebook2List);
        display_ebooks2.setAdapter(ebook2ArrayAdapter);

        individual_login.setText(userDataList.get(0).getRequireLogin());

        if (userDataList.get(0).getDisplayDataUsage().equalsIgnoreCase("Y")) {
            display_usage_data.setText(userDataList.get(0).getDisplayDataUsage());
            display_usage_data.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_selector_right, 0);

            display_usage_data.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (!UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_SITECODE).equalsIgnoreCase(""))
                        GlobalVars.URL = AppConstants.SERVICE_DISPLAY_USAGE_DATA + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_SITECODE);

                    else
                        GlobalVars.URL = AppConstants.SERVICE_DISPLAY_USAGE_DATA + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EMAIL);


                    if (UtilMethods.isConnectionAvailable(mActivity)) {
                        mActivity.openWebActivity("Data Usage");
                    } else {
                        UtilMethods.createToast(mActivity, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                }
            });
        } else
            display_usage_data.setText(userDataList.get(0).getDisplayDataUsage());

        String option_saved1 = UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EBOOKS1);
        String option_saved2 = UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_EBOOKS2);
        String option_saved3 = UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, mActivity, AppConstants.KEY_LANGUAGE);

        int option1_pos = ebook1ArrayAdapter.getPosition(option_saved1);
        int option2_pos = ebook2ArrayAdapter.getPosition(option_saved2);
        int option3_pos = languageArrayAdapter.getPosition(option_saved3);

        language.setSelection(option3_pos);
        display_ebooks1.setSelection(option1_pos);
        display_ebooks2.setSelection(option2_pos);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.overview_faq || v.getId() == R.id.faq_arrow) {
            if (UtilMethods.isConnectionAvailable(mActivity)) {
                GlobalVars.URL = AppConstants.URL_OVERVIEW_FAQ;
                mActivity.openWebActivity("Overview / FAQ");
            } else {
                UtilMethods.createToast(mActivity, AppMessaging.NO_INTERNET_CONNECTION);
            }
        } else if (v.getId() == R.id.reading_learning || v.getId() == R.id.reading_arrow) {
            if (UtilMethods.isConnectionAvailable(mActivity)) {
                GlobalVars.URL = AppConstants.URL_OVERVIEW_READING;
                mActivity.openWebActivity("Reading");
            } else {
                UtilMethods.createToast(mActivity, AppMessaging.NO_INTERNET_CONNECTION);
            }
        } else if (v.getId() == R.id.discrete_login || v.getId() == R.id.discrete_arrow) {
            if (UtilMethods.isConnectionAvailable(mActivity)) {
                GlobalVars.URL = AppConstants.URL_OVERVIEW_DISCRETE;
                mActivity.openWebActivity("Discrete Login");
            } else {
                UtilMethods.createToast(mActivity, AppMessaging.NO_INTERNET_CONNECTION);
            }

        } else if (v.getId() == R.id.quiz_scoring || v.getId() == R.id.quiz_arrow) {
            if (UtilMethods.isConnectionAvailable(mActivity)) {
                GlobalVars.URL = AppConstants.URL_OVERVIEW_QUIZ_SCORING;
                mActivity.openWebActivity("Quiz Scoring");
            } else {
                UtilMethods.createToast(mActivity, AppMessaging.NO_INTERNET_CONNECTION);
            }
        } else if (v.getId() == R.id.manage) {
            mActivity.purchaseOrDownloadBooksFragment();
        } else if (v.getId() == R.id.rate) {
            final String appPackageName = mActivity.getPackageName(); // getPackageName() from Context or Activity object
            SharedPreferences myPrefs1 = mActivity.getSharedPreferences("FunReader", mActivity.context.MODE_PRIVATE);
            SharedPreferences.Editor prefsEditor = myPrefs1.edit();
            prefsEditor.putBoolean("RATE_CLICKED", true);
            prefsEditor.commit();

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            rate.setVisibility(View.GONE);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mActivity = (HomeScreen) activity;
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

}

package com.arbordale.navfragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.arbordale.home.R;
import com.arbordale.ui.PageLoadingView;
import com.arbordale.util.GlobalVars;

public class WebPageActivity extends SherlockFragmentActivity{
	
	ActionBar actionBar;
	PageLoadingView mLoadingView;
	boolean running = false;
	public static int progress;
	private VideoView videoview;
	MediaController mediaController;
	ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		actionBar = getSupportActionBar();
	    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
	    actionBar.setTitle(getIntent().getStringExtra("action_item"));
	    actionBar.setIcon(R.drawable.splash_drawable);
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		System.out.println("WebPage Activity running");
		if(GlobalVars.URL.contains(".mp4"))
		{
		 setContentView(R.layout.video_player);	
		 videoview = (VideoView) findViewById(R.id.surfaceview);
		 playvideo(GlobalVars.URL);
		}	
		
		else
		{	
		setContentView(R.layout.webview_fragment);
		WebView webview=(WebView) findViewById(R.id.webview_panel);
		mLoadingView = (PageLoadingView) findViewById(R.id.pageprogressStudy);
		
		final Thread s = new Thread(r);
		
		webview.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setAllowFileAccess(true);
		webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		    
		System.out.println("value of global url is "+GlobalVars.URL);
		
		webview.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				Integer temp = newProgress;
				if (temp != null) {
					progress = (int) (3.6 * temp);
					if (!running) {
						try {
							s.join();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						running = true;
					}
					if (progress >= 360)
						{
						mLoadingView.setVisibility(View.INVISIBLE);
						}
				}
			}
		 });
		
	     webview.setWebViewClient(new WebViewClient() {
	    	
	     public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
	     }
	     
	     @Override
	    	public void onReceivedSslError(WebView view,SslErrorHandler handler, SslError error) {
	    		super.onReceivedSslError(view, handler, error);
	    	}
	    
	       @Override
	    	public void onPageFinished(WebView view, String url) {
	    	}
	    
	        @Override
	    	public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	
	    	  if(GlobalVars.URL.contains("w=ta"))
	    		{
	    		  view.loadUrl("https://docs.google.com/gview?embedded=true&url="+url);
	    		}

	   	      else
	   	    	view.loadUrl(url);	
	    	 
	    	  return true;
	    	}
	    });
	    
	    webview.loadUrl(GlobalVars.URL);	
	    
	    if (!mLoadingView.isSpinning) {
			mLoadingView.spin();
			mLoadingView.setVisibility(View.VISIBLE);
		}
	  } 
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		 switch (item.getItemId()) {
		    
		    case android.R.id.home:
		    	onBackPressed();
	           break;
		 }
		 return true;
	}
	
	
	final Runnable r = new Runnable() {
		public void run() {
			while (progress <= 360) {
				running = true;
				mLoadingView.incrementProgress(progress);
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			mLoadingView.setVisibility(View.INVISIBLE);
			running = false;
		}
	}; 
	
	public void playvideo(String videopath) {
	    try {
	        progressDialog = ProgressDialog.show(WebPageActivity.this, "","Buffering video...", false);
	        progressDialog.setCancelable(true);
	        getWindow().setFormat(PixelFormat.TRANSLUCENT);

	        mediaController = new MediaController(WebPageActivity.this);

	        Uri video = Uri.parse(videopath);
	        videoview.setMediaController(mediaController);
	        videoview.setVideoURI(video);

	        videoview.setOnPreparedListener(new OnPreparedListener() {

	            public void onPrepared(MediaPlayer mp) {
	                progressDialog.dismiss();
	                videoview.start();
	            }
	        });

	    } catch (Exception e) {
	        progressDialog.dismiss();
	        System.out.println("Video Play Error :" + e.getMessage());
	    }

	}
}

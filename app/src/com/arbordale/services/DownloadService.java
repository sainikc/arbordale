package com.arbordale.services;

import java.io.File;
import java.io.FileNotFoundException;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;

public class DownloadService extends IntentService {

	//For Download
    SharedPreferences preferenceManager;
    DownloadManager downloadManager;
    
	public DownloadService() {
		super("");
		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		//IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_VIEW_DOWNLOADS);
		//registerReceiver(downloadReceiver, intentFilter);
		
		preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
	    downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);
		
		String source=intent.getStringExtra("source");
		String destination=intent.getStringExtra("destination");
		String title=intent.getStringExtra("title");
		String description=intent.getStringExtra("description");
		
		//Source
		Uri suri=Uri.parse(source);
		
		//Destination
		File f=new File(destination);
		Uri dUri=Uri.fromFile(f);
	    
		DownloadManager.Request request = new DownloadManager.Request(suri);
	    request.setTitle(title);
	    request.setDescription(description);
	    request.setDestinationUri(dUri);
	    request.allowScanningByMediaScanner();
		
	    long download_id=downloadManager.enqueue(request);
	  
	    //Save the download id
	    Editor PrefEdit = preferenceManager.edit();
	    PrefEdit.putLong("DOWNLOAD_ID", download_id);
	    PrefEdit.commit();
	    
	    
	    DownloadManager.Query q = new DownloadManager.Query();
	    q.setFilterById(download_id);
	    Cursor cursor = downloadManager.query(q);
	    cursor.moveToFirst();
	    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
	    System.out.println("bytes downloded "+bytes_downloaded+"/"+cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES)));
	    cursor.close();
	    
	}

	
	 private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
   	  
   	  @Override
   	  public void onReceive(Context arg0, Intent arg1) {
   		  
   	   // TODO Auto-generated method stub
   	   DownloadManager.Query query = new DownloadManager.Query();
   	   query.setFilterById(preferenceManager.getLong("DOWNLOAD_ID", 0));
   	   Cursor cursor = downloadManager.query(query);
   	     
   	   if(cursor.moveToFirst()){
   	    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
   	    int status = cursor.getInt(columnIndex);
   	    int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
   	    int reason = cursor.getInt(columnReason);
   	      
   	    if(status == DownloadManager.STATUS_SUCCESSFUL){
   	     //Retrieve the saved download id
   	     long downloadID = preferenceManager.getLong("DOWNLOAD_ID", 0);
   	       
   	     ParcelFileDescriptor file;
   	     try {
   	      System.out.println("download successful unzip now"); 	 
   	      file = downloadManager.openDownloadedFile(downloadID);
   	     } catch (FileNotFoundException e) {
   	      // TODO Auto-generated catch block
   	      e.printStackTrace();
   	     }
   	       
   	    }else if(status == DownloadManager.STATUS_FAILED){
   	    	System.out.println("download failed");
   	    }else if(status == DownloadManager.STATUS_PAUSED){
   	    	System.out.println("download paused");
   	    }else if(status == DownloadManager.STATUS_PENDING){
   	    	System.out.println("download pending");
   	    }else if(status == DownloadManager.STATUS_RUNNING){
   	    	System.out.println("download running");
   	    }
   	   }
   	  }
   	    
   	 };
}

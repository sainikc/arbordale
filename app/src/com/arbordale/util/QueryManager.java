package com.arbordale.util;

public class QueryManager {

	// For E-book section
	//public static String PARAM_FALL = "2014FallSxet";
	//public static String PARAM_FALL = "Fall2017";
	 public static String PARAM_FALL = "2017FallSet";
	//public static String PARAM_SPRING = "2013SpringSet";
	public static String PARAM_SPRING_2014 = "2014SpringSet";
	public static String PARAM_SPRING_2015 = "2015SpringSet";
	//public static String PARAM_SPRING_2017 = "Spring2017";
	public static String PARAM_SPRING_2017 = "2017SpringSet";
	public static String PARAM_SET = "SET";
	public static String PARAM_YES = "YES";
	public static String PARAM_NO = "NO";
	public static String PARAM_PRV = "PRV";
	public static String PARAM_SORT_CATEGORY = "Common Core & Science";

	public static String QUERY_FETCH_IS_BOOK_DOWNLOADED = "select count(*) from book_icon where Download_status=? and Status=? and book_name=?";

	// Purchase_download_screen
	public static String QUERY_FETCH_BOOK_NO_CATEGORY="select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where section!=?";
	public static String QUERY_FETCH_BOOK_SETS = "select book_full_name,book_name,short_description,Status,Download_status,book_color,Author,Illustrator,book_full_name from book_icon where section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_PURCHASE = "select book_name,Status,book_full_name,Download_status,short_description,no_of_page,book_color,Author,Illustrator,book_full_name  from book_icon where section=? or section=? or section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_OTHERS_PURCHASE = "select book_name,Status,book_full_name,Download_status,short_description,no_of_page,book_color,Author,Illustrator,book_full_name from book_icon where section!=? and section!=? and section!=? and section!=? order by book_full_name";

/*	public static String QUERY_FETCH_BOOKS_ALL = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color from book_icon where section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_DOWNLOAD_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color from book_icon where Download_status=? and (section=? or section=?) order by book_full_name";
	public static String QUERY_FETCH_BOOKS_PURCHASED_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color from book_icon where Status=? and (section=? or section=?) order by book_full_name";
	public static String QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color from book_icon where  Status=? and  Download_Status=? and  (section=? or section=?) order by book_full_name";
*/
	public static String QUERY_FETCH_BOOKS_ALL = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_DOWNLOAD_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where Download_status=? and section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_PURCHASED_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where Status=? and section=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_PURCHASED_DOWNLOAD_ONLY = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where  Status=? and  Download_Status=? and section=? order by book_full_name";

	
	public static String QUERY_FETCH_BOOKS_OTHERS_ALL = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where section!=? and section!=? and section!=? and section!=? order by book_full_name";
	public static String QUERY_FETCH_BOOKS_OTHERS_ALL_DOWNLOAD = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where Download_status=? and (section!=? and section!=? and section!=? and section!=? ) order by book_full_name";
	public static String QUERY_FETCH_BOOKS_OTHERS_ALL_PURCHASED = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where Status=? and (section!=? and section!=? and section!=? and section!=?) order by book_full_name";
	public static String QUERY_FETCH_BOOKS_OTHERS_ALL_PURCHASED_DOWNLOAD = "select book_name,Status,Download_status,access_flag,no_of_page,sd_keywords,sd_animals,book_color,Author,Illustrator,book_full_name from book_icon where Download_status=? and Status=? and (Section!=? and section!=? and section!=? and section!=?) order by book_full_name";

	public static String QUERY_DROPDOWN = "SELECT DISTINCT set_sort_category FROM titles WHERE set_sort_category!='' ORDER BY 1";
	public static String QUERY_DROPDOWN_FURTHER = "SELECT title_id, set_sort_category, title FROM titles WHERE set_sort_category=? ORDER BY 3";

/*	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL = "SELECT short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color FROM setdetails, titles , book_icon WHERE titles.title_id=setdetails.title_id AND set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND short_title=book_icon.Book_Name group by title order by title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_DOWNLOAD = "SELECT short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color FROM setdetails, titles , book_icon WHERE titles.title_id=setdetails.title_id AND set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND short_title=book_icon.Book_Name AND Download_status=? group by title order by title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED = "SELECT short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color FROM setdetails, titles , book_icon WHERE titles.title_id=setdetails.title_id AND set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND short_title=book_icon.Book_Name AND Status=? group by title order by title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED_DOWNLOAD = "SELECT short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color FROM setdetails, titles , book_icon WHERE titles.title_id=setdetails.title_id AND set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND short_title=book_icon.Book_Name AND Download_status=? AND Status=? group by title order by title";
*/
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL ="SELECT distinct short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color,book_icon.Author,book_icon.Illustrator,book_icon.book_full_name FROM setdetails, titles, book_icon WHERE set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND titles.title_id=setdetails.title_id AND title_type!='SET'  AND titles.short_title=book_icon.Book_Name AND (book_icon.Status='YES' OR book_icon.Status='PRV') group by short_title  ORDER BY book_full_name";// "SELECT distinct short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color,book_icon.Author,book_icon.Illustrator,book_icon.book_full_name FROM setdetails, titles, items,book_icon WHERE set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND titles.title_id=setdetails.title_id AND title_type!='SET' AND spanish='Y' AND items.title_id=titles.title_id AND items.isbn=setdetails.member_isbn AND titles.short_title=book_icon.Book_Name AND (book_icon.Status='YES' OR book_icon.Status='PRV') AND item_type='EBook' ORDER BY titles.title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_DOWNLOAD = "SELECT distinct short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color,book_icon.Author,book_icon.Illustrator,book_icon.book_full_name FROM setdetails, titles, items,book_icon WHERE set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND titles.title_id=setdetails.title_id AND title_type!='SET' AND spanish='Y' AND items.title_id=titles.title_id AND items.isbn=setdetails.member_isbn AND titles.short_title=book_icon.Book_Name AND (book_icon.Status='YES' OR book_icon.Status='PRV') AND item_type='EBook' AND Download_status=? ORDER BY titles.title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED = "SELECT distinct short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color,book_icon.Author,book_icon.Illustrator,book_icon.book_full_name FROM setdetails, titles, items,book_icon WHERE set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND titles.title_id=setdetails.title_id AND title_type!='SET' AND spanish='Y' AND items.title_id=titles.title_id AND items.isbn=setdetails.member_isbn AND titles.short_title=book_icon.Book_Name AND (book_icon.Status='YES' OR book_icon.Status='PRV') AND item_type='EBook' AND Status=? ORDER BY titles.title";
	public static String QUERY_DROPDOWN_FURTHER_LIST_ALL_PURCHASED_DOWNLOAD = "SELECT distinct short_title,member_isbn, titles.title,book_icon.Book_Name,book_icon.Status,book_icon.download_status,book_icon.no_of_page,book_icon.access_flag,book_icon.sd_keywords,book_icon.sd_animals,book_icon.book_color,book_icon.Author,book_icon.Illustrator,book_icon.book_full_name FROM setdetails, titles, items,book_icon WHERE set_isbn IN (SELECT isbn FROM titles, items WHERE titles.title_id=? AND items.title_id=titles.title_id) AND titles.title_id=setdetails.title_id AND title_type!='SET' AND spanish='Y' AND items.title_id=titles.title_id AND items.isbn=setdetails.member_isbn AND titles.short_title=book_icon.Book_Name AND (book_icon.Status='YES' OR book_icon.Status='PRV') AND item_type='EBook' AND Download_status=? AND Status=? ORDER BY titles.title";

	// For Login / Registration
	public static String QUERY_FETCH_USER_INFO = "select * from users";
	public static String QUERY_FETCH_UPDATE_BOOKS_ICON_REGEX = "select book_full_name from book_icon where book_full_name like ? and section!=?";
	public static String QUERY_FETCH_UPDATE_BOOKS_ICON_NAME_NON_SET = "select book_full_name from book_icon where Book_Name=? and section!=?";
	public static String QUERY_FETCH_UPDATE_BOOKS_ICON_NAME_SET = "select book_full_name from book_icon where Book_Name=? and section=?";
	public static String QUERY_FETCH_PURCHASED_BOOKS = "select book_full_name from book_icon where Status=? and Section!=?";
	public static String QUERY_FETCH_DOWNLOADED_BOOKS = "select book_full_name from book_icon where Download_status=? and Status=? and Section!=?";

	// Fetch title id
	public static String QUERY_FETCH_TITLE_ID = "SELECT title_id FROM titles WHERE short_title=?";

	// Fetch activity log
	public static String QUERY_FETCH_ACTIVITY_LOG = "select short_title,sl_code,password,act_when,act_id from activity_log";
}

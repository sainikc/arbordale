package com.arbordale.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arbordale.home.R;

public class UtilMethods {
	
	 public static HttpClient client;
     static AlertDialog alertDialog = null;

	 public static List<String> getImage(Context context,String assetFolder) throws IOException
     {
      AssetManager assetManager = context.getAssets();
      String[] files = assetManager.list(assetFolder);   
      List<String> it=Arrays.asList(files);
      return it; 
      }

	
	 public static Bitmap getBitmapFromAssets(String fileName,Context mContext) {

		 AssetManager assetManager = mContext.getAssets();
		 InputStream istr;
		 Bitmap bitmap = null;
		
		 try {
				istr = assetManager.open(fileName);
				bitmap = BitmapFactory.decodeStream(istr);
			} 
		 catch (IOException e) {
				e.printStackTrace();
			}
		    
		 return bitmap;
	   }
	 
	 
	 public static Drawable getAssetImage(Context context, String filename,double densityX,double densityY) throws IOException {
		    AssetManager assets = context.getResources().getAssets();
		    InputStream buffer = new BufferedInputStream((assets.open(filename)));
		    Bitmap bitmap = BitmapFactory.decodeStream(buffer);
		    
		    //For testing
		    Bitmap bm=Bitmap.createScaledBitmap(bitmap, (int)(0.732*densityX), (int)(0.96*densityY), true);
		    
		    return new BitmapDrawable(context.getResources(), bm);
		}
	 
	 
	 public static Drawable getAssetImageDownloads(Context context, String filename) throws IOException {
		    AssetManager assets = context.getResources().getAssets();
		    InputStream buffer = new BufferedInputStream((assets.open(filename)));
		    Bitmap bitmap = BitmapFactory.decodeStream(buffer);
		    
		    return new BitmapDrawable(context.getResources(), getRoundedCornerBitmap(bitmap));
		}
	 
	 public static Drawable getAssetImageDownloadsSmallThumbs(Context context, String filename) throws IOException {
		    AssetManager assets = context.getResources().getAssets();
		    InputStream buffer = new BufferedInputStream((assets.open(filename)));
		    Bitmap bitmap = BitmapFactory.decodeStream(buffer);
		    
		    return new BitmapDrawable(context.getResources(), getRoundedCornerBitmapSmall(bitmap));
		}
	 
	 public static String randomImage(List<String> imgList)
	 {
		 Random randomGenerator= new Random();
		 int index=randomGenerator.nextInt(imgList.size());
		 String randomImg=imgList.get(index);
		 return randomImg;
	 }
	 
	 public static String argumentResultString(ArrayList<String> args)
	 {
		 String finalArgumentList="";
		 Iterator<String> argsIterator=args.iterator();
		 
		 while(argsIterator.hasNext())
		 {
			 finalArgumentList+=argsIterator.next();
		 }	 
			 
		 return finalArgumentList;
	 }
	 
	 
	 public static String getData(String _URL,List<NameValuePair> params) {

         String postResponse = "-1";

         if(!_URL.endsWith("?"))
        	 _URL += "?";

         try {
                BufferedReader in = null;
                try {
                	
                       client = new DefaultHttpClient();
                       String paramString = URLEncodedUtils.format(params, "utf-8");
                       HttpGet get = new HttpGet(_URL+paramString);
                       get.setHeader("Content-type","application/json");
                       HttpResponse responseGet = client.execute(get);
                       HttpEntity resEntityGet = responseGet.getEntity();

                       if (resEntityGet != null) {
                             postResponse = EntityUtils.toString(resEntityGet);
                       }
                       
                       System.out.println("value of postResponse is "+_URL+paramString+"/"+postResponse);
                       return postResponse;
                       
                } catch (Exception e) {
                       Log.e("GET CALL Exception", e.getMessage());
                } 
                finally {
                       
                	if (in != null) {
                             try {
                                    in.close();
                             } catch (IOException e) {
                                    e.printStackTrace();
                             }
                       }
                }
            } catch (Exception e) {
         }
         return postResponse;
   } 


   public static void updateAddSharedPrefernces(String filename,Context ctx,String key ,String value)
   {
	   SharedPreferences.Editor editor = ctx.getSharedPreferences(filename,Context.MODE_PRIVATE).edit();
	   editor.putString(key,value);
	   editor.commit();
	   editor.apply();
   }
   
   
   public static void updateAddLongSharedPrefernces(String filename,Context ctx,String key ,long value)
   {
	   SharedPreferences.Editor editor = ctx.getSharedPreferences(filename,Context.MODE_PRIVATE).edit();
	   editor.putLong(key,value);
	   editor.commit();
	   editor.apply();
   }
   
   
   public static void clearSharedPrefernces(String filename,Context ctx)
   {
	   SharedPreferences.Editor editor = ctx.getSharedPreferences(filename,Context.MODE_PRIVATE).edit();
	   editor.clear();
	   editor.apply();
   }
   
   
   public static String getDataFromSharedPrefernces(String filename,Context ctx,String key)
   {
	   SharedPreferences prefs = ctx.getSharedPreferences(filename,Context.MODE_PRIVATE); 
	   String restoredText = prefs.getString(key, "");
	   return restoredText;  
   }

   public static long getLongDataFromSharedPrefernces(String filename,Context ctx,String key)
   {
	   SharedPreferences prefs = ctx.getSharedPreferences(filename,Context.MODE_PRIVATE); 
	   long restoredText = prefs.getLong(key, 0L);
	   return restoredText;  
   }
   
   public static boolean isConnectionAvailable(Context ctx) {
       ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo netInfo = cm.getActiveNetworkInfo();
       if (netInfo != null && netInfo.isConnectedOrConnecting()) {
           return true;
       }
       return false;
   }
   
   public static boolean inetAddr(){

	    boolean x1 = false;

	    try {
	        Socket s = new Socket("arbordalepublishing.com", 37);

	        InputStream i = s.getInputStream();

	        Scanner scan = new Scanner(i);

	        while(scan.hasNextLine()){

	            System.out.println("receiver call : "+scan.nextLine());
	            x1 = true;
	        }
	        scan.close();
	        s.close();
	    } 
	    catch (Exception e) {
	            x1 = false;
	    } 

	    return x1;

	}
   
  public static void createToast(Context ctx,String toastText)
  {
	  LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	  View layout = inflater.inflate(R.layout.toast_custom_layout,null);
	  TextView msg=(TextView) layout.findViewById(R.id.textView1);
	  msg.setText(toastText);
	  Toast toast = new Toast(ctx);
	  toast.setGravity(Gravity.CENTER_VERTICAL, 0, 100);
	  toast.setDuration(Toast.LENGTH_SHORT);
	  toast.setView(layout);
	  toast.show();
  }
   
  
  public static String getDatabaseName(String dBName){
		/* the path for Database file i e should be internal memory or sdard */
		File result = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+AppConstants.APP_FOLDER,dBName);
		return result.getAbsolutePath();
	}
  
  /*public static void createSpinnerDialog(Context ctx,ArrayAdapter<String> adapter)
  {
		final Dialog d=new Dialog(ctx,R.style.FullHeightDialog);
		d.setContentView(R.layout.spinner_dialog);
		Button btn=(Button) d.findViewById(R.id.bckbutton);
		
		
		ListView ls=(ListView) d.findViewById(R.id.list);
		ls.setAdapter(adapter);
		
		d.show();
  }*/
  
  
	public static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}
	
	public static boolean passwordsMatch(String pass,String confirm_pass) {
		if(pass.equals(confirm_pass))
		{
			return true;
		}
		return false;
	}
	
	public static void hidekeyboard(Context ctx,IBinder windowToken)
	{
		InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(windowToken, 0); 
	}
	
	public static JSONObject getJSONObjFromUrl(String url, List<NameValuePair> params,Context ctx) {      
        
		 String json="";
	     JSONObject jObj=null;
	     InputStream is = null;
		 
	     if(UtilMethods.isConnectionAvailable(ctx))
		  {
	        // Making HTTP request
	        try {
	        	// defaultHttpClient
	        	DefaultHttpClient httpclient = new DefaultHttpClient();
	            HttpPost httpPost = new HttpPost(url);
	            httpPost.setEntity(new UrlEncodedFormEntity(params));

	            HttpResponse httpResponse = httpclient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            is = httpEntity.getContent();

	            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line);
	            }
	            is.close();
	            httpResponse.getEntity().consumeContent();

	           json = sb.toString();
	           jObj = new JSONObject(json);
	           
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }catch (Exception e) {
	            Log.e("Buffer Error", "Error converting result " + e.toString());
	        }
		 }
		
	     else
	     {
	    	 jObj=UtilMethods.getJSONObjFromSD(Environment.getExternalStorageDirectory()+"/"+AppConstants.APP_FOLDER+"/"+AppConstants.JSON_SUBSCRIPTION,ctx);
	     }	 
		
        // return JSON String
        return jObj;
      }
	
      public static JSONObject getJSONObjFromSD(String url,Context ctx) {      
        
        String json="";
        JSONObject jObj=null;
        File file=null;
        file=new File(url);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

           json = sb.toString();//.substring(0, sb.toString().length()-1);
           
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON String

        return jObj;
    }
      
    public static void createDialog(Context ctx)
    {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
 
			// set title
			alertDialogBuilder.setTitle("Book Folder not Found !");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("You might have removed it from phone storage")
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						alertDialog.dismiss();
						alertDialog=null;
					}
				  });
 
				// create alert dialog
				alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
			}
    
    final static private String charset = "[a-zA-Z0-9_]"; // separate this out for future fixes
    final static private String regex = charset + "+@" + charset + "+." + charset + "+";

    public static boolean validEmail(String email) {
        return email.matches(regex);
    }
    
    public static void recycleImagesFromView(View view) {
        if(view instanceof ImageView)
        {
            Drawable drawable = ((ImageView)view).getDrawable();
            if(drawable instanceof BitmapDrawable)
            {
                BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
                bitmapDrawable.getBitmap().recycle();
            }
        }
    }
 
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
            bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
     
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()-40);
        final RectF rectF = new RectF(rect);
        final float roundPx = 15;
     
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
     
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
     
        return output;
      }
    
    public static Bitmap getRoundedCornerBitmapSmall(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
            bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
     
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 73, bitmap.getWidth(), bitmap.getHeight()-40);
        final RectF rectF = new RectF(rect);
        final float roundPx = 15;
     
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
     
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
     
        return output;
      }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static public <T> void executeAsyncTask(AsyncTask<T, ?, ?> task, T... params) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
      }
      else {
        task.execute(params);
      }
    }
    
    public static boolean isKindleFire() {
        return android.os.Build.MANUFACTURER.equals("Amazon")
                && (android.os.Build.MODEL.equals("Kindle Fire")
                    || android.os.Build.MODEL.startsWith("KF"));
    }
    }

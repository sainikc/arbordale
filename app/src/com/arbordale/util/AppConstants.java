package com.arbordale.util;

public interface AppConstants {

	//LOG TAG(S)
	public final String TAG_CURL="Curlactivity";
	public final String TAG="Arbordale";
	
	
	public final int SPLASH_DISPLAY_LENGHT =3000;
	public final int HOME_DISPLAY_LENGHT = 5000;
	public final String IMAGE_FORMAT_PNG = ".png";
	public final String IMAGE_FORMAT_JPG = ".jpg";
	String IMAGE_FORMAT_EXTENSION = ".b";
	String MP3_FORMAT_EXTENSION = ".a";
	public final String SOUND_FORMAT_MP3 = ".mp3";
	public final String PREFIX_SPANISH="ES";
	
	//Asset folders
	public final String ASSET_HOME="home";
	
	//Database Path
	public final String DB_PATH="Arbordale/Arbordale_1";
	public final String DB_NAME="Arbordale_1";
	public final int DB_VERSION=1;
	public final String DB_PASSWORD="password";//password
	
	//SERVER Response
	public final String STATUS_SUCCESS="SUCCESS";
	public final String STATUS_EMAIL_EXISTS="ERROR: THERE IS ALREADY AN ACCOUNT FOR THAT EMAIL ADDRESS WITH A DIFFERENT PASSWORD";
	public final String STATUS_NO_SHORT_TITLE="ERROR: NO SHORT TITLE";
	public final String STATUS_NO_LICENSE="{\"ERROR\":\"NO LICENSE FOUND FOR THIS USER\"}";
	public final String STATUS_WRONG_EMAIL="ERROR: UNKNOWN EMAIL ADDR";
	public final String STATUS_WRONG_PASSWORD="ERROR: INCORRECT PASSWORD";
	public final String STATUS_WRONG_SITE_CODE="ERROR: UNKNOWN SITE CODE";
	public final String STATUS_ERROR_SITE_CODE="\"ERROR\": \"UNKNOWN SITE CODE\"";
	public final String STATUS_INVALID="INVALID";
	public final String STATUS_INVALID_MESSAGE="ERROR: WRONG LIBRARY SELECTED";
	public final String STATUS_NO="NO";
	public final String STATUS_NO_MESSAGE="NO SITE LICENCE ATTACHED TO SELECTED LIBRARY";
	public final String STATUS_BADSETUP="BADSETUP";
	public final String STATUS_BADSETUP_MESSAGE="library record has not been properly setup to validate card numbers";
	public final String STATUS_BADCARDNO="BADCARDNO";
	public final String STATUS_BADCARDNO_MESSAGE="ERROR: INVALID CARD NUMBER";
	

	//Apps folder name
	public final String APP_FOLDER="Arbordale";
	
	
	//Json Files
	public final String JSON_SUBSCRIPTION="subscription_license.json";
	
	//Shared preferences keys and file names
	public final String SHARED_PREFERENCES_FILE_NAME = "ArbordaleUserData";
	public final String SHARED_PREFERENCES_SYSTEM_FILE = "ArbordaleSystemData";
	
	public final String KEY_EMAIL = "email";
	public final String KEY_FIRST_NAME = "first_name";
	public final String KEY_LAST_NAME = "last_name";
	public final String KEY_PASSWORD = "password";
	public final String KEY_SITECODE = "site_code";
	public final String KEY_REQUIRE_LOGIN = "Require_login";
	public final String KEY_DISPLAY_DATA_USAGE = "Display_data_usage";
	public final String KEY_LANGUAGE = "Language";
	public final String KEY_LANGUAGE_IN_READER = "Language_Reader";
	public final String KEY_EXPIRY_DATE = "Expiry_date";
	public final String KEY_CITY = "City";
	public final String KEY_STATE = "State";
	public final String KEY_EBOOKS1 = "ebooks1";
	public final String KEY_EBOOKS2 = "ebooks2";
	public final String KEY_SORT_EBOOKS = "sort_state";
	public final String KEY_DIALOG_SEEN = "dialog_seen";
	public final String KEY_FRAG_VISIBLE_LAST = "fragment_visible";
	
	public final String KEY_IPAD="ipad=1";
	public final String KEY_DB_COPIED="dbcopied";
	public final String KEY_DB_ENCRYPTED="dbencrypted";
	public final String WEB_CALL_EXECUTION_TIME="webcall_exec_time";
	
	//Shared preferences values 
	public final String VALUE_DB_COPIED="true";
	public final String VALUE_DB_ENCRYPTED="yes";
	public final String VALUE_EBOOK_1_1="Purchased and Previews";
	public final String VALUE_EBOOK_1_2="Purchased only";
	public final String VALUE_EBOOK_2_1="Live Stream and Downloads";
	public final String VALUE_EBOOK_2_2="Downloads Only";
	public final String VALUE_ENGLISH="English";
	public final String VALUE_SPANISH="Spanish";
	
	
	//PAGE URLS
	public final String URL_HOME_PAGE="http://www.arbordalepublishing.com/bookhome.php?";
	public final String URL_QUIZZES="http://www.arbordalepublishing.com/interactive_quizzes.php?";
	public final String URL_FOR_CREATIVE_MINDS="http://www.arbordalepublishing.com/forcreativeminds.php?";
	public final String URL_TEACHING_ACTIVITIES="http://www.arbordalepublishing.com/teachingactivities.php?";
	public final String URL_RELATED_WEBSITES="http://www.arbordalepublishing.com/RelatedWebsites_stub.php?";
	public final String URL_TRAILERS="http://www.arbordalepublishing.com/sl_Trailers.php?";
	public final String URL_AUTHORS="http://www.arbordalepublishing.com/sl_authors.php?";
	public final String URL_STANDARDS="http://www.arbordalepublishing.com/Standards.php?";
	public final String URL_LEXILE_CODES="http://www.arbordalepublishing.com/sl_LexileCodes.php?";
	
	public final String URL_OVERVIEW_FAQ="http://www.arbordalepublishing.com/FuneReader.htm";
	public final String URL_OVERVIEW_READING="http://www.arbordalepublishing.com/APictureBookApproach-Tutorial.htm";
	public final String URL_OVERVIEW_DISCRETE="http://www.arbordalepublishing.com/Login-DataDisplay.htm";
	public final String URL_OVERVIEW_QUIZ_SCORING="http://www.arbordalepublishing.com/QuizScoring.htm";
	
	
	public final String URL_POPUP="http://www.arbordalepublishing.com/redirect.php?ipad=1";
	public final String URL_TRAILERS_POPUP="http://www.arbordalepublishing.com/Trailers/";
	
	//WEB-SERVICE-URLS
	public final String SERVICE_VALIDATE="http://www.arbordalepublishing.com/validateaccess.php?";
	public final String SERVICE_CHECK_VIDEO_FILE="http://www.arbordalepublishing.com/Trailers/checkvediofile.php?";
	public final String SERVICE_CHECK_RELATED_WEBSITES="http://www.arbordalepublishing.com/check_relatedwebsites_ipad.php?";
	public final String SERVICE_PURCHASE_RESULT="http://www.arbordalepublishing.com/logipadebooks.php?";
	public final String SERVICE_DISPLAY_USAGE_DATA="http://www.arbordalepublishing.com/SiteLicenseUsage.php?";
	public final String SERVICE_PURCHASE_DATA="http://www.arbordalepublishing.com/book_detail_ipad_5aug.php?title=";
	//public final String SERVICE_ALL_STATE="http://www.arbordalepublishing.com/validatelibaccess.php?";
	//public final String SERVICE_GET_LIBRARIES="http://www.arbordalepublishing.com/validatelibaccess.php?";
	public final String SERVICE_VALIDATE_LIBRARY="http://www.arbordalepublishing.com/validatelibaccess.php?";
	public final String SERVICE_GET_ALL_STATE="http://www.arbordalepublishing.com/validatelibaccess.php?ls";
	public final String SERVICE_GET_LIBRARY_FOR_STATE="http://www.arbordalepublishing.com/validatelibaccess.php?ll=";
	//CONSTANT
	public final String DEFAULT_ALPHABETIC="Alphabetic (Default)";
	
	//Fragment Tags
	public final String FRAG_GRIDVIEW="GridFragment";
	public final String FRAG_HOME="HomeFragment";
	public final String FRAG_LOGIN="LoginFragment";
	public final String FRAG_REGISTRATION="RegistrationFragment";
	public final String FRAG_WEBVIEW="WebviewFragment";
	public final String FRAG_MY_ACCOUNT="MyAccountFragment";
	
}

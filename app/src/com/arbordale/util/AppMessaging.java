package com.arbordale.util;

public interface AppMessaging {

	public String REG_SUCCESS="You are successfuly registered with us!";
	public String REG_ALREADY_REGISTERED="ALREADY AN ACCOUNT FOR THAT EMAIL ADDRESS WITH A DIFFERENT PASSWORD!";
	public String CANT_BE_BLANK="All fields are mandatory!";
	public String EMAIL_INVALID="Please check the email address!";
	public String PASSWORDS_DOESNT_MATCH="Passwords doesn't match!";
	public String SITE_CODE_BLANK="Site code can't be blank!";
	public String LIBRARY_CODE_BLANK="Library code can't be blank!";
	public String NO_INTERNET_CONNECTION="No internet connection!";
	public String CONNECTION_TIMEOUT="Problem with internet connection!";
	public String STATE_BLANK="Select a State";
	public String LIBRARY_BLANK="Select a LIBRARY";
	public String WRONG_SITE_CODE="Invalid Password!";
	public String WRONG_Library_code="Invalid Library Code!";

}

package com.arbordale.util;

import android.os.Environment;


public class PathUtil {
	
   public static String currentPath(int pathVar,String foldername)
   {
	   String pathToResource="";
	   
	   if(pathVar==0)
	   {
		   pathToResource=Environment.getExternalStorageDirectory()+"/"+AppConstants.APP_FOLDER+"/"+foldername;
		   return pathToResource;
	   }
	   
	   else if(pathVar==1)
	   {
		   pathToResource="http://arbordalepublishing.com/eBooks3/";
		   return pathToResource;
	   }
	   
	   else if(pathVar==2)
	   {
		   pathToResource="http://arbordalepublishing.com/eBooks3/Preview/";
		   return pathToResource;
	   }
	    
	   return pathToResource;
   }
}

package com.arbordale.util;

public class GlobalVars {
	public static String URL = "";
	public static String HOME_READER_PRESSED="false";
	public static String BACK_FROM_PURCHASE_DOWNLOAD="false";
	
	//Login Registration progress dialogs
	public static String WEB_CALL_DIALOG_TITLE_LOGIN_1="Login";
	public static String WEB_CALL_DIALOG_TITLE_LOGIN_2="Fetching Info";
	public static String WEB_CALL_DIALOG_TITLE_LOGIN_3="Getting Books";
	public static String WEB_CALL_DIALOG_TITLE_REGISTER="Registration";
	public static String TXT_LOADING="Loading...";
	
	public static String WEB_DIALOG_TITLE="";
	public static String WEB_DIALOG_MSG="Loading...";
	
}

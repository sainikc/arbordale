package com.arbordale.database;

import com.arbordale.util.AppConstants;
import com.arbordale.util.UtilMethods;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
//import android.database.SQLException;
//import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

public class SqliteDb {

	private EncryptedDbHelper dbHelper;

	private SQLiteDatabase database;
	Context context;
	
	public SqliteDb(Context context) {
		this.context=context;
		dbHelper=new EncryptedDbHelper(this.context, AppConstants.DB_PATH, null, AppConstants.DB_VERSION);
	}
	
	
	public void execSQL(String Query){
		try{
			if(database==null||!database.isOpen())
			{
				database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
				//database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
				database.execSQL(Query);
			}
			
			else
			{
				database.execSQL(Query);	
			}	
		}catch (SQLException e){
			Log.e("query error" , e.getMessage());
		}
	}
	
	
	public Cursor fetchData(String query,String[] args)
	{
		Cursor cr=null;
		try
		{
			if(database==null||!database.isOpen())
			{
			   database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
			  //database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
			}
			
			cr=database.rawQuery(query, args);
		}
		catch(Exception e)
		{
			Log.e("Cursor", "fetch Data Exception "+e);
		}
		finally
		{
			Log.e("Cursor", "finally ");
		}
		return cr;
	}
	
	
	public boolean insertData(String table_name,ContentValues cv)
	{
		try
		{
			if(database==null||!database.isOpen())
			{
			  database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
			  //database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
			}
			
			database.insert(table_name,null,cv);
			return true;
		}
		catch(Exception e)
		{
			Log.e("Cursor", "fetch Data Exception "+e);
		}
		finally
		{
			Log.e("Cursor", "finally ");
		}
		return false;
	}
	
	
	public boolean updateData(String tablename,ContentValues cv,String whereClause)
	{
		try
		{
			if(database==null||!database.isOpen())
			{
				database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
			  //database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
			}
			
			database.update(tablename,cv,whereClause,null);
			return true;
		}
		catch(Exception e)
		{
			Log.e("Cursor", "fetch Data Exception "+e);
		}
		finally
		{
			Log.e("Cursor", "finally ");
		}
		return false;
	}
	
	
	public boolean execSql(String query)
	{
		try
		{
			if(database==null||!database.isOpen())
			{
				database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
			  //database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
			}
			
			database.execSQL(query);
			return true;
		}
		catch(Exception e)
		{
			Log.e("Cursor", "fetch Data Exception "+e);
		}
		finally
		{
			Log.e("Cursor", "finally ");
		}
		return false;
	}
	
	
	
	public boolean isOpenOrclose(){
		try{
			if (database.isOpen())
				return true;
		}catch (Exception e){
			return false;
		}
		return false;
	}
	
	public void closeDb()
	{
		if(database==null)
		{
		  database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
		  //database = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
		  database.close();
		} 
		
		else
		 database.close();
	}
	
}

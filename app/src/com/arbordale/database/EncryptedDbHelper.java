package com.arbordale.database;

import com.arbordale.util.AppConstants;
import com.arbordale.util.UtilMethods;

import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;
import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteDatabase.CursorFactory;
import net.sqlcipher.database.SQLiteOpenHelper;

public class EncryptedDbHelper { //extends SQLiteOpenHelper{

	Context context;
	private static SqliteDb sqliteDb;
	private static SQLiteDatabase mainDatabase;
	
	public EncryptedDbHelper(Context context, String name,CursorFactory factory, int version) {
		
		//super(context, name, factory, version);
		// TODO Auto-generated constructor stub
		this.context=context;
		
		try{if(context!=null)

		    SQLiteDatabase.loadLibs(context);
			
			if(!UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, context, AppConstants.KEY_DB_ENCRYPTED).equals(AppConstants.VALUE_DB_ENCRYPTED))
			{
			  try 
			   {
				SQLCipherUtils.encrypt(context,UtilMethods.getDatabaseName(AppConstants.DB_NAME), AppConstants.DB_PASSWORD);
				UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, context, AppConstants.KEY_DB_ENCRYPTED, AppConstants.VALUE_DB_ENCRYPTED);
			   } 
			  catch (Exception e) {
				System.out.println("encryption not successful");  
				e.printStackTrace();
			   }
			}
			//mainDatabase = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
			//mainDatabase = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);

		}
		catch (Exception e){
			//Log.e("database_open_or_create-error" , e.getMessage());
		}
	}

	/*@Override
	public void onCreate(SQLiteDatabase encryptedDbObj) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int prevVersion, int newVersion) {
		// TODO Auto-generated method stub
	}*/
	
	public SqliteDb getSqliteDB(){
		 
		if(sqliteDb==null){
			 sqliteDb = new SqliteDb(context);
			  }
		return sqliteDb;
	}

	public boolean IsOpenOrclose(){
		return sqliteDb.isOpenOrclose();
	}
	
	
	public void OpenDb()
	{
		mainDatabase = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),AppConstants.DB_PASSWORD, null);
		//mainDatabase = SQLiteDatabase.openOrCreateDatabase(UtilMethods.getDatabaseName(AppConstants.DB_NAME),null);
	}
	
}

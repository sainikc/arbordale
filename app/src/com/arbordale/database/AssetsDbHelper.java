
package com.arbordale.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.arbordale.util.AppConstants;
import com.arbordale.util.UtilMethods;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class AssetsDbHelper extends SQLiteAssetHelper {

	SQLiteDatabase mainDatabase;
	public AssetsDbHelper(Context mContext) {
		
		// For an alternate location of db we can change this path to internal memory or whatever
		// you must ensure that this folder is available and you have permission
		// to write to it

		super(mContext, AppConstants.DB_NAME, Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+AppConstants.APP_FOLDER, null, AppConstants.DB_VERSION);
		mainDatabase=getWritableDatabase();

		UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, mContext, AppConstants.KEY_DB_COPIED, AppConstants.VALUE_DB_COPIED);

	}
}
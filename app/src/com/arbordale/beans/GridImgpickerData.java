package com.arbordale.beans;

public class GridImgpickerData {

	private String shortTitle;
	private String status;
	private String downloadStatus;
	private String shortDescription;
	private String bookFullName;
	private String accessFlag;
	private String noOfPages;
	private String sdKeywords;
	private String sdAnimals;
	private String section="";
	private String colorCode="";
	private String author="";
	private String illustrator="";
	private String sectionName="";
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getShortTitle() {
		return shortTitle;
	}
	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDownloadStatus() {
		return downloadStatus;
	}
	public void setDownloadStatus(String downloadStatus) {
		this.downloadStatus = downloadStatus;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getBookFullName() {
		return bookFullName;
	}
	public void setBookFullName(String bookFullName) {
		this.bookFullName = bookFullName;
	}
	public String getAccessFlag() {
		return accessFlag;
	}
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	public String getNoOfPages() {
		return noOfPages;
	}
	public void setNoOfPages(String noOfPages) {
		this.noOfPages = noOfPages;
	}
	public String getSdKeywords() {
		return sdKeywords;
	}
	public void setSdKeywords(String sdKeywords) {
		this.sdKeywords = sdKeywords;
	}
	public String getSdAnimals() {
		return sdAnimals;
	}
	public void setSdAnimals(String sdAnimals) {
		this.sdAnimals = sdAnimals;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	public String getIllustrator() {
		return illustrator;
	}
	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	@Override
	public String toString() {
		return "GridImgpickerData [shortTitle=" + shortTitle + ", status="
				+ status + ", downloadStatus=" + downloadStatus
				+ ", shortDescription=" + shortDescription + ", bookFullName="
				+ bookFullName + ", accessFlag=" + accessFlag + ", noOfPages="
				+ noOfPages + ", sdKeywords=" + sdKeywords + ", sdAnimals="
				+ sdAnimals + ", section=" + section + ", colorCode="
				+ colorCode + ", author=" + author + ", illustrator="
				+ illustrator + ", sectionName=" + sectionName + "]";
	}
	
	
	
}

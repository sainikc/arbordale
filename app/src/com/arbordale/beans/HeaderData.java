package com.arbordale.beans;

public class HeaderData {

	private int sectionCount;
	private String sectionName="SECTION";
	
	public int getSectionCount() {
		return sectionCount;
	}
	public void setSectionCount(int sectionCount) {
		this.sectionCount = sectionCount;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	@Override
	public String toString() {
		return "HeaderData [sectionCount=" + sectionCount + ", sectionName="
				+ sectionName + "]";
	}
	
	
}

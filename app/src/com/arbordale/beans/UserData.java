package com.arbordale.beans;

public class UserData {
	
	private String username="";
	private String password="";
	private String siteCode="";
	private String firstName="";
	private String lastName="";
	private String email="";
	private String organizationName="";
	private String city="";
	private String state="";
	private String requireLogin="";
	private String displayDataUsage="";
	private String language="";
	private String expiryDate="";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRequireLogin() {
		return requireLogin;
	}
	public void setRequireLogin(String requireLogin) {
		this.requireLogin = requireLogin;
	}
	public String getDisplayDataUsage() {
		return displayDataUsage;
	}
	public void setDisplayDataUsage(String displayDataUsage) {
		this.displayDataUsage = displayDataUsage;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	
    
}

package com.arbordale.beans;

import java.util.ArrayList;

public class GridImageList {
	
	private ArrayList<String> sectionNameList;
	private String sectionName;
	
	public ArrayList<String> getSectionNameList() {
		return sectionNameList;
	}
	public void setSectionNameList(ArrayList<String> sectionNameList) {
		this.sectionNameList = sectionNameList;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	@Override
	public String toString() {
		return "GridImageList [sectionNameList=" + sectionNameList
				+ ", sectionName=" + sectionName + "]";
	}
	
	
}

package com.arbordale.reader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.arbordale.home.R;

import java.util.List;

public class SpeedSpinnerAdapter extends ArrayAdapter<String> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<String> elementList;

    public SpeedSpinnerAdapter(final Context context, List<String> elementList) {
        super(context, 0);
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.elementList = elementList;
    }

    @Override
    public int getCount() {
        return elementList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String getItem(int position) {
        return elementList.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        CheckedTextView row = (CheckedTextView) layoutInflater
                .inflate(R.layout.speed_spinner_item, parent, false);
        row.setText(this.elementList.get(position));
        return row;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return layoutInflater
                .inflate(R.layout.speed_spinner_rabbit_item,
                        parent,
                        false);
    }


}


package com.arbordale.reader;

import uk.co.senab.photoview.PhotoViewAttacher;
import uk.co.senab.photoview.PhotoViewAttacher.OnMatrixChangedListener;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.arbordale.home.R;
import com.arbordale.util.UtilMethods;

public class SimpleSampleActivity extends SherlockFragmentActivity {

    static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %%";
    static final String SCALE_TOAST_STRING = "Scaled to: %.2ff";


    private PhotoViewAttacher mAttacher;

    private Toast mCurrentToast;
    ActionBar actionBar;
    private AQuery aq;
    private int widthOfScreen,heightOfScreen;
    Handler mHandler;
    AlertDialog alertDialog = null;
    private String fromAutomode="0";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_fancy_box);
        
        widthOfScreen = getResources().getDisplayMetrics().widthPixels;
		heightOfScreen= getResources().getDisplayMetrics().heightPixels;
		
        aq = new AQuery(this);
        actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
	    actionBar.setTitle("Zoom");
	    actionBar.setIcon(R.drawable.splash_drawable);
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
		actionBar.hide();

        ImageView mImageView = (ImageView) findViewById(R.id.iv_photo);

        Bitmap bitmap =aq.getCachedImage(getIntent().getStringExtra("imagepath"));
        fromAutomode=getIntent().getStringExtra("fromautomode");
        
        if(bitmap==null)
        {
        	BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        	bitmap = BitmapFactory.decodeFile(getIntent().getStringExtra("imagepath").replace("%20", " "), options);
        }
        
        mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, widthOfScreen, heightOfScreen, true));
        
        mHandler=new Handler();
        
        if(CurlActivity.isActivityPaused&&CurlActivity.threadStarted==1)
        {
        	CurlActivity.threadStarted=0;
        	callHandler(CurlActivity.handlerDuration);
        }	

        
        // The MAGIC happens here!
        mAttacher = new PhotoViewAttacher(mImageView);

        // Lets attach some listeners, not required though!
        mAttacher.setOnMatrixChangeListener(new MatrixChangeListener());
        mAttacher.setOnPhotoTapListener(new PhotoTapListener());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Need to call clean-up
        mAttacher.cleanup();
    }


    private class PhotoTapListener implements OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
            //float xPercentage = x * 100f;
            //float yPercentage = y * 100f;

            //showToast(String.format(PHOTO_TAP_TOAST_STRING, xPercentage, yPercentage));
            
            if(actionBar.isShowing())
             actionBar.hide();
            
            else	
             actionBar.show();
        }
    }

    private void showToast(CharSequence text) {
        if (null != mCurrentToast) {
            mCurrentToast.cancel();
        }

        mCurrentToast = Toast.makeText(SimpleSampleActivity.this, text, Toast.LENGTH_SHORT);
        mCurrentToast.show();
    }

    private class MatrixChangeListener implements OnMatrixChangedListener {

        @Override
        public void onMatrixChanged(RectF rect) {
            //mCurrMatrixTv.setText(rect.toString());
        }
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		 switch (item.getItemId()) {
		    
		    case android.R.id.home:
		    	onBackPressed();
	           break;
		 }
		 return true;
	}
    
    @Override
    public void onBackPressed() {
    	
    	if(fromAutomode.equalsIgnoreCase("1"))
    	 CurlActivity.threadStarted=1;
    	
    	finish();
    }
    
    private void callHandler(int duration)
	{
		 mHandler.postDelayed(r,duration);
	}
	
	Runnable r=new Runnable(){
        @Override
        public void run() {
        	
        	CurlActivity.threadStarted=1;
			onBackPressed();
			
        	/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SimpleSampleActivity.this);
			// set title
			alertDialogBuilder.setTitle("Back to Reader ?");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("If you are done zooming then you may get back and keep ebook flipping!")
				.setCancelable(false).setNegativeButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						alertDialog.dismiss();
						alertDialog=null;
						CurlActivity.threadStarted=1;
						onBackPressed();
					}
				  })
				.setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						alertDialog.dismiss();
						alertDialog=null;
					}
				  });
 
				// create alert dialog
				alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();*/
        }
    };
}

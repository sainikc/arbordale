package com.arbordale.reader;

import java.util.ArrayList;

import com.arbordale.beans.SetCategoryList;
import com.arbordale.home.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MoreListAdapter extends ArrayAdapter<String>{

	private ArrayList<String> list;
	
	public MoreListAdapter(Context context,int resource,ArrayList<String> list) {
		super(context, resource,list);
		this.list=list;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dialog_list_options, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView)convertView.findViewById(R.id.top_grid_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
		
		holder.textView.setText(list.get(position));
		
		return convertView;
	}
	
	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}
	
	protected class ViewHolder {
        public TextView textView;
    }

}

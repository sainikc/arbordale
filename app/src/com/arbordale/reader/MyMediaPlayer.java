package com.arbordale.reader;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;

public class MyMediaPlayer extends MediaPlayer {

	public MyMediaPlayer(){
	    super();

	    setOnPreparedListener(new OnPreparedListener() {

	        public void onPrepared(MediaPlayer mp) {
	            start();
	        }
	    });

	    setOnErrorListener(new OnErrorListener() {


	        @Override
	        public boolean onError(MediaPlayer mp, int what, int extra) {
	             if (what == -38){
	                 return true;
	             }
	             return false;
	        }
	    });
	}

	public boolean changeSource (String urlfile){
	    try{
	        reset();
	        setAudioStreamType(AudioManager.STREAM_MUSIC);
	        setDataSource(urlfile);
	        prepare();      
	        seekTo(0);
	     }  catch (IllegalArgumentException e1) {
	        e1.printStackTrace();
	     } catch (SecurityException e1) {
	        e1.printStackTrace();
	     } catch (IllegalStateException e1) {
	        e1.printStackTrace();
	     } catch (IOException e1) {
	        e1.printStackTrace();
	    }
	    start();
	    return true;        
	}

	}

package com.arbordale.reader;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.AsyncTaskCompleteListener;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.home.WebServiceCallTask;
import com.arbordale.inapp.InAppPurchaseActivity;
import com.arbordale.navfragments.WebPageActivity;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.PathUtil;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;

import org.apache.commons.io.FileUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import it.sephiroth.android.library.widget.HorizontalListView.OnLayoutChangeListener;
import it.sephiroth.android.library.widget.HorizontalVariableListView;
import it.sephiroth.android.library.widget.HorizontalVariableListView.OnItemClickedListener;
import it.sephiroth.android.library.widget.HorizontalVariableListView.SelectionMode;

import static android.os.Build.VERSION.SDK_INT;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class CurlActivity extends SherlockActivity implements OnClickListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener,
        AsyncTaskCompleteListener<String>,
        OnItemSelectedListener {

    private static RelativeLayout bottompanel;
    private CurlView mCurlView;
    Button btnLang, options, buyButton;
    ImageView btnMore;
    //Button btnRead;
    //static ImageView refresh;
    static ImageView zoom;
    ImageView btnHome, btnFirst, btnPrev, btnNext, btnLast, showgrid, btnAuto;
    public static TextView pageNo;
    private AQuery aq;
    Drawable d = null;
    private static HorizontalVariableListView mList;
    TextView mText;

    List<String> soundUrls, imageUrls;

    MediaPlayer mPlayer;
    Bitmap y, thumbnail;
    File f;

    //to call web service
    WebServiceCallTask webCall;
    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    private static int step = 0;

    //option list
    ArrayList<String> optionList = new ArrayList<String>();

    //reader activity
    public static int motionDirection = 10, jump = 0, threadStarted = 0, playSound = 1, languageSwitched = 0, currentIndex = 0, lastPageSelected = 0, handlerDuration = 5000;
    public static boolean isActivityPaused = false;

    //for last page
    public static int noOfImagesDownloaded = 1;

    private static String folderName = "", chapterName = "", noOfPages = "0", status = "", title_id = "", colorcode = "0";
    String noOfPagesPurchase = "", bookFullName = "";
    int totalimages = 0;

    Handler mHandler;

    ProgressBar pb;

    MoreListAdapter adapter_dialog;

    int width, height, density;

    ArrayList<String> activityList = new ArrayList<String>();
    ActionBar actionBar;

    //To download images in a queue
    LongOperation streamFromServer;

    //for forward flip
    String url = "";
    ListAdapter adapter;
    Spinner speedSpinner;
    static float playSpeed = 1.0f;
    private boolean isNewLaunch = true;


    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        actionBar = getSupportActionBar();

        if (actionBar != null)
            actionBar.hide();

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        width = display.getWidth();
        height = display.getHeight();

        setContentView(R.layout.mainx);
        aq = new AQuery(this);
        mPlayer = new MediaPlayer();
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);

        chapterName = getIntent().getStringExtra("chaptername");
        noOfPages = getIntent().getStringExtra("no_of_pages");
        status = getIntent().getStringExtra("mode");
        noOfPagesPurchase = getIntent().getStringExtra("no_of_pages");
        bookFullName = getIntent().getStringExtra("book_full_name");
        colorcode = getIntent().getStringExtra("colorcode");

        Log.i(AppConstants.TAG_CURL, "..." + chapterName + "/" + noOfPages + "/" + status + "/" + noOfPagesPurchase + "/" + bookFullName + "/" + colorcode);

        //To fetch title id
        SqliteDb db_conn = new SqliteDb(CurlActivity.this);
        Cursor records = db_conn.fetchData(QueryManager.QUERY_FETCH_TITLE_ID, new String[]{chapterName});
        if (records != null && records.getCount() > 0) {
            records.moveToFirst();
            title_id = "" + records.getInt(0);
            System.out.println("title_id at CurlActivity:" + title_id);
        }

        if (records != null) {
            records.close();
        }
        db_conn.closeDb();
        //Title id fetched

        mCurlView = (CurlView) findViewById(R.id.curl);
        //mCurlView.setKeepScreenOn(true);

        if (colorcode != null && !colorcode.equalsIgnoreCase(""))
            mCurlView.setBackgroundColor(Color.parseColor(colorcode));

        else
            mCurlView.setBackgroundColor(Color.parseColor("#ffffff"));

        mCurlView.setAllowLastPageCurl(false);

        pb = (ProgressBar) findViewById(R.id.progressBar1);
        btnHome = (ImageView) findViewById(R.id.home);
        btnFirst = (ImageView) findViewById(R.id.btnfirst);
        btnPrev = (ImageView) findViewById(R.id.btnprev);
        btnNext = (ImageView) findViewById(R.id.btnnext);
        btnLast = (ImageView) findViewById(R.id.index);
        btnAuto = (ImageView) findViewById(R.id.auto);
        showgrid = (ImageView) findViewById(R.id.showgrid);
        speedSpinner = (Spinner) findViewById(R.id.playspeed);
        pageNo = (TextView) findViewById(R.id.pageno);
        btnMore = (ImageView) findViewById(R.id.more);
        bottompanel = (RelativeLayout) findViewById(R.id.bottompanel);
        buyButton = (Button) findViewById(R.id.buy);
        mList = (HorizontalVariableListView) findViewById(R.id.list);
        zoom = (ImageView) findViewById(R.id.zoom);
        //refresh=(ImageView)findViewById(R.id.refresh);

        btnHome.setOnClickListener(this);
        btnFirst.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnAuto.setOnClickListener(this);
        showgrid.setOnClickListener(this);
        btnLast.setOnClickListener(this);
        btnMore.setOnClickListener(this);
        bottompanel.setOnClickListener(this);
        buyButton.setOnClickListener(this);
        zoom.setOnClickListener(this);
        //refresh.setOnClickListener(this);
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.playspeed);
        if (SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {

            // Spinner click listener
            spinner.setOnItemSelectedListener(this);
            // Spinner Drop down elements
            List<String> categories = new ArrayList<String>();
            categories.add("1.2");
            categories.add("1.1");
            categories.add("1.0");
            categories.add("0.9");
            categories.add("0.8");
            categories.add("0.7");

            // Creating adapter for spinner
            //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

            // Drop down layout style - list view with radio button
            //dataAdapter.setDropDownViewResource(R.layout.speed_spinner_item);
            ArrayAdapter<String> dataAdapter = new SpeedSpinnerAdapter(this, categories);
            // attaching data adapter to spinner
            spinner.setAdapter(dataAdapter);
            //spinner.setSelection(((ArrayAdapter<String>) spinner.getAdapter()).getPosition("1.0"));
        } else {
            spinner.setVisibility(View.GONE);
        }

        dataLogics();

        //btnRead=(Button)findViewById(R.id.soundcontrol);
        btnLang = (Button) findViewById(R.id.spanish);

        //btnRead.setOnClickListener(this);
        btnLang.setOnClickListener(this);
        //btnRead.setText("Read to Me");

        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_ENGLISH))
            btnLang.setText(AppConstants.VALUE_SPANISH);

        else
            btnLang.setText(AppConstants.VALUE_ENGLISH);

        adapter = new ListAdapter(this, R.layout.view1, soundUrls);

        // change the selection mode: single or multiple ; children gravity ( top, center, bottom )
        mList.setSelectionMode(SelectionMode.Single);
        mList.setOverScrollMode(HorizontalVariableListView.OVER_SCROLL_ALWAYS);
        mList.setEdgeGravityY(Gravity.CENTER);
        mList.setAdapter(adapter);
        mList.setGravity(Gravity.CENTER);

        //Create cache directory to move back with cached image
        if (folderName.equalsIgnoreCase("")) {
            File ext = Environment.getExternalStorageDirectory();
            File cacheDir = new File(ext, AppConstants.APP_FOLDER + "/" + chapterName + "_cached");

            if (!cacheDir.exists()) {
                cacheDir.mkdirs();
            } else {
                new ClearCache().execute(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + chapterName + "_cached");
            }

            AQUtility.setCacheDir(cacheDir);
            AjaxCallback.setNetworkLimit(20);
        }

        bookReadLog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        refresh(currentIndex);
        hideShowBottomPanel();
    }


    void refresh(final int index) {
        Log.i(AppConstants.TAG_CURL, "Call to Refresh();");

        if (folderName.equalsIgnoreCase("")) {
            y = aq.getCachedImage(imageUrls.get(index));
            f = aq.getCachedFile(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

            Log.i(AppConstants.TAG_CURL, "Web Streaming...");

            if (y == null && f == null) {

                Log.i(AppConstants.TAG_CURL, "Refresh files not cached " + imageUrls.get(index));

                pb.setVisibility(View.VISIBLE);

                aq.ajax(imageUrls.get(index), Bitmap.class, 0, new AjaxCallback<Bitmap>() {
                    @Override
                    public void callback(String url, Bitmap object, AjaxStatus status) {
                        y = object;

                        aq.ajax(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {

                            @Override
                            public void callback(String url, File object, AjaxStatus status) {

                                f = object;

                                if (f != null) {
                                    Log.i(AppConstants.TAG_CURL, "First Image Downloaded! " + status.getCode());

                                    if (index == 0 && status.getCode() == 200)
                                        //To download second
                                        aq.ajax(imageUrls.get(index + 1), Bitmap.class, 0, new AjaxCallback<Bitmap>() {

                                            @Override
                                            public void callback(String url, Bitmap object, AjaxStatus status) {
                                                aq.ajax(imageUrls.get(index + 1).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {
                                                    @Override
                                                    public void callback(String url, File object, AjaxStatus status) {
                                                        Log.i(AppConstants.TAG_CURL, "Second Image Downloaded! " + status.getCode());

                                                        if (status.getCode() == 200)
                                                            //To download Third
                                                            aq.ajax(imageUrls.get(index + 2), Bitmap.class, 0, new AjaxCallback<Bitmap>() {

                                                                @Override
                                                                public void callback(String url, Bitmap object, AjaxStatus status) {
                                                                    aq.ajax(imageUrls.get(index + 2).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {
                                                                        @Override
                                                                        public void callback(String url, File object, AjaxStatus status) {
                                                                            Log.i(AppConstants.TAG_CURL, "Third Image Downloaded! " + status.getCode());

                                                                            mCurlView.setPageProvider(new PageProvider());
                                                                            mCurlView.setSizeChangedObserver(new SizeChangedObserver());
                                                                            mCurlView.setCurrentIndex(index);

                                                                            if (streamFromServer == null || streamFromServer.getStatus() != AsyncTask.Status.RUNNING) {
                                                                                streamFromServer = new LongOperation();
                                                                                streamFromServer.execute("");

                                                                                Log.i(AppConstants.TAG_CURL, "LongOperation(); Started! with Index " + index);
                                                                            }
                                                                        }

                                                                    });
                                                                }
                                                            });
                                                    }

                                                });
                                            }
                                        });

                                    else {
                                        mCurlView.setPageProvider(new PageProvider());
                                        mCurlView.setSizeChangedObserver(new SizeChangedObserver());
                                        mCurlView.setCurrentIndex(index);

                                        if (streamFromServer == null || streamFromServer.getStatus() != AsyncTask.Status.RUNNING) {
                                            streamFromServer = new LongOperation();
                                            streamFromServer.execute("");

                                            Log.i(AppConstants.TAG_CURL, "LongOperation(); Started! with Index " + index);
                                        }
                                    }

                                }
                            }
                        });
                    }
                });
            } else {
                Log.i(AppConstants.TAG_CURL, "refresh(); Files already cached!");

                pb.setVisibility(View.INVISIBLE);
                mCurlView.setPageProvider(new PageProvider());
                mCurlView.setSizeChangedObserver(new SizeChangedObserver());
                mCurlView.setCurrentIndex(index);
            }
        } else {
            Log.i(AppConstants.TAG_CURL, "refresh(); Downloaded Images mode!");

            pb.setVisibility(View.INVISIBLE);
            mCurlView.setPageProvider(new PageProvider());
            mCurlView.setSizeChangedObserver(new SizeChangedObserver());
            mCurlView.setCurrentIndex(index);
        }
        jump = 0;
    }


    @Override
    public void onResume() {
        super.onResume();

        mCurlView.onResume();

        isActivityPaused = false;

        if (threadStarted == 1) {
            mCurlView.nextPage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCurlView.onPause();

        isActivityPaused = true;

        currentIndex = mCurlView.getCurrentIndex();
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        return mCurlView.getCurrentIndex();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        playSpeed = Float.parseFloat(item);

        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + playSpeed, Toast.LENGTH_LONG).show();
        if (mPlayer != null && !isNewLaunch) {
            if (SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    if (mPlayer.isPlaying()) {
                        mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(playSpeed));
                    }
                } catch (IllegalStateException e) {
                    String err = (e.getMessage() == null) ? "IllegalStateException occurred."
                            : e.getMessage();
                    Log.e("CurlActivity", err);
                }

            }
        } else {
            isNewLaunch = false;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Bitmap provider.
     */
    private class PageProvider implements CurlView.PageProvider {

        @Override
        public int getPageCount() {
            return imageUrls.size();
        }

        private Bitmap loadBitmap(int width, int height, final int index) throws MalformedURLException, IOException {

            Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            if (colorcode != null && colorcode.length() > 0)
                b.eraseColor(Color.parseColor(colorcode));
            Canvas c = new Canvas(b);

            if (folderName.equalsIgnoreCase("")) {
                if (CurlActivity.motionDirection == 11) {
                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        y = aq.getCachedImage(imageUrls.get(index));

                        if (y != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pb.setVisibility(View.INVISIBLE);
                                }
                            });
                        } else if (y == null && f == null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pb.setVisibility(View.VISIBLE);
                                }
                            });

                            aq.ajax(imageUrls.get(index), Bitmap.class, 0, new AjaxCallback<Bitmap>() {
                                @Override
                                public void callback(final String urlimg, final Bitmap object1, AjaxStatus status) {
                                    if (object1 != null)
                                        try {
                                            y = object1;

                                            aq.ajax(imageUrls.get(index + 1).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {
                                                @Override
                                                public void callback(String url, File object, AjaxStatus status) {
                                                    try {
                                                        if (object != null || status.getCode() == 404) {
                                                            Log.i(AppConstants.TAG_CURL, "Backward Flip downloaded " + urlimg.replace("http://arbordalepublishing.com/eBooks3/", "") + "/" + (index) + "/" + mCurlView.getCurrentIndex());

                                                            mCurlView.setCurrentIndex(currentIndex);
                                                            mCurlView.requestRender();
                                                            pb.setVisibility(View.INVISIBLE);

                                                            FileInputStream inputStream = new FileInputStream(object);

                                                            if (index > 0) {
                                                                mPlayer.stop();
                                                                mPlayer.reset();
                                                            }

                                                            if (playSound == 0)
                                                                prepareMediaPlayer(inputStream.getFD());

                                                            inputStream.close();
                                                        }
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                            }
                        });
                    }
                } else {
                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        //hide progress-bar
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pb.setVisibility(View.INVISIBLE);
                            }
                        });

                        y = aq.getCachedImage(imageUrls.get(index));

                        if (y != null && f != null) {
                            if (threadStarted == 1 && index == 0) {
                                f = aq.getCachedFile(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));
                                FileInputStream inputStream = new FileInputStream(f);

                                if (playSound == 0)
                                    prepareMediaPlayer(inputStream.getFD());
                                inputStream.close();
                            } else if (currentIndex == (Integer.parseInt(noOfPages) - 1)) {
                                if (!noOfPages.equalsIgnoreCase("0")) {
                                    f = aq.getCachedFile(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                                    if (f == null) {
                                        if (mPlayer != null) {
                                            mPlayer.stop();
                                            mPlayer.reset();
                                        }
                                    }
                                }
                            }
                        } else if (y != null && f == null) {
                            aq.ajax(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {
                                @Override
                                public void callback(String url, File object, AjaxStatus status) {

                                    try {
                                        if (object != null || status.getCode() != 404) {
                                            f = object;

                                            FileInputStream inputStream = new FileInputStream(object);

                                            if (playSound == 0)
                                                prepareMediaPlayer(inputStream.getFD());
                                            inputStream.close();
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            });
                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pb.setVisibility(View.VISIBLE);
                                }
                            });


                            try {
                                url = imageUrls.get(index + 1);
                            } catch (IndexOutOfBoundsException e) {
                                lastPageSelected = 1;
                                url = imageUrls.get(index);
                            }

                            aq.ajax(url, Bitmap.class, 0, new AjaxCallback<Bitmap>() {
                                @Override
                                public void callback(final String urlimg, final Bitmap object1, AjaxStatus status) {
                                    if (object1 != null)
                                        try {
                                            y = object1;

                                            aq.ajax(url.replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {
                                                @Override
                                                public void callback(String url, File object, AjaxStatus status) {

                                                    try {
                                                        if (object != null || status.getCode() != 404) {
                                                            Log.i(AppConstants.TAG_CURL, "Forward Flip downloaded " + urlimg.replace("http://arbordalepublishing.com/eBooks3/", "") + "/" + mCurlView.getCurrentIndex() + ":" + (index + 1));

                                                            if (mCurlView.getCurrentIndex() == (index + 1)) {
                                                                mCurlView.setCurrentIndex(currentIndex);
                                                                mCurlView.requestRender();
                                                                pb.setVisibility(View.INVISIBLE);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                            }
                        });
                    }
                }

                d = new BitmapDrawable(getResources(), y);

                if (y != null) {
                    int margin = 0;
                    int border = 0;
                    int imageWidth;
                    int imageHeight;

                    Rect r = new Rect(margin, margin, width - margin, height - margin);

                    if (imageUrls.get(index).contains("p1.jpg") && d.getIntrinsicWidth() > 0) {
                        imageWidth = r.width() - (border * 2);
                        imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
                        if (imageHeight > r.height() - (border * 2)) {
                            imageHeight = r.height() - (border * 2);
                            imageWidth = imageHeight * d.getIntrinsicWidth() / d.getIntrinsicHeight();
                        }
                    } else {
                        imageWidth = r.width() - (border * 2);
                        imageHeight = r.height() - (border * 2);
                    }

                    r.left += ((r.width() - imageWidth) / 2) - border;
                    r.right = r.left + imageWidth + border + border;
                    r.top += ((r.height() - imageHeight) / 2) - border;
                    r.bottom = r.top + imageHeight + border + border;

                    Paint p = new Paint();
                    p.setColor(0xFFC0C0C0);
                    c.drawRect(r, p);
                    r.left += border;
                    r.right -= border;
                    r.top += border;
                    r.bottom -= border;

                    d.setBounds(r);
                    d.draw(c);

                }


                if (jump == 1 || languageSwitched == 1) {
                    if (mPlayer != null) {
                        mPlayer.stop();
                        mPlayer.reset();
                    }

                    f = aq.getCachedFile(imageUrls.get(mCurlView.getCurrentIndex()).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                    if (f != null) {
                        try {
                            FileInputStream inputStream = new FileInputStream(f);

                            if (playSound == 0)
                                prepareMediaPlayer(inputStream.getFD());

                            inputStream.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } else {

                File photo = new File(getImageUri(imageUrls.get(index)));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath(), options);

                Drawable d = new BitmapDrawable(getResources(), bitmap);

                int margin = 0;
                int border = 0;
                int imageWidth;
                int imageHeight;

                Rect r = new Rect(margin, margin, width - margin, height - margin);

                if (imageUrls.get(index).contains("p1.jpg") && d.getIntrinsicWidth() > 0) {
                    imageWidth = r.width() - (border * 2);
                    imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
                    if (imageHeight > r.height() - (border * 2)) {
                        imageHeight = r.height() - (border * 2);
                        imageWidth = imageHeight * d.getIntrinsicWidth() / d.getIntrinsicHeight();
                    }
                } else {
                    imageWidth = r.width() - (border * 2);
                    imageHeight = r.height() - (border * 2);
                }

           /* int imageWidth = r.width() - (border * 2);
            int imageHeight = r.height() - (border * 2);*/

                r.left += ((r.width() - imageWidth) / 2) - border;
                r.right = r.left + imageWidth + border + border;
                r.top += ((r.height() - imageHeight) / 2) - border;
                r.bottom = r.top + imageHeight + border + border;

                Paint p = new Paint();
                p.setColor(0xFFC0C0C0);
                c.drawRect(r, p);
                r.left += border;
                r.right -= border;
                r.top += border;
                r.bottom -= border;

                d.setBounds(r);
                d.draw(c);

                if (jump == 1 || languageSwitched == 1) {
                    if (mPlayer != null) {
                        mPlayer.stop();
                        mPlayer.reset();
                    }

                    f = new File(getMp3Uri(imageUrls.get(mCurlView.getCurrentIndex())));

                    if (f.exists() != false) {
                        FileInputStream inputStream;
                        try {
                            inputStream = new FileInputStream(f);

                            if (playSound == 0)
                                prepareMediaPlayer(inputStream.getFD());

                            inputStream.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return b;
        }

        @Override
        public void updatePage(CurlPage page, final int width, final int height, final int index) {

            Bitmap front = null;

            try {
                front = loadBitmap(width, height, index);

                if (front != null) {
                    page.setTexture(front, CurlPage.SIDE_FRONT);
                    page.setColor(Color.rgb(180, 180, 180), CurlPage.SIDE_BACK);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void playAudio(final int index) {

            currentIndex = index;

            if (folderName.equalsIgnoreCase("")) {
                if (mPlayer != null) {
                    mPlayer.stop();
                    mPlayer.reset();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pageNo.setText("  " + (index + 1) + "  ");
                    }
                });

                f = aq.getCachedFile(imageUrls.get(index).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                if (f != null) {
                    try {
                        FileInputStream inputStream = new FileInputStream(f);

                        if (playSound == 0)
                            prepareMediaPlayer(inputStream.getFD());

                        inputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (mPlayer != null) {
                    mPlayer.stop();
                    mPlayer.reset();
                }
                // TODO
                f = new File(getMp3Uri(imageUrls.get(index)));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pageNo.setText("  " + (index + 1) + "  ");
                    }
                });


             /*if(index==Integer.parseInt(noOfPages)-1)
                {
            	lastPage=false;
   		     }*/

                if (f.exists() != false) {
                    FileInputStream inputStream;
                    try {
                        inputStream = new FileInputStream(f);

                        if (playSound == 0)
                            prepareMediaPlayer(inputStream.getFD());

                        inputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }

        private String getImageUri(String str) {
            return str.replace("%20", " ").replace(".jpg", ".b");
        }

        private String getMp3Uri(String str) {
            return str.replace("%20", " ").replace(".jpg", ".a");
        }

    }

    /**
     * CurlView size changed observer.
     */
    private class SizeChangedObserver implements CurlView.SizeChangedObserver {
        @Override
        public void onSizeChanged(int w, int h) {
            mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
            mCurlView.setMargins(0, 0, 0, 0);
        }
    }


    @Override
    public void onContentChanged() {
        super.onContentChanged();

        mList = (HorizontalVariableListView) findViewById(R.id.list);
        //mText = (TextView) findViewById( R.id.text );
        mList.setOnLayoutChangeListener(new OnLayoutChangeListener() {

            @Override
            public void onLayoutChange(boolean changed, int left, int top, int right, int bottom) {
                if (changed) {

                    if (mList != null)
                        mList.setEdgeHeight(bottom - top);
                }
            }
        });

        mList.setOnItemClickedListener(new OnItemClickedListener() {

            @Override
            public boolean onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("hh", "onItemClick: " + position);

                // item has been clicked, return true if you want the
                // HorizontalVariableList to handle the event
                // false otherwise
                return true;
            }
        });

        mList.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //mText.setText( "item selected: " + position + ", selected items: " + mList.getSelectedPositions().length );
            }

            @Override
            public void onNothingSelected(android.widget.AdapterView<?> parent) {
                //mText.setText( "nothing selected" );
            }

            ;

        });

        //let's select the first item by default
        mList.setSelectedPosition(0, false);
    }


    class ListAdapter extends ArrayAdapter<String> {

        int resId1;
        int resId2;
        List<String> imagePaths;

        public ListAdapter(Context context, int textViewResourceId, List<String> objects) {
            super(context, textViewResourceId, objects);
            resId1 = textViewResourceId;
            //resId2 = dividerResourceId;
            this.imagePaths = objects;
        }

        @Override
        public int getCount() {
            return imagePaths.size();
        }

        @Override
        public String getItem(int position) {
            return imagePaths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return imagePaths.get(position).hashCode();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view;
            int type = getItemViewType(position);

            if (convertView == null) {
                view = LayoutInflater.from(getContext()).inflate(type == 0 ? resId1 : resId1, parent, false);
                view.setLayoutParams(new HorizontalVariableListView.LayoutParams(HorizontalVariableListView.LayoutParams.WRAP_CONTENT,
                        HorizontalVariableListView.LayoutParams.WRAP_CONTENT));
            } else {
                view = convertView;
            }

            if (type == 0) {
                //TextView text = (TextView) view.findViewById( R.id.text );
                //text.setText( "Image " + String.valueOf( position ));
            }
            final ImageView im = (ImageView) view.findViewById(R.id.image);

            if (folderName.equals(""))
                thumbnail = aq.getCachedImage(imagePaths.get(position));

            else {
                File photo = new File(imagePaths.get(position).replace("%20", " "));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                thumbnail = BitmapFactory.decodeFile(photo.getAbsolutePath(), options);
            }

            if (thumbnail == null) {
                aq.id(im).progress(R.id.downloadprogress).image(imagePaths.get(position), true, true, 0, 0, new BitmapAjaxCallback() {

                    @Override
                    public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                        im.setImageBitmap(bm);
                        if (bm != null) {
                            aq.id(R.id.downloadprogress).gone();
                        }
                    }
                });
            } else {
                im.setImageBitmap(thumbnail);
            }

            im.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    jump = 1;
                    currentIndex = position;

                    mCurlView.setCurrentIndex(currentIndex);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pageNo.setText("  " + (position + 1) + "  ");
                        }
                    });
                    findViewById(R.id.list).setVisibility(View.INVISIBLE);
                }
            });

            return view;
        }
    }

    public void prepareMediaPlayer(FileDescriptor fd) {
        try {
            if (fd != null && mPlayer != null) {
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setDataSource(fd);
                mPlayer.prepareAsync();
                if (SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    try {
                        if (mPlayer.isPlaying()) {
                            mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(playSpeed));
                        }
                    } catch (IllegalStateException e) {
                        String err = (e.getMessage() == null) ? "IllegalStateException occurred."
                                : e.getMessage();
                        Log.e("CurlActivity", err);
                    }
                }
            }
        } catch (IOException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalStateException e) {
        }
    }

    @Override
    public void onPrepared(MediaPlayer arg0) {

        if (playSound == 0) {
            mPlayer.start();
            if (SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    if (mPlayer.isPlaying()) {
                        mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(playSpeed));
                    }

                } catch (IllegalStateException e) {
                    String err = (e.getMessage() == null) ? "IllegalStateException occurred."
                            : e.getMessage();
                    Log.e("CurlActivity", err);
                }
            }

        }

    }

    @Override
    public void onCompletion(MediaPlayer mp) {

        if (threadStarted == 1) {
            if (isActivityPaused == false) {
                mCurlView.nextPage();
            }
        }
    }


    //To download images/sounds passed as urls
    public class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            for (int i = 0; i < imageUrls.size(); i++) {
                y = aq.getCachedImage(imageUrls.get(i));
                f = aq.getCachedFile(imageUrls.get(i).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                if (y == null)
                    aq.ajax(imageUrls.get(i), Bitmap.class, 0, new AjaxCallback<Bitmap>() {
                        @Override
                        public void callback(final String urlimg, final Bitmap object1, AjaxStatus status) {
                            noOfImagesDownloaded++;
                            lastFlip(noOfImagesDownloaded);
                            //Log.i(AppConstants.TAG_CURL,"AsyncTask Longoperation(); Image downloaded "+urlimg.replace("http://arbordalepublishing.com/eBooks3/", "").replace("%20", ""));
                        }
                    });

                if (f == null)
                    aq.ajax(imageUrls.get(i).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3), File.class, 0, new AjaxCallback<File>() {

                        @Override
                        public void callback(String url, File object, AjaxStatus status) {
                        }
                    });
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new ClearCache().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + chapterName + "_cached");
        } else {
            new ClearCache().execute(Environment.getExternalStorageDirectory() + "/" + AppConstants.APP_FOLDER + "/" + chapterName + "_cached");
        }


        aq.ajaxCancel();
        threadStarted = 0;
        noOfImagesDownloaded = 0;
        folderName = "";
        chapterName = "";
        noOfPages = "1";
        status = "";
        playSound = 1;
        mPlayer.release();
        mPlayer = null;
        mList = null;
        currentIndex = 0;

        mCurlView.setCurrentIndex(0);

        if (mHandler != null)
            mHandler.removeCallbacksAndMessages(null);

        if (mPlayer != null)
            if (mPlayer.isPlaying())
                mPlayer.stop();


        GlobalVars.HOME_READER_PRESSED = "true";
        GlobalVars.BACK_FROM_PURCHASE_DOWNLOAD = "false";

        Intent it = new Intent(CurlActivity.this, HomeScreen.class);
        startActivity(it);

        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        aq.ajaxCancel();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.home) {
            onBackPressed();
        } else if (v.getId() == R.id.soundcontrol) {
            soundPlayPause();
        } else if (v.getId() == R.id.spanish) {
            noOfImagesDownloaded = 1;

            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.reset();
            }

            String lang = btnLang.getText().toString();

            if (lang.equalsIgnoreCase(AppConstants.VALUE_ENGLISH)) {
                btnLang.setText(AppConstants.VALUE_SPANISH);
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER, AppConstants.VALUE_SPANISH);
            } else {
                btnLang.setText(AppConstants.VALUE_ENGLISH);
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER, AppConstants.VALUE_ENGLISH);
            }

            new RefreshData().execute("");
        } else if (v.getId() == R.id.btnfirst) {
            currentIndex = 0;
            jump = 1;
            refresh(0);
            pageNo.setText("" + 1);
        } else if (v.getId() == R.id.btnprev) {
            jump = 0;
            mCurlView.prevPage();
        } else if (v.getId() == R.id.btnnext) {
            jump = 0;
            mCurlView.nextPage();
        } else if (v.getId() == R.id.index) {
            lastPageSelected = 1;
            jump = 1;
            currentIndex = Integer.parseInt(noOfPages) - 1;

            if (folderName.equalsIgnoreCase("") && aq.getCachedFile(imageUrls.get(currentIndex).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3)) == null)
                refresh(currentIndex);

            else if (folderName.equalsIgnoreCase("") && aq.getCachedFile(imageUrls.get(currentIndex).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3)) != null)
                mCurlView.setCurrentIndex(currentIndex);

            else
                mCurlView.setCurrentIndex(currentIndex);

            pageNo.setText("" + imageUrls.size());
        } else if (v.getId() == R.id.auto) {
            //Toast.makeText(this, "Hi! Rkumar",Toast.LENGTH_SHORT).show();
            if (threadStarted == 1) {
                if (mPlayer != null) {
                    mPlayer.stop();
                    mPlayer.reset();
                }

			/*if(btnRead!=null)
              btnRead.setText("Read to Me");*/

                playSound = 1;
                threadStarted = 0;
                btnAuto.setImageResource(R.drawable.play);
                //lastPage=false;

                if (mHandler != null)
                    mHandler.removeCallbacksAndMessages(null);
            } else {
                soundPlayPause();

                //lastPage=true;
                threadStarted = 1;
                playSound = 0;

                btnAuto.setImageResource(R.drawable.pause);

                mHandler = new Handler();
                if (folderName.equalsIgnoreCase("")) {

                } else {
                    //callHandler(mCurlView.getCurrentIndex());
                }
            }
        } else if (v.getId() == R.id.showgrid) {
            if (mList.getVisibility() == View.INVISIBLE) {
                adapter.notifyDataSetChanged();
                mList.setVisibility(View.VISIBLE);
            } else {
                mList.setVisibility(View.INVISIBLE);
            }
        } else if (v.getId() == R.id.more) {
            adapter_dialog = null;

            step = 0;
            params.clear();
            params.add(new BasicNameValuePair("id", title_id));

            if (!(activityList.size() > 1)) {
                activityList.clear();

                if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                    webCall = new WebServiceCallTask(CurlActivity.this, CurlActivity.this);
                    webCall.execute(AppConstants.SERVICE_CHECK_RELATED_WEBSITES, params);
                } else {
                    UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                }
            } else {
                adapter_dialog = new MoreListAdapter(CurlActivity.this, android.R.layout.simple_list_item_1, activityList);
                createSpinnerDialog(CurlActivity.this, adapter_dialog, width, height);
            }
        } else if (v.getId() == R.id.bottompanel) {

        } else if (v.getId() == R.id.buy) {
            Intent it = new Intent(CurlActivity.this, InAppPurchaseActivity.class);
            it.putExtra("book_name", chapterName);
            it.putExtra("book_full_name", bookFullName);
            it.putExtra("file_set_name", "set");
            it.putExtra("no_of_pages", noOfPagesPurchase);
            it.putExtra("colorcode", colorcode);
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);
        } else if (v.getId() == R.id.zoom) {
            if (mPlayer != null) {
                handlerDuration = mPlayer.getDuration() - mPlayer.getCurrentPosition();
            }

            if (folderName.equalsIgnoreCase("")) {
                File f = aq.getCachedFile(imageUrls.get(currentIndex).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));
                if (f != null) {
                    Intent it = new Intent(CurlActivity.this, SimpleSampleActivity.class);
                    it.putExtra("imagepath", imageUrls.get(mCurlView.getCurrentIndex()));

                    if (threadStarted == 1)
                        it.putExtra("fromautomode", "1");

                    else
                        it.putExtra("fromautomode", "0");

                    startActivity(it);
                } else {
                    UtilMethods.createToast(CurlActivity.this, "Please wait! Loading Image..");
                }
            } else {
                Intent it = new Intent(CurlActivity.this, SimpleSampleActivity.class);
                it.putExtra("imagepath", imageUrls.get(mCurlView.getCurrentIndex()));


                if (threadStarted == 1)
                    it.putExtra("fromautomode", "1");

                else
                    it.putExtra("fromautomode", "0");

                startActivity(it);
            }
        }

		/*else if(v.getId()==R.id.refresh)
        {
			UtilMethods.createToast(CurlActivity.this, "Loading Image...");

			if(aq.getCachedFile(imageUrls.get(mCurlView.getCurrentIndex()))!=null)
			{
				//refresh.setEnabled(false);
				mCurlView.setCurrentIndex(mCurlView.getCurrentIndex());
				mCurlView.requestRender();
				//refresh.setEnabled(true);
				pb.setVisibility(View.INVISIBLE);
			}

			else
			{
				aq.ajax(imageUrls.get(mCurlView.getCurrentIndex()), Bitmap.class,0, new AjaxCallback<Bitmap>() {
					@Override
			        public void callback(final String urlimg, final Bitmap object1, AjaxStatus status) {

					}
				});
			}
		}*/
    }

    public static void updateTextOnBack(int index) {
        pageNo.setText("  " + index + "  ");
    }


    protected void onDestroy() {
        super.onDestroy();

        Log.i(AppConstants.TAG_CURL, "Cleaning aquery cache " + isTaskRoot());

        if (isTaskRoot()) {
            AQUtility.cleanCacheAsync(this);
        }
    }

    @Override
    public void onLowMemory() {
        AQUtility.cleanCacheAsync(this);
        BitmapAjaxCallback.clearCache();
        super.onLowMemory();
    }

    private void createSpinnerDialog(Context ctx, MoreListAdapter adapter, int width, int positiony) {
        final Dialog d = new Dialog(ctx, R.style.FullHeightDialog);
        d.setContentView(R.layout.spinner_dialog);
        d.setCanceledOnTouchOutside(true);

        ListView ls = (ListView) d.findViewById(R.id.list);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                String selected = (String) arg0.getItemAtPosition(position);

                if (selected.equalsIgnoreCase("Teaching Activity")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("") &&
                            UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")) {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                "Dummy"
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=ta&args=" + title_id + "," + chapterName;
                    } else {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=ta&args=" + title_id + "," + chapterName;
                    }

                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        Intent it = new Intent(CurlActivity.this, WebPageActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        it.putExtra("from", "reader");
                        it.putExtra("action_item", "Teaching Activity");
                        startActivity(it);
                    } else {
                        UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                } else if (selected.equalsIgnoreCase("Quiz - Reading Comprehension")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("") &&
                            UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")) {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                "Dummy" + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",1";
                    } else {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",1";
                    }

                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        Intent it = new Intent(CurlActivity.this, WebPageActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        it.putExtra("from", "reader");
                        it.putExtra("action_item", "Quiz - Reading Comprehension");
                        startActivity(it);
                    } else {
                        UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                } else if (selected.equalsIgnoreCase("Quiz - For Creative Minds")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("") &&
                            UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")) {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                "Dummy" + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",2";
                    } else {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",2";
                    }

                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        Intent it = new Intent(CurlActivity.this, WebPageActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        it.putExtra("from", "reader");
                        it.putExtra("action_item", "Quiz - For Creative Minds");
                        startActivity(it);
                    } else {
                        UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                } else if (selected.equalsIgnoreCase("Quiz - Math Word Problems")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("") &&
                            UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")) {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                "Dummy" + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",3";
                    } else {
                        GlobalVars.URL = AppConstants.URL_POPUP + "&c=" +
                                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
                                + "&w=quiz&args=" + title_id + "," + chapterName + ",3";
                    }

                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        Intent it = new Intent(CurlActivity.this, WebPageActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        it.putExtra("from", "reader");
                        it.putExtra("action_item", "Quiz - Math Word Problems");
                        startActivity(it);
                    } else {
                        UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                }

//			    	else if(selected.equalsIgnoreCase("Related Websites"))
//			    	{
//			    		if(UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("")&&
//					    		 UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase(""))
//					    	  {
//			    		GlobalVars.URL=AppConstants.URL_POPUP+"&c="+
//				    	 "Dummy"+"&e="+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
//				    	 +"&w=rw&args="+title_id+","+chapterName;
//					    	  }
//
//			    		else
//			    		{
//			    			GlobalVars.URL=AppConstants.URL_POPUP+"&c="+
//					    	  UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
//					    	 +"&e="+UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL)
//					    	 +"&w=rw&args="+title_id+","+chapterName;
//			    		}
//
//			    		if(UtilMethods.isConnectionAvailable(CurlActivity.this))
//			    		{
//			    		Intent it=new Intent(CurlActivity.this,WebPageActivity.class);
//					    it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					    it.putExtra("from", "reader");
//				    	it.putExtra("action_item", "Related Websites");
//					    startActivity(it);
//			    		}
//
//					    else
//						{
//						  UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
//						}
//			    	}

                else if (selected.equalsIgnoreCase("Trailers")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("") &&
                            UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")) {
                        GlobalVars.URL = AppConstants.URL_TRAILERS_POPUP + chapterName + ".mp4?" + "&c=" +
                                "Dummy" + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL);
                    } else {
                        GlobalVars.URL = AppConstants.URL_TRAILERS_POPUP + chapterName + ".mp4?" + "&c=" +
                                UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE)
                                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL);
                    }

                    if (UtilMethods.isConnectionAvailable(CurlActivity.this)) {
                        Intent it = new Intent(CurlActivity.this, WebPageActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        it.putExtra("from", "reader");
                        it.putExtra("action_item", "Trailers");
                        startActivity(it);
                    } else {
                        UtilMethods.createToast(CurlActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
                    }
                }

                d.dismiss();
            }
        });

        WindowManager.LayoutParams wmlp = d.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.LEFT;

        wmlp.x = width - 320;   //x position
        wmlp.y = (positiony - 200) - 50;   //y position
        d.getWindow().setAttributes(wmlp);
        d.show();
    }


    private void dataLogics() {
        //To check if the book is downloaded to sd-card
        SqliteDb db_conn = new SqliteDb(CurlActivity.this);
        Cursor records = db_conn.fetchData(QueryManager.QUERY_FETCH_IS_BOOK_DOWNLOADED,
                new String[]{QueryManager.PARAM_YES, QueryManager.PARAM_YES, chapterName});
        if (records.getCount() > 0) {
            records.moveToFirst();
            if (records.getInt(0) > 0)
                folderName = chapterName;
        }
        records.close();
        db_conn.closeDb();

        //To get the urls for thumbnails and reader images
        totalimages = Integer.parseInt(noOfPages) * 2;

        imageUrls = new ArrayList<String>();

        //If the status is PRV then show only 7 pages
        if (status.equalsIgnoreCase("PRV")) {
            noOfPages = "7";
            totalimages = 15;
        } else {
            buyButton.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnMore.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            btnMore.setLayoutParams(params);
        }


        //Creating urls for reader images based upon the current language context (Spanish or English)
        int j = 1, k = 0;
        while (j < totalimages) {
            if (folderName.equalsIgnoreCase("")) {
                if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                    if (status.equalsIgnoreCase("PRV")) {
                        imageUrls.add(String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        imageUrls.add(String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));
                    }
                } else {
                    if (status.equalsIgnoreCase("PRV")) {
                        imageUrls.add(String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + chapterName + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        imageUrls.add(String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + chapterName + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));
                    }
                }
            } else {
                if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                    imageUrls.add(String.valueOf(PathUtil.currentPath(0, folderName) + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));

                    File yourFile = new File(imageUrls.get(k).replace("%20", " ").replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                    if (yourFile.exists()) {
                        MediaPlayer mp = new MediaPlayer();
                        FileInputStream fs;
                        FileDescriptor fd;
                        try {
                            fs = new FileInputStream(yourFile);
                            fd = fs.getFD();
                            mp.setDataSource(fd);
                            mp.prepare(); // might be optional
                            mp.release();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                    }
                } else {
                    imageUrls.add(String.valueOf(PathUtil.currentPath(0, folderName) + "/" + chapterName + "%20p" + j + AppConstants.IMAGE_FORMAT_JPG));

                    File yourFile = new File(imageUrls.get(k).replace("%20", " ").replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));

                    if (yourFile.exists()) {
                        MediaPlayer mp = new MediaPlayer();
                        FileInputStream fs;
                        FileDescriptor fd;
                        try {
                            fs = new FileInputStream(yourFile);
                            fd = fs.getFD();
                            mp.setDataSource(fd);
                            mp.prepare(); // might be optional
                            mp.release();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                    }
                }
            }
            j = j + 2;
            k++;
        }

        //Create thumbnail urls based upon the current language context (Spanish or English)
        soundUrls = new ArrayList<String>();
        int i = 1;
        while (i < totalimages) {
            if (folderName.equalsIgnoreCase("")) {
                if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                    if (status.equalsIgnoreCase("PRV"))
                        soundUrls.add(String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));

                    else
                        soundUrls.add(String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));
                } else {
                    if (status.equalsIgnoreCase("PRV"))
                        soundUrls.add(String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + "M_" + chapterName + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));

                    else
                        soundUrls.add(String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + "M_" + chapterName + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));
                }
            } else {
                if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                    soundUrls.add(String.valueOf(PathUtil.currentPath(0, folderName) + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));
                } else {
                    soundUrls.add(String.valueOf(PathUtil.currentPath(0, folderName) + "/" + "M_" + chapterName + "%20p" + i + AppConstants.IMAGE_FORMAT_JPG));
                }
            }

            i = i + 2;
        }

        Log.i(AppConstants.TAG_CURL, "Urls prepared for download");
    }


    public class RefreshData extends AsyncTask<String, Integer, String> {

        private ProgressDialog progress;

        @Override
        protected String doInBackground(String... params) {

            int imgsize = Integer.parseInt(noOfPages);

            int j = 0, k = 1;
            while (j < imgsize) {
                if (folderName.equalsIgnoreCase("")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                        if (status.equalsIgnoreCase("PRV"))
                            imageUrls.set(j, String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + chapterName + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));

                        else
                            imageUrls.set(j, String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + chapterName + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        if (status.equalsIgnoreCase("PRV"))
                            imageUrls.set(j, String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));

                        else
                            imageUrls.set(j, String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));
                    }
                } else {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                        imageUrls.set(j, String.valueOf(PathUtil.currentPath(0, folderName) + "/" + chapterName + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        imageUrls.set(j, String.valueOf(PathUtil.currentPath(0, folderName) + "/" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + k + AppConstants.IMAGE_FORMAT_JPG));
                    }
                }
                j++;
                k = k + 2;
            }

            int i = 0, l = 1;
            while (i < imgsize) {
                if (folderName.equalsIgnoreCase("")) {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                        if (status.equalsIgnoreCase("PRV"))
                            soundUrls.set(i, String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + "M_" + chapterName + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));

                        else
                            soundUrls.set(i, String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + "M_" + chapterName + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        if (status.equalsIgnoreCase("PRV"))
                            soundUrls.set(i, String.valueOf(PathUtil.currentPath(2, folderName) + chapterName + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));

                        else
                            soundUrls.set(i, String.valueOf(PathUtil.currentPath(1, folderName) + chapterName + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));
                    }
                } else {
                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_LANGUAGE_IN_READER).equalsIgnoreCase(AppConstants.VALUE_SPANISH)) {
                        soundUrls.set(i, String.valueOf(PathUtil.currentPath(0, folderName) + "/" + "M_" + chapterName + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));
                    } else {
                        soundUrls.set(i, String.valueOf(PathUtil.currentPath(0, folderName) + "/" + "M_" + chapterName + AppConstants.PREFIX_SPANISH + "%20p" + l + AppConstants.IMAGE_FORMAT_JPG));
                    }
                }

                i++;
                l = l + 2;
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();

            languageSwitched = 1;


            if (streamFromServer != null && streamFromServer.getStatus() == AsyncTask.Status.RUNNING) {
                streamFromServer.cancel(true);
                System.out.println("refresh1 Longoperation task iscancelled " + streamFromServer.isCancelled());
            } else {
                System.out.println("refresh1 Longoperation task when language changed " + streamFromServer);
            }

            //mCurlView.setCurrentIndex(currentIndex);

            Log.i(AppConstants.TAG_CURL, "Index is going to be " + currentIndex);

            if (folderName.equalsIgnoreCase("") && aq.getCachedFile(imageUrls.get(currentIndex).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3)) == null)
                refresh(currentIndex);

            else if (folderName.equalsIgnoreCase("") && aq.getCachedFile(imageUrls.get(currentIndex).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3)) != null)
                mCurlView.setCurrentIndex(currentIndex);

            else
                mCurlView.setCurrentIndex(currentIndex);
        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(CurlActivity.this);
            progress.setTitle("Switching Language");
            progress.setMessage("Preparing ...");
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }
    }


    @Override
    public void onTaskComplete(String result) {

        if (result.trim().equalsIgnoreCase("?NO") || result.trim().equalsIgnoreCase("?YES")) {
            if (step == 0) {
                activityList.add("Teaching Activity");
                activityList.add("Quiz - Reading Comprehension");
                activityList.add("Quiz - For Creative Minds");
                activityList.add("Quiz - Math Word Problems");

//		 	if(result.trim().equalsIgnoreCase("?YES"))
//			{
//				activityList.add("Related Websites");
//			}

                params.clear();
                params.add(new BasicNameValuePair("fileurl", chapterName + ".mp4"));
                webCall = new WebServiceCallTask(CurlActivity.this, CurlActivity.this);
                webCall.execute(AppConstants.SERVICE_CHECK_VIDEO_FILE, params);
                step++;
            } else {
                if (result.trim().equalsIgnoreCase("?YES")) {
                    activityList.add("Trailers");
                }

                adapter_dialog = new MoreListAdapter(CurlActivity.this, android.R.layout.simple_list_item_1, activityList);
                createSpinnerDialog(CurlActivity.this, adapter_dialog, width, height);
            }
        }

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public void bookReadLog() {
        Log.i(AppConstants.TAG_CURL, "Data stored locally for reader log");

        SqliteDb db_conn = new SqliteDb(CurlActivity.this);

        if (!UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME,
                CurlActivity.this,
                AppConstants.KEY_SITECODE).equalsIgnoreCase("")) {
            String query = "INSERT INTO activity_log (title_id,short_title,sl_code) VALUES ('"
                    + title_id + "','" + chapterName + "','"
                    + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_SITECODE) + "')";
            db_conn.execSQL(query);
        } else {
            String query = "INSERT INTO activity_log (title_id,short_title,sl_code,password) VALUES ('"
                    + title_id + "','" + chapterName + "','"
                    + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_EMAIL) + "','"
                    + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, CurlActivity.this, AppConstants.KEY_PASSWORD) + "')";
            db_conn.execSQL(query);
        }

        db_conn.closeDb();

        Intent it = new Intent();
        it.setAction("com.arbordale.DATA_POST");
        it.putExtra("chaptername", chapterName);
        this.sendBroadcast(it);
    }


    private void soundPlayPause() {
        if (playSound == 1) {
            playSound = 0;

			/*if(btnRead!=null)
            btnRead.setText("Read to Myself");*/

            if (mCurlView.getCurrentIndex() == 0) {
                if (folderName.equalsIgnoreCase("")) {
                    f = aq.getCachedFile(imageUrls.get(mCurlView.getCurrentIndex()).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));
                } else {
                    f = new File(imageUrls.get(mCurlView.getCurrentIndex()).replace("%20", " ").replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));
                }
            }

            if (f == null) {
                UtilMethods.createToast(CurlActivity.this, "Please wait loading sound...");
                f = aq.getCachedFile(imageUrls.get(mCurlView.getCurrentIndex()).replace(AppConstants.IMAGE_FORMAT_JPG, AppConstants.SOUND_FORMAT_MP3));
            } else {
                FileInputStream inputStream;
                try {
                    inputStream = new FileInputStream(f);
                    mPlayer.stop();
                    mPlayer.reset();

                    if (playSound == 0)
                        prepareMediaPlayer(inputStream.getFD());

                    inputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            playSound = 1;

			/*if(btnRead!=null)
            btnRead.setText("Read to Me");*/
            mPlayer.pause();
        }

    }


    public static void hideShowBottomPanel() {
        if (!folderName.equalsIgnoreCase("")) {
            //refresh.setVisibility(View.GONE);
        }

        if (bottompanel.getVisibility() == View.VISIBLE) {
            mList.setVisibility(View.INVISIBLE);
            Animation fadeOut = new AlphaAnimation(1.00f, 0.00f);
            fadeOut.setDuration(500);
            fadeOut.setAnimationListener(new AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    bottompanel.setVisibility(View.GONE);
                    zoom.setVisibility(View.GONE);

                    if (!folderName.equalsIgnoreCase("")) {
                        //refresh.setVisibility(View.GONE);
                    }

                	 /*else
                      refresh.setVisibility(View.GONE);*/
                }
            });

            if (folderName.equalsIgnoreCase("")) {
                //refresh.startAnimation(fadeOut);
            }
            zoom.startAnimation(fadeOut);
            bottompanel.startAnimation(fadeOut);
        } else {
            Animation fadeIn = new AlphaAnimation(0.00f, 1.00f);
            fadeIn.setDuration(500);
            fadeIn.setAnimationListener(new AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    zoom.setVisibility(View.VISIBLE);
                    bottompanel.setVisibility(View.VISIBLE);

                    if (folderName.equalsIgnoreCase("")) {
                        //refresh.setVisibility(View.VISIBLE);
                    }
                }
            });

            zoom.startAnimation(fadeIn);
            bottompanel.startAnimation(fadeIn);

            if (folderName.equalsIgnoreCase("")) {
                //refresh.startAnimation(fadeIn);
            }
        }
    }


    private class ClearCache extends AsyncTask<String, String, String> {

        protected String doInBackground(String... path) {

            try {
                File f = new File(path[0]);
                if (f.exists()) {
                    FileUtils.cleanDirectory(f);
                    Log.i(AppConstants.TAG_CURL, "Directory Cleaned " + path[0]);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }


    public void lastFlip(int totalpages) {
        Log.i(AppConstants.TAG_CURL, "last page jump " + totalpages);

        if (totalpages == imageUrls.size() && lastPageSelected == 1) {
            mCurlView.setCurrentIndex(totalpages - 1);
            mCurlView.requestRender();
            lastPageSelected = 0;
        }

        if ((aq.getCachedImage(imageUrls.get(mCurlView.getCurrentIndex())) != null && jump == 1)) {
            mCurlView.setCurrentIndex(mCurlView.getCurrentIndex());
            mCurlView.requestRender();
        }
    }


    public void refreshCurrentPage() {
        mCurlView.setCurrentIndex(mCurlView.getCurrentIndex());
        mCurlView.requestRender();
    }
}
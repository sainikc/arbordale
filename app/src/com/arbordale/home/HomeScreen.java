package com.arbordale.home;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.arbordale.navfragments.LoginFragment;
import com.sherlock.navigationdrawer.compat.SherlockActionBarDrawerToggle;
import com.arbordale.beans.SetCategoryList;
import com.arbordale.database.SqliteDb;
import com.arbordale.navfragments.GridViewFragment;
import com.arbordale.navfragments.HomeFragment;
import com.arbordale.navfragments.MyAccountFragment;
import com.arbordale.navfragments.PurchaseDownload;
import com.arbordale.navfragments.RegistrationFragment;
import com.arbordale.navfragments.WebPageActivity;
import com.arbordale.navfragments.WebViewFragment;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.NaturalSortOrderComparator;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;

public class HomeScreen extends SherlockFragmentActivity implements OnItemClickListener {

    private String[] left_menu_array;
    private MenuListAdapter listAdapter;
    private ListView left_menu_listview;
    private DrawerLayout mDrawerLayout;
    private ActionBarHelper mActionBar;
    private LinearLayout left_drawer;
    private MenuItem menuItem = null;
    private Menu mainMenu;
    public Context context;

    private FragmentManager fragmentManager = getSupportFragmentManager();
    ArrayAdapter<String> adapter;
    DialogListAdapter adapter_dialog;
    ArrayList<String> orderList = new ArrayList<String>(10);
    ArrayList<SetCategoryList> orderListDialog = new ArrayList<SetCategoryList>(10);
    private SqliteDb db_conn;
    private static int ebook_fragment_selected = 0, account_screen_selected = 0;
    int actionBarHeight = 60, width, height, density;

    private SherlockActionBarDrawerToggle mDrawerToggle;
    private ArrayList<String> fragList = new ArrayList<String>();


    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = HomeScreen.this;
        setContentView(R.layout.menu_and_container);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        left_drawer = (LinearLayout) findViewById(R.id.left_drawer);

        mDrawerLayout.setDrawerListener(new MenuDrawerListener());
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        left_menu_listview = (ListView) findViewById(R.id.left_menu_listview);
        left_menu_array = getResources().getStringArray(R.array.left_menu_items);
        listAdapter = new MenuListAdapter(this, Arrays.asList(left_menu_array));

        mActionBar = createActionBarHelper();
        mActionBar.init();

        left_menu_listview.setOnItemClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        width = display.getWidth();
        height = display.getHeight();

        TypedValue tv = new TypedValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        } else if (getTheme().resolveAttribute(com.actionbarsherlock.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        mDrawerToggle = new SherlockActionBarDrawerToggle(HomeScreen.this, mDrawerLayout, R.drawable.ic_drawer_light, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.syncState();

        if (GlobalVars.HOME_READER_PRESSED.equals("true")) {
            attachGridFragment();
        } else if (GlobalVars.BACK_FROM_PURCHASE_DOWNLOAD.equals("true")) {
            attachPurchaseDownload();
        } else {
            attachHomeFragment();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //createFragmentList();

        orderList.clear();
        orderList.add(AppConstants.DEFAULT_ALPHABETIC);
        queryDropdown(QueryManager.QUERY_DROPDOWN);

        left_menu_listview.setAdapter(listAdapter);

        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase("")) {
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EBOOKS1, AppConstants.VALUE_EBOOK_1_1);
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EBOOKS2, AppConstants.VALUE_EBOOK_2_1);
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_LANGUAGE, AppConstants.VALUE_ENGLISH);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void loginDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(HomeScreen.this).create(); //Read Update
        alertDialog.setTitle("Please Select");
        alertDialog.setMessage("Is this Login to an already existing account? (either your own personal account or your school or public library's account)");
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                replaceWithLoginFragment();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //attachHomeFragment();
                replaceWithRegistrationFragment();
            }
        });
        alertDialog.show();
    }

    public void logoutDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(HomeScreen.this).create(); //Read Update
        alertDialog.setTitle("Please Select");
        alertDialog.setMessage("Are you sure that you want to Logout?");
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                ebook_fragment_selected = 0;
                account_screen_selected = 0;

                supportInvalidateOptionsMenu();

                SqliteDb db_conn = new SqliteDb(HomeScreen.this);

                Cursor users = db_conn.fetchData(QueryManager.QUERY_FETCH_USER_INFO, null);

                if (users.getCount() == 1) {
                    users.moveToFirst();

                    ContentValues cv = new ContentValues();
                    cv.put("username", "Default");
                    cv.put("password", "");
                    cv.put("Email", "");
                    cv.put("last_name", "Welcome");
                    cv.put("first_name", "Guest");
                    cv.put("City", "");
                    cv.put("State", "");
                    cv.put("Require_login", "");
                    cv.put("Display_data_usage", "");
                    cv.put("Language", "");
                    cv.put("Expiry_date", "");
                    cv.put("site_code", "");
                    cv.put("Organization_name", "");

                    db_conn.updateData("users", cv, "username='" + users.getString(0) + "'");
                }
                users.close();

                db_conn.execSQL("update book_icon set Status='PRV'");

                db_conn.closeDb();

                UtilMethods.clearSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this);

                attachHomeFragment();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void moveToRegistration() {
        final Dialog d = new Dialog(HomeScreen.this, R.style.FullHeightDialog);
        d.setContentView(R.layout.registration_dialog);
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        Button register = (Button) d.findViewById(R.id.yes);
        Button login = (Button) d.findViewById(R.id.no);

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                d.dismiss();
                replaceFragment(LoginFragment.newInstance(0), AppConstants.FRAG_LOGIN);
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_DIALOG_SEEN, "true");
            }
        });

        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                d.dismiss();
                replaceFragment(RegistrationFragment.newInstance(0), AppConstants.FRAG_REGISTRATION);
                UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_DIALOG_SEEN, "true");
            }
        });

        d.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        d.show();
    }


    //To do actions when drawer is opened or closed
    private class MenuDrawerListener implements DrawerLayout.DrawerListener {
        @Override
        public void onDrawerOpened(View drawerView) {
            mDrawerToggle.onDrawerOpened(drawerView);
            mActionBar.onDrawerOpened();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            mDrawerToggle.onDrawerClosed(drawerView);
            mActionBar.onDrawerClosed();
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            mDrawerToggle.onDrawerStateChanged(newState);
        }
    }


    /**
     * Create a compatible helper that will manipulate the action bar if
     * available.
     */
    private ActionBarHelper createActionBarHelper() {
        return new ActionBarHelper();
    }

    private class ActionBarHelper {
        private final ActionBar mActionBar;
        private CharSequence mDrawerTitle;
        private CharSequence mTitle;

        private ActionBarHelper() {
            mActionBar = getSupportActionBar();
        }

        public void init() {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            mActionBar.setIcon(R.drawable.splash_drawable);
            mTitle = mDrawerTitle = getTitle();
        }

        public void onDrawerClosed() {
            mActionBar.setTitle(mTitle);
        }

        public void onDrawerOpened() {
            mActionBar.setTitle(mDrawerTitle);

            IBinder windowToken = getCurrentFocus().getWindowToken();
            UtilMethods.hidekeyboard(HomeScreen.this, windowToken);
        }

        public void setTitle(CharSequence title) {
            mTitle = title;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        if (arg2 == 0) {
            System.out.println("eBook selected");
            mDrawerLayout.closeDrawer(left_drawer);
            mActionBar.setTitle(getString(R.string.ebooks));

            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.selected_fragment, GridViewFragment.newInstance(0), AppConstants.FRAG_GRIDVIEW);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            getSupportFragmentManager().executePendingTransactions();
            ebook_fragment_selected = 1;
            account_screen_selected = 0;
            supportInvalidateOptionsMenu();
        }

        if (arg2 == 1) {
            if (UtilMethods.isConnectionAvailable(HomeScreen.this)) {
                mDrawerLayout.closeDrawer(left_drawer);
                mActionBar.setTitle(getString(R.string.book_homepages));

                GlobalVars.URL = AppConstants.URL_HOME_PAGE
                        + AppConstants.KEY_IPAD
                        + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL)
                        + "&p=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_PASSWORD)
                        + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE);

                replaceFragment(WebViewFragment.newInstance(0), AppConstants.FRAG_WEBVIEW);

                getSupportFragmentManager().executePendingTransactions();
                ebook_fragment_selected = 0;
                account_screen_selected = 0;
                supportInvalidateOptionsMenu();
            } else {
                UtilMethods.createToast(HomeScreen.this, AppMessaging.NO_INTERNET_CONNECTION);
            }
        }

        if (arg2 == 2) {
            if (UtilMethods.isConnectionAvailable(HomeScreen.this)) {
                mDrawerLayout.closeDrawer(left_drawer);
                mActionBar.setTitle(getString(R.string.authors));

                GlobalVars.URL = AppConstants.URL_AUTHORS
                        + AppConstants.KEY_IPAD
                        + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL)
                        + "&p=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_PASSWORD)
                        + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE);

                replaceFragment(WebViewFragment.newInstance(0), AppConstants.FRAG_WEBVIEW);

                getSupportFragmentManager().executePendingTransactions();
                ebook_fragment_selected = 0;
                account_screen_selected = 0;
                supportInvalidateOptionsMenu();
            } else {
                UtilMethods.createToast(HomeScreen.this, AppMessaging.NO_INTERNET_CONNECTION);
            }
        }

        if (arg2 == 3) {
            if (UtilMethods.isConnectionAvailable(HomeScreen.this)) {
                mDrawerLayout.closeDrawer(left_drawer);
                mActionBar.setTitle(getString(R.string.standards));

                GlobalVars.URL = AppConstants.URL_STANDARDS
                        + AppConstants.KEY_IPAD
                        + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL)
                        + "&p=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_PASSWORD)
                        + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE);

                replaceFragment(WebViewFragment.newInstance(0), AppConstants.FRAG_WEBVIEW);

                getSupportFragmentManager().executePendingTransactions();
                ebook_fragment_selected = 0;
                account_screen_selected = 0;
                supportInvalidateOptionsMenu();
            } else {
                UtilMethods.createToast(HomeScreen.this, AppMessaging.NO_INTERNET_CONNECTION);
            }
        }

        if (arg2 == 4) {
            if (UtilMethods.isConnectionAvailable(HomeScreen.this)) {
                mDrawerLayout.closeDrawer(left_drawer);
                mActionBar.setTitle(getString(R.string.reading_levels));

                GlobalVars.URL = AppConstants.URL_LEXILE_CODES
                        + AppConstants.KEY_IPAD
                        + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL)
                        + "&p=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_PASSWORD)
                        + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE);

                replaceFragment(WebViewFragment.newInstance(0), AppConstants.FRAG_WEBVIEW);

                getSupportFragmentManager().executePendingTransactions();
                ebook_fragment_selected = 0;
                account_screen_selected = 0;
                supportInvalidateOptionsMenu();
            } else {
                UtilMethods.createToast(HomeScreen.this, AppMessaging.NO_INTERNET_CONNECTION);
            }
        }

        if (arg2 == 5) {
            attachPurchaseDownload();
        }

        if (arg2 == 6) {
            mDrawerLayout.closeDrawer(left_drawer);
            mActionBar.setTitle(getString(R.string.home_imgs));

            attachHomeFragment();
            ebook_fragment_selected = 0;
            account_screen_selected = 0;
            supportInvalidateOptionsMenu();
        }

    }

    private void replaceFragment(Fragment f, String tag) {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.selected_fragment, f, tag);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.actionbar_gridview, menu);
        this.mainMenu = menu;

        if (ebook_fragment_selected == 1) {

            SearchView searchView = (SearchView) menu.findItem(R.id.menuSearch).getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String newText) {

                    IBinder windowToken = getCurrentFocus().getWindowToken();
                    UtilMethods.hidekeyboard(HomeScreen.this, windowToken);

                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS).equalsIgnoreCase("")) {
                        GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);
                        fragment.handleGrid(newText, "", "search");
                    } else {
                        GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);
                        fragment.handleGrid(newText, UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS), "search");
                    }

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS).equalsIgnoreCase("")) {
                        GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);
                        fragment.handleGrid(newText, "", "search");
                    } else {
                        GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);
                        fragment.handleGrid(newText, UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS), "search");
                    }

                    return true;
                }

            });


            menuItem = menu.findItem(R.id.order_menu);
            View view1 = menuItem.getActionView();

            if (view1 instanceof Button) {
                final Button spinner = (Button) view1;
                //spinner.setAdapter(adapter);

                spinner.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        createSpinnerDialogOuter(HomeScreen.this, adapter, width, actionBarHeight);

                    }
                });

            }
        }


        if (account_screen_selected == 1) {
            menuItem = menu.findItem(R.id.logout_login);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(left_drawer))
                    mDrawerLayout.closeDrawer(left_drawer);

                else
                    mDrawerLayout.openDrawer(left_drawer);
                break;

            case R.id.logout_login: {
                String title = menuItem.getTitle().toString();

                if (title.equalsIgnoreCase("Logout    ")) {
                    logoutDialog();
                } else {
                    loginDialog();
                }
                break;
            }

            case R.id.order_menu: {
                System.out.println("order menu clicked");
                break;
            }
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem search = menu.findItem(R.id.menuSearch);
        MenuItem sort = menu.findItem(R.id.order_menu);
        MenuItem logout_login = menu.findItem(R.id.logout_login);
        System.out.println("selected ebook fragment show search and sort " + ebook_fragment_selected + "/" + account_screen_selected);

        if (ebook_fragment_selected == 1) {
           /*View view1 = sort.getActionView();
		     
		       if(view1 instanceof IcsSpinner)
		        {
		    	 final IcsSpinner spinner = (IcsSpinner) view1;  
			     int position=adapter.getPosition(UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS));
                 spinner.setSelection(position);
                 
                 System.out.println("spinner set "+position);
		    }*/
            getSupportActionBar().setTitle("eBooks");
            mActionBar.setTitle("eBooks");
            search.setVisible(true);
            sort.setVisible(true);
        } else {
            search.setVisible(false);
            sort.setVisible(false);
        }

        if (account_screen_selected == 1) {
            getSupportActionBar().setTitle(getString(R.string.my_account));

            logout_login.setVisible(true);

            if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")
                    && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("")) {
                logout_login.setTitle("Login    ");
            } else {
                logout_login.setTitle("Logout    ");
            }

        } else {
            logout_login.setVisible(false);
        }

        return true;
    }

    public void onGroupItemClick(MenuItem item) {
        System.out.println("selected item is " + item.getAlphabeticShortcut());
    }

    public void queryDropdown(String query) {
        System.out.println("Query DropDown : " + query);
        db_conn = new SqliteDb(HomeScreen.this);

        Cursor initial_dropdown = db_conn.fetchData(query, null);

        if (initial_dropdown != null) {
            if (initial_dropdown.getCount() > 0) {
                initial_dropdown.moveToFirst();
                while (!initial_dropdown.isAfterLast()) {
                    orderList.add(initial_dropdown.getString(0));
                    initial_dropdown.moveToNext();
                }
            }
            initial_dropdown.close();
        }
        db_conn.closeDb();

        if (orderList.size() > 0)
            adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, orderList);
    }

    public void queryDropdownDialog(String query, String params[]) {
        //dsfsdfsdf
        System.out.println("Query DropDownDialog : " + query + " params : " + params);
        for (int i = 0; i < params.length; i++) {
            System.out.println("params[" + i + "] : " + params[i]);
        }
        SetCategoryList obj = null;
        orderListDialog.clear();
        adapter_dialog = null;

        db_conn = new SqliteDb(this);

        Cursor dropdown = db_conn.fetchData(query, params);

        if (dropdown.getCount() > 0) {
            dropdown.moveToFirst();
            while (!dropdown.isAfterLast()) {
                obj = new SetCategoryList();

                obj.setTitle_id(dropdown.getString(0));
                obj.setTitle(dropdown.getString(2));

                orderListDialog.add(obj);

                dropdown.moveToNext();
            }
        }
        dropdown.close();
        db_conn.closeDb();

        Collections.sort(orderListDialog, new NaturalSortOrderComparator());

        adapter_dialog = new DialogListAdapter(this, android.R.layout.simple_list_item_1, orderListDialog);
    }


    private void createSpinnerDialogOuter(final Context ctx, ArrayAdapter<String> adapter, final int width, final int positiony) {
        final Dialog d = new Dialog(ctx, R.style.FullHeightDialog);
        d.setContentView(R.layout.spinner_dialog);

        ListView ls = (ListView) d.findViewById(R.id.list);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                if (position != 0) {
                    d.dismiss();
                    // Toast.makeText(ctx,"position:"+position+" clicked",Toast.LENGTH_LONG).show();
                    String params[] = {(String) parent.getItemAtPosition(position)};

                    GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);

                    if (fragment == null)
                        attachGridFragment();

                    fragment.handleGrid("", (String) parent.getItemAtPosition(position), "sort1");

                    UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS, (String) parent.getItemAtPosition(position));
                    for (int i = 0; i < params.length; i++)
                        System.out.println("Query DropDownFurther params[" + i + "] : " + params[i]);
                    //Ram
                    queryDropdownDialog(QueryManager.QUERY_DROPDOWN_FURTHER, params);
                    createSpinnerDialog(HomeScreen.this, adapter_dialog, width, actionBarHeight);
                } else {
                    d.dismiss();
                    UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SORT_EBOOKS, "");

                    GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);

                    if (fragment == null)
                        attachGridFragment();

                    fragment.handleGrid("", "", "search");
                }
            }
        });


        WindowManager.LayoutParams wmlp = d.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = width - 320;
        wmlp.y = positiony;   //y position

        //System.out.println("x and y position "+this.width+":"+wmlp.y);
        d.getWindow().setAttributes(wmlp);
        d.setCanceledOnTouchOutside(true);

        d.show();
    }

    private void createSpinnerDialog(final Context ctx, DialogListAdapter adapter, final int width, final int positiony) {
        final Dialog d = new Dialog(ctx, R.style.FullHeightDialog);
        d.setContentView(R.layout.spinner_dialog);

        ListView ls = (ListView) d.findViewById(R.id.list);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                d.dismiss();
                //	Toast.makeText(ctx,"position further:"+position+" clicked",Toast.LENGTH_LONG).show();
                SetCategoryList object = (SetCategoryList) arg0.getItemAtPosition(position);

                GridViewFragment fragment = (GridViewFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_GRIDVIEW);
                fragment.handleGrid(object.getTitle_id(), object.getTitle(), "sort2");
            }
        });


        WindowManager.LayoutParams wmlp = d.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = width - 320;
        wmlp.y = positiony;   //y position

        //	System.out.println("x and y position "+this.width+":"+wmlp.y);
        d.getWindow().setAttributes(wmlp);
        d.setCanceledOnTouchOutside(true);

        d.show();
    }


    public void attachHomeFragment() {
        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_LANGUAGE).equalsIgnoreCase("")) {
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EBOOKS1, AppConstants.VALUE_EBOOK_1_1);
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EBOOKS2, AppConstants.VALUE_EBOOK_2_1);
            UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_LANGUAGE, AppConstants.VALUE_ENGLISH);
        }

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.selected_fragment, HomeFragment.newInstance(0), AppConstants.FRAG_HOME);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();

        getSupportActionBar().setTitle(getString(R.string.home_imgs));

        if (mainMenu != null) {
            MenuItem menuItem = mainMenu.findItem(R.id.logout_login);
            menuItem.setVisible(false);
        }

        ebook_fragment_selected = 0;
        account_screen_selected = 0;
    }


    public void attachGridFragment() {
        mActionBar.setTitle(getString(R.string.ebooks));

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.selected_fragment, GridViewFragment.newInstance(0), AppConstants.FRAG_GRIDVIEW);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();

        ebook_fragment_selected = 1;
        account_screen_selected = 0;
        supportInvalidateOptionsMenu();
    }


    public void attachPurchaseDownload() {

        GlobalVars.BACK_FROM_PURCHASE_DOWNLOAD = "false";

        mDrawerLayout.closeDrawer(left_drawer);
        mActionBar.setTitle(getString(R.string.my_account));

        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")
                && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("")
                && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_DIALOG_SEEN).equalsIgnoreCase("")) {
            moveToRegistration();
        } else {
            account_screen_selected = 1;
            replaceFragment(MyAccountFragment.newInstance(0), AppConstants.FRAG_MY_ACCOUNT);
        }

        getSupportFragmentManager().executePendingTransactions();
        ebook_fragment_selected = 0;
        supportInvalidateOptionsMenu();

        if (UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL).equalsIgnoreCase("")
                && UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE).equalsIgnoreCase("")) {
            if (mainMenu != null) {
                MenuItem menuItem = mainMenu.findItem(R.id.logout_login);
                menuItem.setTitle("Login    ");
            }
        } else {
            if (mainMenu != null) {
                MenuItem menuItem = mainMenu.findItem(R.id.logout_login);
                menuItem.setTitle("Logout    ");
            }
        }
    }


    public void webViewFragment() {
        GlobalVars.URL = AppConstants.URL_LEXILE_CODES
                + AppConstants.KEY_IPAD
                + "&e=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_EMAIL)
                + "&p=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_PASSWORD)
                + "&c=" + UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, HomeScreen.this, AppConstants.KEY_SITECODE);

        replaceFragment(WebViewFragment.newInstance(0), AppConstants.FRAG_WEBVIEW);
        getSupportFragmentManager().executePendingTransactions();

        ebook_fragment_selected = 0;
        account_screen_selected = 0;
        supportInvalidateOptionsMenu();
    }

    public void replaceWithRegistrationFragment() {
        replaceFragment(RegistrationFragment.newInstance(0), AppConstants.FRAG_REGISTRATION);
        getSupportFragmentManager().executePendingTransactions();
        account_screen_selected = 0;
        supportInvalidateOptionsMenu();
    }


    public void replaceWithLoginFragment() {
        replaceFragment(LoginFragment.newInstance(0), AppConstants.FRAG_LOGIN);
        getSupportFragmentManager().executePendingTransactions();
        account_screen_selected = 0;
        supportInvalidateOptionsMenu();
    }

    public void purchaseOrDownloadBooksFragment() {
        Intent it = new Intent(HomeScreen.this, PurchaseDownload.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);
    }

    public void openWebActivity(String page) {
        Intent it = new Intent(HomeScreen.this, WebPageActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        it.putExtra("from", "myaccount");
        it.putExtra("action_item", page);
        startActivity(it);
    }

    public void createFragmentList() {
        fragList.clear();
        fragList.add(AppConstants.FRAG_GRIDVIEW);
        fragList.add(AppConstants.FRAG_HOME);
        fragList.add(AppConstants.FRAG_LOGIN);
        fragList.add(AppConstants.FRAG_MY_ACCOUNT);
        fragList.add(AppConstants.FRAG_REGISTRATION);
        fragList.add(AppConstants.FRAG_WEBVIEW);
    }

    public void clearFullscreenMode() {
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    }

    public void setFullscreenMode() {
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (db_conn != null) {
            db_conn.closeDb();
        }
    }


}

package com.arbordale.home;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MenuListAdapter extends BaseAdapter{

	static class ViewHolder {
	    public TextView menu_text;
	  }
	
	List<String> menuList;
	Activity ctx;

	public MenuListAdapter(Activity ctx,List<String> menuList) {
		this.menuList=menuList;
		this.ctx=ctx;
	}
	
	
	@Override
	public int getCount() {
		return menuList.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		View rowView = convertView;
		
		System.out.println("position "+position);
		
		if(rowView==null)
		{
			LayoutInflater inflater=ctx.getLayoutInflater();
			rowView=inflater.inflate(R.layout.left_menu_layout, null);
			ViewHolder viewHolder=new ViewHolder();
			viewHolder.menu_text=(TextView) rowView.findViewById(R.id.option_text);
			rowView.setTag(viewHolder);
		}
		
		ViewHolder holder=(ViewHolder) rowView.getTag();
		holder.menu_text.setText(menuList.get(position));
 
		return rowView;
	}
	
}
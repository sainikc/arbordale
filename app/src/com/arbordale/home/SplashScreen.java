package com.arbordale.home;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.arbordale.database.AssetsDbHelper;
import com.arbordale.database.SqliteDb;
import com.arbordale.util.AppConstants;
import com.arbordale.util.UtilMethods;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class SplashScreen extends SherlockActivity{

	private static final int RC_STORAGE_PERM = 2;
	
	private ImageView splashimg;
	private TextView splashimg_overlay;
	private List<String> imagesForSplash;
	private String randomImgPath;
	private ActionBar actionBar;
  
	int widthOfScreen,heightOfScreen;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		actionBar = getSupportActionBar();
		actionBar.hide();
		
		widthOfScreen = getResources().getDisplayMetrics().widthPixels;
		heightOfScreen= getResources().getDisplayMetrics().heightPixels;
		
		splashimg=(ImageView) findViewById(R.id.splashscreen);
		splashimg_overlay=(TextView) findViewById(R.id.img_overlay);
		String fontPath = "fonts/sd_font.ttf";
		 
        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);		
        splashimg_overlay.setTypeface(tf);
		
		try {
			imagesForSplash=UtilMethods.getImage(this, AppConstants.ASSET_HOME);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		randomImgPath=UtilMethods.randomImage(imagesForSplash);
		
		splashimg.setImageBitmap(Bitmap.createScaledBitmap(UtilMethods.getBitmapFromAssets(AppConstants.ASSET_HOME+"/"+randomImgPath, this), widthOfScreen, heightOfScreen, true));
		
		//UtilMethods.recycleImagesFromView(splashimg);
		
		/* New Handler to start the HomeScreen 
           and close this Splash-Screen after some seconds.*/
		
		if(UtilMethods.getDataFromSharedPrefernces(
                AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, 
                SplashScreen.this, 
                AppConstants.KEY_DB_COPIED).equals(AppConstants.VALUE_DB_COPIED))
        {


			if((System.currentTimeMillis()-UtilMethods.getLongDataFromSharedPrefernces(
                    AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, 
                    SplashScreen.this, 
                    AppConstants.WEB_CALL_EXECUTION_TIME)/(24*60*60000L))>0)
			{


				if(UtilMethods.isConnectionAvailable(SplashScreen.this))
            	{
            		new DownloadJsonTask().execute("http://www.arbordalepublishing.com/validatelibaccess.php?all=1");
            	}

			}	
			
			else
			{

			Intent mainIntent = new Intent(SplashScreen.this,HomeScreen.class);
            SplashScreen.this.startActivity(mainIntent);
            SplashScreen.this.finish();
			}
        }
		
		
		else
		{
			new Handler().postDelayed(new Runnable(){
	            @Override
	            public void run() {
					startDbCopyTask();
	            }
	        }, AppConstants.SPLASH_DISPLAY_LENGHT);
		}	
		
    }

    private void startDbCopyTask() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                new DbCopyTask().execute("");
            } else {
                requestPermissions(new String[] {
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, RC_STORAGE_PERM);
            }
        } else {
            new DbCopyTask().execute("");
        }
    }

	@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
            int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != RC_STORAGE_PERM) {
            return;
        }
        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new DbCopyTask().execute("");
            return;
        }
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name)
                .setMessage(R.string.no_read_write_permission)
                .setPositiveButton("OK", listener)
                .show();
    }
	
	
	public class DbCopyTask extends AsyncTask<String , Integer , String>{

		String dbCopied="0";
		
		@Override
		protected String doInBackground(String... params) {
			
			if(UtilMethods.getDataFromSharedPrefernces(
					                            AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, 
							                    SplashScreen.this, 
							                    AppConstants.KEY_DB_COPIED)
					      .equals(AppConstants.VALUE_DB_COPIED))
			{
			 dbCopied="1";	
			 return dbCopied;	
			}
			
			else
			{	
			
			AssetsDbHelper db=new AssetsDbHelper(SplashScreen.this);
			db.close();
			
			new SqliteDb(SplashScreen.this);
			
			UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, 
                    SplashScreen.this, 
                    AppConstants.KEY_DB_COPIED, 
                    AppConstants.VALUE_DB_COPIED);
			
			dbCopied="1";
			
			return dbCopied;
			}
			
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result){
			
			if(result.equals("1"))
			{	
				if(UtilMethods.isConnectionAvailable(SplashScreen.this))
            	{
            		new DownloadJsonTask().execute("http://www.arbordalepublishing.com/validatelibaccess.php?all=1");
            	}
				
				else
				{
					Toast.makeText(SplashScreen.this, "No internet Connection!", Toast.LENGTH_SHORT).show();
				}	
			} 
		}

		@Override
		protected void onPreExecute(){}

	}
	
	public class DownloadJsonTask extends AsyncTask<String , Integer , String>{

		String data="";
		
		@Override
		protected String doInBackground(String... urls) {
			
			 try
			 {
				FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory()+"/Arbordale", "logdata.json"));;

				// Defined URL  where to send data
                URL url = new URL(urls[0]);
                URLConnection conn = url.openConnection(); 
            
               // Get the server response 
               InputStream is = conn.getInputStream();
               
               int read = 0;
               byte[] buffer = new byte[32768];
               while( (read = is.read(buffer)) > 0) {
                 fos.write(buffer, 0, read);
               }

               fos.close();
               is.close();
               
               UtilMethods.updateAddLongSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, SplashScreen.this, 
            		   AppConstants.WEB_CALL_EXECUTION_TIME, System.currentTimeMillis());
               
               return "success";
			  } 
             catch(Exception ex)
             {
              return "failure";
             }
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result){
			
			if(result.equals("success"))
			{	
				/* Create an Intent that will start the Menu-Activity. */
	            Intent mainIntent = new Intent(SplashScreen.this,HomeScreen.class);
	            SplashScreen.this.startActivity(mainIntent);
	            SplashScreen.this.finish();
			} 
		}

		@Override
		protected void onPreExecute(){}

	}
}

package com.arbordale.home;

import java.util.ArrayList;

import com.arbordale.beans.SetCategoryList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DialogListAdapter extends ArrayAdapter<SetCategoryList>{

	private ArrayList<SetCategoryList> list;
	
	public DialogListAdapter(Context context,int resource,ArrayList<SetCategoryList> list) {
		super(context, resource,list);
		this.list=list;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dialog_list_options, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView)convertView.findViewById(R.id.top_grid_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
		
		holder.textView.setText(list.get(position).getTitle());
		
		return convertView;
	}
	
	@Override
	public SetCategoryList getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}
	
	protected class ViewHolder {
        public TextView textView;
    }

}

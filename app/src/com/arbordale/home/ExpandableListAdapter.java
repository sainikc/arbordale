package com.arbordale.home;

import java.util.HashMap;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	
	public static int selectedIndex,selectedChildIndex;
	private Context _context;
	private HashMap<Integer, String> _listDataHeader; // header titles
	// child data in format of header title, child title

	public ExpandableListAdapter(Context context, HashMap<Integer, String> listDataHeader) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		selectedIndex = -1;
		selectedChildIndex=-1;
	}

	@Override
	public String getChild(int groupPosition, int childPosititon) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		
		final String childText = getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.header_layout_grid, null);
		}
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataHeader.get(groupPosition).length();
	}

	@Override
	public String getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.header_layout_grid, null);
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	   public void setSelectedIndex(int ind,int childindex)
	    {
	        selectedIndex = ind;
	        selectedChildIndex=childindex;
	        notifyDataSetChanged();
	    }
}

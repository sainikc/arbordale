package com.arbordale.home;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import com.arbordale.util.GlobalVars;
import com.arbordale.util.UtilMethods;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class WebServiceCallTask extends AsyncTask<Object , Integer , String>{

	private AsyncTaskCompleteListener<String> callback;
	private ProgressDialog progress;
	Context context;
	public WebServiceCallTask(Context context, AsyncTaskCompleteListener<String> cb) {
        this.context = context;
        this.callback = cb;
    }
	
	@Override
	protected String doInBackground(Object... params) {
		
		String url=(String)params[0];
	//	System.out.println("Url of page loading : "+url);
		@SuppressWarnings("unchecked")
		ArrayList<NameValuePair> data=(ArrayList<NameValuePair>)params[1];
		
		String result=UtilMethods.getData(url,data);
		//System.out.println("Data : "+data);
		
		return result;
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(String result){
		callback.onTaskComplete(result);
		progress.dismiss();
	}

	@Override
	protected void onPreExecute(){
		progress=new ProgressDialog(context);
		progress.setCancelable(false);
		progress.setTitle(GlobalVars.WEB_DIALOG_TITLE);
		progress.setMessage(GlobalVars.WEB_DIALOG_MSG);
		progress.setCanceledOnTouchOutside(false);
		progress.show();
	}

}

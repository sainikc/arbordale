package com.arbordale.home;

public interface AsyncTaskCompleteListener<T> {
	   public void onTaskComplete(T result);
	}

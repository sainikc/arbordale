package com.arbordale.jsonparse;

public class SchoolData {

	private String schoolName;
	private String schoolPassword;
	private String siteLicenseCode;
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolPassword() {
		return schoolPassword;
	}
	public void setSchoolPassword(String schoolPassword) {
		this.schoolPassword = schoolPassword;
	}
	public String getSiteLicenseCode() {
		return siteLicenseCode;
	}
	public void setSiteLicenseCode(String siteLicenseCode) {
		this.siteLicenseCode = siteLicenseCode;
	}
	
	
}

package com.arbordale.jsonparse;

import java.util.ArrayList;

public class SateData {

	private String stateCode;
	private String stateName;
	private ArrayList<SchoolData> schools;
	private ArrayList<Library> libraries;

	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public ArrayList<SchoolData> getSchools() {
		return schools;
	}
	public void setSchools(ArrayList<SchoolData> schools) {
		this.schools = schools;
	}
	public ArrayList<Library> getLibraries() {
		return libraries;
	}
	public void setLibraries(ArrayList<Library> libraries) {
		this.libraries = libraries;
	}
	
	
	
}

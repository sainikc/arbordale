package com.arbordale.jsonparse;

public class Library {

	private String libraryName;
	private String librarySiteLicenseCode;
	private String libraryDigits2Expect;
	private String libraryDigits2Match;

	private String libraryMatch1;
	private String libraryMatch2;
	private String libraryMatch3;
	private String libraryMatch4;
	private String libraryMatch5;
	private String libraryMatch6;

	private String libraryMatch7;
	private String libraryMatch8;

	private String libraryMatch9;
	private String libraryMatch10;

	private String libraryMatch11;

	private String libraryMatch12;
	private String libraryMatch13;

	private String libraryMatch14;
	private String libraryMatch15;

	private String libraryMatch16;

	private String libraryMatch17;
	private String libraryMatch18;

	private String libraryMatch19;
	private String libraryMatch20;
	public String getLibraryName() {
		return libraryName;
	}
	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}
	public String getLibrarySiteLicenseCode() {
		return librarySiteLicenseCode;
	}
	public void setLibrarySiteLicenseCode(String librarySiteLicenseCode) {
		this.librarySiteLicenseCode = librarySiteLicenseCode;
	}
	public String getLibraryDigits2Expect() {
		return libraryDigits2Expect;
	}
	public void setLibraryDigits2Expect(String libraryDigits2Expect) {
		this.libraryDigits2Expect = libraryDigits2Expect;
	}
	public String getLibraryDigits2Match() {
		return libraryDigits2Match;
	}
	public void setLibraryDigits2Match(String libraryDigits2Match) {
		this.libraryDigits2Match = libraryDigits2Match;
	}
	public String getLibraryMatch1() {
		return libraryMatch1;
	}
	public void setLibraryMatch1(String libraryMatch1) {
		this.libraryMatch1 = libraryMatch1;
	}
	public String getLibraryMatch2() {
		return libraryMatch2;
	}
	public void setLibraryMatch2(String libraryMatch2) {
		this.libraryMatch2 = libraryMatch2;
	}
	public String getLibraryMatch3() {
		return libraryMatch3;
	}
	public void setLibraryMatch3(String libraryMatch3) {
		this.libraryMatch3 = libraryMatch3;
	}
	public String getLibraryMatch4() {
		return libraryMatch4;
	}
	public void setLibraryMatch4(String libraryMatch4) {
		this.libraryMatch4 = libraryMatch4;
	}
	public String getLibraryMatch5() {
		return libraryMatch5;
	}
	public void setLibraryMatch5(String libraryMatch5) {
		this.libraryMatch5 = libraryMatch5;
	}
	public String getLibraryMatch6() {
		return libraryMatch6;
	}
	public void setLibraryMatch6(String libraryMatch6) {
		this.libraryMatch6 = libraryMatch6;
	}
	public String getLibraryMatch7() {
		return libraryMatch7;
	}
	public void setLibraryMatch7(String libraryMatch7) {
		this.libraryMatch7 = libraryMatch7;
	}
	public String getLibraryMatch8() {
		return libraryMatch8;
	}
	public void setLibraryMatch8(String libraryMatch8) {
		this.libraryMatch8 = libraryMatch8;
	}
	public String getLibraryMatch9() {
		return libraryMatch9;
	}
	public void setLibraryMatch9(String libraryMatch9) {
		this.libraryMatch9 = libraryMatch9;
	}
	public String getLibraryMatch10() {
		return libraryMatch10;
	}
	public void setLibraryMatch10(String libraryMatch10) {
		this.libraryMatch10 = libraryMatch10;
	}
	public String getLibraryMatch11() {
		return libraryMatch11;
	}
	public void setLibraryMatch11(String libraryMatch11) {
		this.libraryMatch11 = libraryMatch11;
	}
	public String getLibraryMatch12() {
		return libraryMatch12;
	}
	public void setLibraryMatch12(String libraryMatch12) {
		this.libraryMatch12 = libraryMatch12;
	}
	public String getLibraryMatch13() {
		return libraryMatch13;
	}
	public void setLibraryMatch13(String libraryMatch13) {
		this.libraryMatch13 = libraryMatch13;
	}
	public String getLibraryMatch14() {
		return libraryMatch14;
	}
	public void setLibraryMatch14(String libraryMatch14) {
		this.libraryMatch14 = libraryMatch14;
	}
	public String getLibraryMatch15() {
		return libraryMatch15;
	}
	public void setLibraryMatch15(String libraryMatch15) {
		this.libraryMatch15 = libraryMatch15;
	}
	public String getLibraryMatch16() {
		return libraryMatch16;
	}
	public void setLibraryMatch16(String libraryMatch16) {
		this.libraryMatch16 = libraryMatch16;
	}
	public String getLibraryMatch17() {
		return libraryMatch17;
	}
	public void setLibraryMatch17(String libraryMatch17) {
		this.libraryMatch17 = libraryMatch17;
	}
	public String getLibraryMatch18() {
		return libraryMatch18;
	}
	public void setLibraryMatch18(String libraryMatch18) {
		this.libraryMatch18 = libraryMatch18;
	}
	public String getLibraryMatch19() {
		return libraryMatch19;
	}
	public void setLibraryMatch19(String libraryMatch19) {
		this.libraryMatch19 = libraryMatch19;
	}
	public String getLibraryMatch20() {
		return libraryMatch20;
	}
	public void setLibraryMatch20(String libraryMatch20) {
		this.libraryMatch20 = libraryMatch20;
	}
}

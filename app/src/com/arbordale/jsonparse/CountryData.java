package com.arbordale.jsonparse;

import java.util.ArrayList;

public class CountryData {

	private String countryCode;
	private String countryName;
	private ArrayList<SateData> states;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public ArrayList<SateData> getStates() {
		return states;
	}
	public void setStates(ArrayList<SateData> states) {
		this.states = states;
	}
	
	
}

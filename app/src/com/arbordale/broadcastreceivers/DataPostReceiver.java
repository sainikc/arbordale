package com.arbordale.broadcastreceivers;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.arbordale.database.SqliteDb;
import com.arbordale.util.AppConstants;
import com.arbordale.util.QueryManager;
import com.arbordale.util.UtilMethods;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

public class DataPostReceiver extends BroadcastReceiver {

	private ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
	private Context context;
    private ArrayList<String> idToDelete=new ArrayList<String>();
    
	@Override
	public void onReceive(Context context, Intent intent) {
		this.context=context;
		broadCast(UtilMethods.isConnectionAvailable(context));
	}

	
	public void broadCast(boolean connection)
	{
		params.clear();
		 
		if(connection)
		{
		  ArrayList<ArrayList<NameValuePair>> paramsReader = new ArrayList<ArrayList<NameValuePair>>();
	      
	      SqliteDb db_conn=new SqliteDb(context); 
	      Cursor log=db_conn.fetchData(QueryManager.QUERY_FETCH_ACTIVITY_LOG, null);
	      
	      if(log!=null)
	      {
	    	if(log.getCount()>0)
	        {
	    	  paramsReader.clear();
	    	  
	    	  log.moveToFirst();
	    	  
	    	  while(!log.isAfterLast())
	    	  {
	    		  params=new ArrayList<NameValuePair>();
	    		  
	    		  idToDelete.add(log.getString(4));
	    		  
	    		  String siteCodeOrEmail=log.getString(1);
	    		  
	    		  if(UtilMethods.validEmail(siteCodeOrEmail))
		    	  {
	    			  params.add(new BasicNameValuePair("e",siteCodeOrEmail));
	    			  params.add(new BasicNameValuePair("p",log.getString(2)));
		    	  }
	    		  
	    		  else if(siteCodeOrEmail.trim().length()!=0)
	    		  {
	    			  params.add(new BasicNameValuePair("c",siteCodeOrEmail)); 
	    		  }
	    		  
	    		  else
	    		  {
	    			  params.add(new BasicNameValuePair("c","Dummy"));
	    		  }	  
	    		  
				  params.add(new BasicNameValuePair("s",log.getString(0)));
				  
				  if(UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, context, AppConstants.KEY_LANGUAGE).equalsIgnoreCase(AppConstants.VALUE_ENGLISH))
				    params.add(new BasicNameValuePair("lang","en"));
					  
				  else
					params.add(new BasicNameValuePair("lang","es")); 
					  
				  params.add(new BasicNameValuePair("act_when",log.getString(3)));
				  params.add(new BasicNameValuePair("open",""));
	    		  
				  paramsReader.add(params);
				  
	    		  log.moveToNext();
	    	  }	
	    	  
	    	  Object params[]={AppConstants.SERVICE_VALIDATE, paramsReader};
	    	  
	    	  if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
	    		  new ReaderLog().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,params);
	    	  }
	    	  
	    	  else
	    	    new ReaderLog().execute(params);
	      }
	      log.close();
	      db_conn.closeDb();
	      }
		}
	}
	
	
    private class ReaderLog extends AsyncTask<Object , Integer , String>{
		
		ArrayList<ArrayList<NameValuePair>> data=null;
		
		@SuppressWarnings("unchecked")
		@Override
		public String doInBackground(Object... params) {
			
			Log.i(AppConstants.TAG,"Posing data to server");
			
			String result="";
			
			try
			{
			 String url=(String)params[0];
			
			 data=(ArrayList<ArrayList<NameValuePair>>)params[1];
			
			 int i=0,size=data.size();
			 
			 SqliteDb db_conn=new SqliteDb(context);
			 
			 while(size>i)
			  {	
			    result=UtilMethods.getData(url,data.get(i));
			    
			    if(result.equalsIgnoreCase(AppConstants.STATUS_SUCCESS))
			    {
			    Log.i(AppConstants.TAG,"Successfuly posted now deleting"+idToDelete.get(i));
				db_conn.execSQL("DELETE from activity_log where act_id="+idToDelete.get(i));
				i++;
			    }
			  }
			 
			  db_conn.closeDb();
			}
			
			catch(Exception e)
			{
				
			}
			
			return result;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result){
		}

		@Override
		protected void onPreExecute(){
		}
	}
}

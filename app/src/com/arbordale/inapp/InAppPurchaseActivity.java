package com.arbordale.inapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.arbordale.billing.utils.IabHelper;
import com.arbordale.billing.utils.IabResult;
import com.arbordale.billing.utils.Inventory;
import com.arbordale.billing.utils.Purchase;
import com.arbordale.billing.utils.SkuDetails;
import com.arbordale.billing.utils.IabHelper.QueryInventoryFinishedListener;
import com.arbordale.database.SqliteDb;
import com.arbordale.home.AsyncTaskCompleteListener;
import com.arbordale.home.HomeScreen;
import com.arbordale.home.R;
import com.arbordale.navfragments.UnZipper;
import com.arbordale.reader.CurlActivity;
import com.arbordale.util.AppConstants;
import com.arbordale.util.AppMessaging;
import com.arbordale.util.GlobalVars;
import com.arbordale.util.PathUtil;
import com.arbordale.util.UtilMethods;

public class InAppPurchaseActivity extends SherlockFragmentActivity implements Observer,
                                                                               AsyncTaskCompleteListener<String>
                                                                               
                                                                               {
	private AQuery aq;
	private ImageView icon, description;
	private TextView title, price, author, illustratedBy, pages_age,acceleratedReader, grades, lexile;
	private Button buy;
	private TextView intro, reviews, educator_keywords, animals;
	private String book_title,fileDownloading="",fileSetName="set",noofpages="7",colorcode="";
	private double density= 1.0;
	
	private String ITEM_SKU = "android.test.purchased";
	private ActionBar actionBar;
	ProgressBar mBar;
    Dialog d;
    WebServiceCallTask webCall;
    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    
    String base64EncodedPublicKey = "";
    private static final String TAG = "Arbordale";
    private IabHelper mHelper;
    
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.buy_layout);

		density=getResources().getDisplayMetrics().density;
		
		actionBar = getSupportActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME| ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setTitle("Purchase");
		actionBar.setIcon(R.drawable.splash_drawable);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		aq = new AQuery(this);

		icon = (ImageView) findViewById(R.id.book_icon);
		//icon.setKeepScreenOn(true);
		description = (ImageView) findViewById(R.id.book_image);

		title = (TextView) findViewById(R.id.title);
		price = (TextView) findViewById(R.id.amount);
		author = (TextView) findViewById(R.id.author);
		illustratedBy = (TextView) findViewById(R.id.illustrator);
		pages_age = (TextView) findViewById(R.id.pages_age);
		acceleratedReader = (TextView) findViewById(R.id.ar);
		grades = (TextView) findViewById(R.id.grade);
		lexile = (TextView) findViewById(R.id.lexile);

		intro = (TextView) findViewById(R.id.intro);
		reviews = (TextView) findViewById(R.id.reviews);
		educator_keywords = (TextView) findViewById(R.id.educator_keywords);
		animals = (TextView) findViewById(R.id.animals_in_ebook);

		buy = (Button) findViewById(R.id.buy_btn);
		buy.setEnabled(false);

		book_title = getIntent().getStringExtra("book_name");
		fileDownloading=getIntent().getStringExtra("book_full_name");
		fileSetName=getIntent().getStringExtra("file_set_name");
		noofpages=getIntent().getStringExtra("no_of_pages");
		colorcode=getIntent().getStringExtra("colorcode");
		
		asyncJson(AppConstants.SERVICE_PURCHASE_DATA+ book_title);

		Bitmap bm = null;
		
		if(fileSetName.equalsIgnoreCase("set"))
		 bm = UtilMethods.getBitmapFromAssets("thumbs_high_density"+ "/" + book_title + "_132.png", InAppPurchaseActivity.this);
		
		else if(fileSetName.equalsIgnoreCase("AllTitleSeteBooks"))
		  bm = UtilMethods.getBitmapFromAssets("thumbs_high_density"+ "/" + "ic_launcher" + "_132.png", InAppPurchaseActivity.this);
		
		else
		  bm = UtilMethods.getBitmapFromAssets("thumbs_high_density"+ "/" + "ic_launcher_orange" + "_132.png", InAppPurchaseActivity.this);

		base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoi/JaAa36yDFUqmiVnnHoyY7O5nux4EEZAB5NIlFz4j1f6JfWtla4XS4W7DXN+aGOGDggakCaa/HElNc8F5uKFgj5FnBpQ1Gtq446OyBBPM5xY6e5UYAfkLIfJHsTy1CYHHmW4tCzk4oiQvO2LuwOaAHTMR6rJN+8fN4092q2hpxRKeHO/jms2VcTcmDuFnns1wcNGsBg6Gau2RFFFtUIYBfX26aa4R+jCdRySQh50529Hyyg+tGreKQTFVrUDnzm+nK026OmfM3Mem9gH+ZqvUoAP08+eqYsshdbsypv0W18wUGbijEbJHKtziEXKnJh1Lv40xMBH7QA9HGN4iSRwIDAQAB";//"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoi/JaAa36yDFUqmiVnnHoyY7O5nux4EEZAB5NIlFz4j1f6JfWtla4XS4W7DXN+aGOGDggakCaa/HElNc8F5uKFgj5FnBpQ1Gtq446OyBBPM5xY6e5UYAfkLIfJHsTy1CYHHmW4tCzk4oiQvO2LuwOaAHTMR6rJN+8fN4092q2hpxRKeHO/jms2VcTcmDuFnns1wcNGsBg6Gau2RFFFtUIYBfX26aa4R+jCdRySQh50529Hyyg+tGreKQTFVrUDnzm+nK026OmfM3Mem9gH+ZqvUoAP08+eqYsshdbsypv0W18wUGbijEbJHKtziEXKnJh1Lv40xMBH7QA9HGN4iSRwIDAQAB";

		if(density<1.5)
		  bm=Bitmap.createScaledBitmap(bm, bm.getWidth()/2, bm.getHeight()/2, true);
		 	
		icon.setImageBitmap(bm);

		ITEM_SKU = "com.arbordale."+book_title.toLowerCase();
		
		buy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				String buytext=String.valueOf(buy.getText());
				
				if(buytext.trim().length()==7)
				{
					mHelper.launchPurchaseFlow(InAppPurchaseActivity.this,ITEM_SKU, 10001, mPurchaseFinishedListener,"mypurchasetoken");
				}
				
				else if((buytext.equalsIgnoreCase("View Ebook")))
				{
					 CurlActivity.currentIndex=0;	 
					 	 
					 Intent it=new Intent(InAppPurchaseActivity.this,CurlActivity.class);
					 it.putExtra("chaptername", book_title);
					 it.putExtra("no_of_pages", noofpages);
					 it.putExtra("mode","YES");
					 it.putExtra("colorcode", colorcode);
					 it.putExtra("book_full_name", fileDownloading);
					 it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 startActivity(it);
					 
					 InAppPurchaseActivity.this.finish();
				}
				
				else if((buytext.equalsIgnoreCase("Refresh Ebooks")))
				{
					 GlobalVars.HOME_READER_PRESSED="false";
					 GlobalVars.BACK_FROM_PURCHASE_DOWNLOAD="false";
					 UtilMethods.updateAddSharedPrefernces(AppConstants.SHARED_PREFERENCES_SYSTEM_FILE, InAppPurchaseActivity.this, AppConstants.KEY_FRAG_VISIBLE_LAST,AppConstants.FRAG_GRIDVIEW);
					 InAppPurchaseActivity.this.finish();	 
					 Intent it=new Intent(InAppPurchaseActivity.this,HomeScreen.class);
					 it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 startActivity(it);
				}
			
			}
		});
		
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG,"item sku is "+ITEM_SKU);
	}
	
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	public void asyncJson(String url) {
		aq.ajax(url, JSONArray.class, this, "jsonCallback");
	}

	public void jsonCallback(String url, JSONArray json, AjaxStatus status) {

		if (json != null) {

			try {
				JSONObject jObject = json.getJSONObject(0);

				String bookTitle = jObject.getString("title");
				String author = jObject.getString("author");
				String illustrator = jObject.getString("illustrator");
				String numberPages = jObject.getString("num_pages");
				String ages = jObject.getString("ages");
				String grades = jObject.getString("grades");
				String lexile_level = jObject.getString("lexile_level");
				String reading_counts = jObject.getString("reading_counts");
				String accelerated_reader = jObject.getString("accelerated_reader");
				String educator_keywords = jObject.getString("educator_keywords");
				String review1 = jObject.getString("review1");
				String review2 = jObject.getString("review2");
				String review3 = jObject.getString("review3");
				String book_intro = jObject.getString("book_intro");
				String animals = jObject.getString("animals");

				title.setText(bookTitle);
				InAppPurchaseActivity.this.author.setText("Author: " + author);
				illustratedBy.setText("Illustrated by: " + illustrator);
				pages_age.setText(numberPages + " pg  " + " Age: " + ages);
				acceleratedReader.setText("AR: "+ accelerated_reader);
				lexile.setText("Lexile: " + lexile_level);
				InAppPurchaseActivity.this.grades.setText("Grades: " + grades);

				intro.setText(Html.fromHtml(book_intro));
				reviews.setText(Html.fromHtml("<b><font color=\"#15317E\">Reviews</font></b><br>"
								+ review1
								+ "<br><br>"
								+ review2
								+ "<br><br>"
								+ review3));
				
				InAppPurchaseActivity.this.educator_keywords.setText(Html
								.fromHtml("<b><font color=\"#15317E\">Educator Keywords:</font></b><br>"
										+ educator_keywords));
				InAppPurchaseActivity.this.animals.setText(Html
								.fromHtml("<b><font color=\"#15317E\">Animals in the book:</font></b><br>"
										+ animals));

				aq.id(description)
						.progress(R.id.downloadprogress)
						.image("http://www.arbordalepublishing.com/eBooks2/images/Previews/"
								+ book_title + "-Preview.jpg", true, true, 0,
								0, new BitmapAjaxCallback() {

									@Override
									public void callback(String url,
											ImageView iv, Bitmap bm,
											AjaxStatus status) {
										description.setImageBitmap(bm);
										
										mHelper = new IabHelper(InAppPurchaseActivity.this, base64EncodedPublicKey);
										mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

											public void onIabSetupFinished(IabResult result) {
												if (!result.isSuccess()) {
												}

												else {
													String[] moreSkus = { ITEM_SKU };
													mHelper.queryInventoryAsync(true, Arrays.asList(moreSkus),
															new QueryInventoryFinishedListener() {
																@Override
																public void onQueryInventoryFinished(
																		IabResult result, Inventory inv) {
																	if (result.isSuccess()) {
																		// If we successfully got the price,
																		// show it in the text field
																		SkuDetails details = inv.getSkuDetails(ITEM_SKU);
																		String price="";
																		
																		if(details!=null)
																		 {
																			price = details.getPrice();
																			buy.setEnabled(true);
																			InAppPurchaseActivity.this.price.setText(price);
																		 }
																		
																		else
								                                         {
																			UtilMethods.createToast(InAppPurchaseActivity.this, "In App item not created/active yet");
																			buy.setEnabled(false);
																			InAppPurchaseActivity.this.price.setText("Can't get price");
								                                         }
																		
																		// On successful init and price getting,
																		// enable the "buy me" button
																		
																		
																	} else {
																		// Error getting the price... show a
																		// sorry text in the price field now
																		price.setText("can't get price");
																	}
																}
															});
												}
											}
										});

										
										if (bm != null) {
											aq.id(R.id.downloadprogress).gone();
										}
									}
								});

			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				// Handle error
				return;
			} else if (purchase.getSku().equals(ITEM_SKU)) {
				
				System.out.println("item successfuly purchased ");
				
				if(ITEM_SKU.equalsIgnoreCase("android.test.purchased"))
				 consumeItem();
				
				else
				{
					params.clear();
					params.add(new BasicNameValuePair("e",UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, InAppPurchaseActivity.this, AppConstants.KEY_EMAIL)));
					params.add(new BasicNameValuePair("titles",book_title));
		
					webCall=new WebServiceCallTask();
					webCall.execute(AppConstants.SERVICE_PURCHASE_RESULT, params);
					
					SqliteDb db_conn=new SqliteDb(InAppPurchaseActivity.this);
					
					ContentValues cv=new ContentValues();
					cv.put("Status", "YES");
					
					db_conn.updateData("book_icon", cv, "Book_Name='"+book_title+"'");
					
					db_conn.closeDb();
					
					buy.setText("View Ebook");
					
					downloadComplete();
				}	
			}
		}
	};

	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}
	
	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

		public void onQueryInventoryFinished(IabResult result,Inventory inventory) {

			if (result.isFailure()) {
				// Handle failure
			} else {
				mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),mConsumeFinishedListener);
			}
		}
	};
	
	
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				
				System.out.println("item successfuly consumed ");
				
				params.clear();
				params.add(new BasicNameValuePair("e",UtilMethods.getDataFromSharedPrefernces(AppConstants.SHARED_PREFERENCES_FILE_NAME, InAppPurchaseActivity.this, AppConstants.KEY_EMAIL)));
				params.add(new BasicNameValuePair("titles",book_title));
	
				webCall=new WebServiceCallTask();
				webCall.execute(AppConstants.SERVICE_PURCHASE_RESULT, params);
				
				SqliteDb db_conn=new SqliteDb(InAppPurchaseActivity.this);
				
				ContentValues cv=new ContentValues();
				cv.put("Status", "YES");
				
				db_conn.updateData("book_icon", cv, "Book_Name='"+book_title+"'");
				
				db_conn.closeDb();
				
				buy.setText("View Ebook");
				
				downloadComplete();
				
			} else {
			}
		}
	};
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mHelper != null)
			mHelper.dispose();
		mHelper = null;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		 switch (item.getItemId()) {
		    
		    case android.R.id.home:
		    	onBackPressed();
	           break;
		 }
		 return true;
	}
	
	
	public void downloadComplete()
	{
		 final AlertDialog alertDialog = new AlertDialog.Builder(InAppPurchaseActivity.this).create(); //Read Update
		    alertDialog.setTitle("Please Select");
		    alertDialog.setMessage("Thank you for your purchase. Your eBook is available now for cloud viewing or you can download the book right away!");
		    alertDialog.setButton( Dialog.BUTTON_POSITIVE, "Download", new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int which) {
		    	   
		    	   if(UtilMethods.isConnectionAvailable(InAppPurchaseActivity.this))
					{
					  new DownLoader().execute(new String[]{PathUtil.currentPath(1,"")+book_title+".zip" , book_title});
					} 
					
					else
					{
					  UtilMethods.createToast(InAppPurchaseActivity.this, AppMessaging.NO_INTERNET_CONNECTION);
					}	
		       }
		    });
		    
		    alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "View Ebook", new DialogInterface.OnClickListener()    {
		    	      public void onClick(DialogInterface dialog, int which) {
		    	    	     
		    	    	     CurlActivity.currentIndex=0;	 
						 	 
							 Intent it=new Intent(InAppPurchaseActivity.this,CurlActivity.class);
							 it.putExtra("chaptername", book_title);
							 it.putExtra("no_of_pages", noofpages);
							 it.putExtra("mode","YES");
							 it.putExtra("colorcode", colorcode);
							 it.putExtra("book_full_name", fileDownloading);
							 it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							 startActivity(it);
							 
							 InAppPurchaseActivity.this.finish();
		    	      }
		 });
		 alertDialog.show();
	}
	
	
	 public class DownLoader extends AsyncTask<String , Integer , Long>{

	    	int downloadingsuccessful = 1;
			int downloadingerror = 0;
			String filename="";
			
			@Override
			public Long doInBackground(String... params){
				
				try{
					InputStream InStream = null;
					try{
						HttpClient httpclient = new DefaultHttpClient();
						HttpGet getRequest = new HttpGet();
						getRequest.setURI(new URI(params[0].toString().replace(" " , "%20")));
						HttpResponse response = httpclient.execute(getRequest);
						long lenght = response.getEntity().getContentLength();
						InStream = response.getEntity().getContent();
						mBar.setMax((int)lenght);
						mBar.setProgress(0);
					}catch (URISyntaxException e){
						Log.e("Downloading error " , e.getMessage().toString());
						mBar.setProgress(0);
						return Long.valueOf(downloadingerror);
					}
					filename=params[1];
					
					File SDCardRoot = new File(Environment.getExternalStorageDirectory()+"/"+AppConstants.APP_FOLDER);
			        //create a new file, specifying the path, and the filename
			        //which we want to save the file as.
					
			        File file = new File(SDCardRoot,params[1]+".zip");
					FileOutputStream output = new FileOutputStream(file);
					byte data[] = new byte[256];
					long total = 0;
					int count;
					while ((count = InStream.read(data)) != - 1){
						total += count;
						output.write(data , 0 , count);
						publishProgress((int)(total));
					}

					output.flush();
					output.close();
					InStream.close();
					
				}catch (IOException e){
					Log.e("Downloading error " , e.getMessage().toString());
					return Long.valueOf(downloadingerror);
				}
				return Long.valueOf(downloadingsuccessful);
			}

			@Override
			protected void onPostExecute(Long result){
				super.onPostExecute(result);
				d.dismiss();
				
				if(result==1)
				 unzipWebFile(filename);
			}

			@Override
			protected void onPreExecute(){
				super.onPreExecute();
				d=new Dialog(InAppPurchaseActivity.this,R.style.FullHeightDialog);
				d.setContentView(R.layout.download_dialog);
				d.setCancelable(false);
				d.setCanceledOnTouchOutside(false);
				mBar=(ProgressBar) d.findViewById(R.id.downloadprogresshorizontal);
				TextView txt=(TextView)d.findViewById(R.id.title);
				
				if(fileDownloading.contains(", The")||fileDownloading.contains(", A"))
				{
					String arr[]=fileDownloading.split(",");
					txt.setText(""+arr[1]+" "+arr[0]);
				}
				
				d.show();
			}

			@Override
			protected void onProgressUpdate(Integer... values){
				mBar.setProgress(values[0]);
			}
		}
	    
	    private void unzipWebFile(String filename) {
	 	    
	    	d=new Dialog(InAppPurchaseActivity.this,R.style.FullHeightDialog);
			d.setContentView(R.layout.download_dialog);
			d.setCancelable(false);
			d.setCanceledOnTouchOutside(false);
			mBar=(ProgressBar) d.findViewById(R.id.downloadprogresshorizontal);
			TextView txt1=(TextView)d.findViewById(R.id.dwnldtxt);
			txt1.setText("Installing...");
			TextView txt=(TextView)d.findViewById(R.id.title);
			
			if(fileDownloading.contains(", The")||fileDownloading.contains(", A"))
			{
				String arr[]=fileDownloading.split(",");
				txt.setText(""+arr[1]+" "+arr[0]);
			}
			
			mBar.setVisibility(View.INVISIBLE);
			ProgressBar pb=(ProgressBar) d.findViewById(R.id.install_progress);
			pb.setVisibility(View.VISIBLE);
			d.show();
	    	
	    	File SDCardRoot = new File(Environment.getExternalStorageDirectory()+"/"+AppConstants.APP_FOLDER);

	    	String unzipLocation = SDCardRoot+ "";
	 	    String filePath = SDCardRoot.toString();

	 	    UnZipper unzipper = new UnZipper(filename, filePath, unzipLocation);
	 	    unzipper.addObserver(this);
	 	    unzipper.unzip();
	 	    
	 	}

		@Override
		public void update(Observable arg0, Object arg1) {
			SqliteDb db_conn=new SqliteDb(InAppPurchaseActivity.this);
			
			ContentValues cv=new ContentValues();
			cv.put("Download_status", "YES");
			
			db_conn.updateData("book_icon", cv, "book_full_name="+DatabaseUtils.sqlEscapeString(fileDownloading));
			
			if(!fileSetName.equalsIgnoreCase("set"))
			 db_conn.updateData("book_icon", cv, "Section="+DatabaseUtils.sqlEscapeString(fileSetName));
			
			db_conn.closeDb();
			
			buy.setText("View Ebook");
			
		    if(!fileSetName.equalsIgnoreCase("set"))
			  buy.setText("Refresh Ebooks");
		    
		    if(d!=null&&d.isShowing())
				d.dismiss();
		}

		@Override
		public void onTaskComplete(String result) {
		}


	    class WebServiceCallTask extends AsyncTask<Object , Integer , String>{ 
	      
		@Override
		protected String doInBackground(Object... params) {
			
			String url=(String)params[0];
			
			@SuppressWarnings("unchecked")
			ArrayList<NameValuePair> data=(ArrayList<NameValuePair>)params[1];
			
			String result=UtilMethods.getData(url,data);
			
			return result;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result){
		}

		@Override
		protected void onPreExecute(){
		}
    }
		
}
